import datetime
import getpass

import pytest

from gleisbauer.experiments.journal import (
    Journal,
    _journal_path,
    default_journal_lock,
    gen_exp_id,
    Experiment,
    ContainerRunMetadata,
    Status,
    Tuple,
)
from gleisbauer.container import Status as ContainerStatus
from gleisbauer.utils.typing_ import Set, Optional


_next_mock_pid = 5000000
_next_mock_sshd_port = 14000


@pytest.fixture
def running_pids(monkeypatch):
    pids = set()

    def mocked_proc_exists(pid: int) -> bool:
        return pid in pids

    import gleisbauer.experiments.journal as journal

    orig = journal._proc_exists
    monkeypatch.setattr(journal, "_proc_exists", mocked_proc_exists)

    yield pids

    monkeypatch.setattr(journal, "_proc_exists", orig)


@pytest.fixture
def running_containers(monkeypatch):
    import gleisbauer.experiments.journal as journal
    import gleisbauer.container as container

    containers: Set[Tuple[str, journal.Status]] = set()

    class MockEngine:
        def find_container(
            self, id: str = None, name: str = None
        ) -> Optional[container.Container]:
            if name is not None:
                raise NotImplementedError()

            for con_tuple in containers:
                con_id, con_status = con_tuple
                if con_id == id:
                    return container.Container("mocked", con_id, None, con_status)
                return None

    engine = MockEngine()

    def mocked_default_engine():
        return engine

    orig = container.default_engine
    monkeypatch.setattr(container, "default_engine", mocked_default_engine)

    yield containers

    monkeypatch.setattr(container, "default_engine", orig)


@pytest.fixture
def mock_journal_path(monkeypatch, tmp_path):
    import gleisbauer.experiments.journal as journal

    path = tmp_path / "journal.json"
    orig_path = _journal_path

    monkeypatch.setattr(journal, "_journal_path", path)

    yield path

    monkeypatch.setattr(journal, "_journal_path", orig_path)


@pytest.fixture
def corrupt_journal_file_lock(mock_journal_path):
    lock = default_journal_lock()
    lock.target.write_text("not a json")
    return lock


def build_experiment(
    status: Status,
    running_pids: Set[int] = None,
    running_containers: Set[Tuple[str, ContainerStatus]] = None,
) -> Experiment:

    exp_id = gen_exp_id()
    now = datetime.datetime.now()

    global _next_mock_pid
    pid = _next_mock_pid
    _next_mock_pid += 1

    if running_pids is not None:
        running_pids.add(pid)

    global _next_mock_sshd_port
    sshd_port = _next_mock_sshd_port
    _next_mock_sshd_port += 1

    run_metadata = None
    if status == "running":
        con_id = f"{getpass.getuser()}_{exp_id}"
        run_metadata = ContainerRunMetadata(con_id, now, "localhost")
        if running_containers is not None:
            running_containers.add((con_id, "running"))

    return Experiment(
        exp_id,
        getpass.getuser(),
        pid,
        status,
        sshd_port,
        now,
        run_metadata,
    )


@pytest.fixture
def running_exps(running_containers):
    return [
        build_experiment("running", running_containers=running_containers),
        build_experiment("running", running_containers=running_containers),
    ]


@pytest.fixture
def waiting_exps(running_pids):
    return [
        build_experiment("waiting", running_pids=running_pids),
        build_experiment("waiting", running_pids=running_pids),
    ]


@pytest.fixture
def preparing_exps():
    return [
        build_experiment("preparing"),
        build_experiment("preparing"),
    ]


@pytest.fixture
def journal_without_waiting_exps(mock_journal_path, running_exps, preparing_exps):
    journal = Journal()

    # todo: mock engine.find_container as a factory
    # todo: mock _proc_exists as a factory

    for exp in running_exps:
        journal.running[exp.id] = exp

    for exp in preparing_exps:
        journal.preparing[exp.id] = exp

    with default_journal_lock() as lock:
        journal.write(lock)

    return journal


@pytest.fixture
def journal_with_waiting_exps(journal_without_waiting_exps, waiting_exps):
    journal = journal_without_waiting_exps
    for exp in waiting_exps:
        journal.waiting[exp.id] = exp
        journal.wait_queue.append(exp.id)

    return journal


def test_init_exp_in_empty_journal(mock_journal_path):
    with default_journal_lock() as lock:
        Journal.init_exp(lock, gen_exp_id())
        journal = Journal.read(lock)

        assert len(journal.active_experiments) == 1
        assert len(journal.preparing) == 1


def test_read_not_existing_journal(mock_journal_path):
    with default_journal_lock() as lock:
        journal = Journal.read(lock)

        assert len(journal.active_experiments) == 0
        assert len(journal.wait_queue) == 0


def test_read_corrupt_journal(corrupt_journal_file_lock):
    with corrupt_journal_file_lock:
        journal = Journal.read(corrupt_journal_file_lock)

        assert len(journal.active_experiments) == 0
        assert len(journal.wait_queue) == 0


def test_read_without_acquired_journal_lock_raises_ValueError(mock_journal_path):
    lock = default_journal_lock()
    with pytest.raises(ValueError):
        Journal.read(lock)


def test_write_empty_journal(mock_journal_path):
    with default_journal_lock() as lock:
        journal = Journal()
        journal.write(lock)

        assert lock.target.exists()


def test_write_without_acquired_journal_lock_raises_ValueError(mock_journal_path):
    lock = default_journal_lock()
    journal = Journal()
    with pytest.raises(ValueError):
        journal.write(lock)


def test_write_of_filled_journal_removes_orphaned_exps(
    journal_with_waiting_exps, mock_journal_path, running_pids, running_containers
):
    running_pids.clear()
    running_containers.clear()

    with default_journal_lock() as lock:
        journal = journal_with_waiting_exps
        journal.write(lock)

        assert len(journal.history) > 0
        assert len(journal.running) == 0
        assert len(journal.waiting) == 0


def test_try_exp_start_all_waiting_exps_in_correct_order(journal_with_waiting_exps):
    with default_journal_lock() as lock:
        journal_with_waiting_exps.write(lock)

        for exp_id in journal_with_waiting_exps.waiting:
            assert Journal.try_exp_start(
                # container id, device_ids, secondary_hosts, primary_host
                lock,
                exp_id,
                lambda j: ["container_id", list(), list(), "localhost"],
            )

        assert len(Journal.read(lock).waiting) == 0


def test_try_exp_start_of_out_of_place_exp_fails(
    journal_with_waiting_exps, waiting_exps
):
    exp_id = journal_with_waiting_exps.wait_queue[-1]
    with default_journal_lock() as lock:
        journal_with_waiting_exps.write(lock)

        assert not Journal.try_exp_start(
            lock,
            exp_id,
            # container id, device_ids, secondary_hosts, primary_host
            lambda j: ["container_id", list(), list(), "localhost"],
        )

        assert len(Journal.read(lock).waiting) == len(waiting_exps)


def test_continuously_try_exp_start():
    # todo: how should this be tested? needs at least two threads as otherwise it
    #  becomes a endless loop
    pytest.skip("not implemented")
