import getpass
import hashlib
import os
import tarfile
from contextlib import contextmanager
from io import BytesIO

import pytest
from docker.errors import ImageNotFound

import gleisbauer.container
from gleisbauer.utils.typing_ import (
    Generator,
    List,
    Optional,
    Dict,
    Tuple,
)


class MockDockerClient:
    class Image:
        def __init__(self, size: int, tags: List[str], labels: Optional[Dict] = None):
            self.attrs = {"Size": size}
            self.tags = tags
            self.labels = labels if labels is not None else dict()
            self.id = hashlib.blake2s(",".join(tags).encode()).hexdigest()
            self.short_id = self.id[10:]
            self._owner = None

        def history(self, *args, **kwargs):
            raise NotImplementedError()

        def reload(self):
            pass

        def save(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "save", kwargs)
            return [self.id.encode()]

        def tag(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "tag", kwargs)

        def add_owner(self, owner: "MockDockerClient"):
            self._owner = owner

    class Images:
        def __init__(
            self,
            owner: "MockDockerClient",
            images: List["MockDockerClient.Image"] = None,
        ):

            self._images = images if images is not None else list()
            self._owner = owner

            for img in self._images:
                img.add_owner(owner)

        def build(self, **kwargs):
            self._owner.add_to_call_history(self.__class__, "build", kwargs)

            img = MockDockerClient.Image(10, [kwargs["tag"]])
            img.add_owner(self._owner)
            self._images.append(img)

            return img, None

        def load(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "load", kwargs)

        def pull(self, repository, tag=None, all_tags=False):
            self._owner.add_to_call_history(
                self.__class__,
                "pull",
                dict(repository=repository, tag=tag, all_tags=all_tags),
            )

            img = MockDockerClient.Image(10, [f"{repository}:{tag}"])
            img.add_owner(self._owner)
            self._images.append(img)

            return img

        def get(self, name: str):
            self._owner.add_to_call_history(self.__class__, "get", dict(name=name))

            for img in self._images:
                if img.id == name:
                    return img
                for tag in img.tags:
                    if tag == name:
                        return img

            raise ImageNotFound("Image doesn't exist!")

        def list(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "list", kwargs)
            if args or kwargs:
                raise NotImplementedError()
            return self._images.copy()

    class Container:
        def __init__(
            self,
            name: str,
            image: "MockDockerClient.Image",
            status: str,
            labels: Optional[Dict] = None,
        ):

            self.attrs = dict()
            self.name = name
            self.image = image
            self.labels = labels
            self.status = status
            self.id = hashlib.blake2s(name.encode()).hexdigest()
            self.short_id = self.id[10:]
            self._owner = None

        def start(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "start", kwargs)

        def remove(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "remove", kwargs)

        def kill(self, *args, **kwargs):
            self._owner.add_to_call_history(self.__class__, "kill", kwargs)

        def get_archive(self, *args, **kwargs):
            raise NotImplementedError()

        def add_owner(self, owner: "MockDockerClient"):
            self._owner = owner

    class Containers:
        def __init__(
            self,
            owner: "MockDockerClient",
            containers: List["MockDockerClient.Container"] = None,
        ):
            self._containers = containers if containers is not None else list()
            self._owner = owner

            for con in self._containers:
                con.add_owner(owner)

        def list(self, all):
            if all:
                return self._containers.copy()

            return [c for c in self._containers if c.status == "running"]

        def create(self, **kwargs):
            self._owner.add_to_call_history(self.__class__, "create", kwargs)
            try:
                img = next(
                    img
                    for img in self._owner.images.list()
                    if kwargs["image"] in img.tags
                )
            except StopIteration:
                raise ImageNotFound("Image doesn't exist")
            container = MockDockerClient.Container(kwargs["name"], img, "running")
            container.add_owner(self._owner)
            self._containers.append(container)

            return container

        def get(self, id_or_name: str):
            self._owner.add_to_call_history(
                self.__class__, "get", dict(id_or_name=id_or_name)
            )
            for con in self._containers:
                if con.id == id_or_name or con.name == id_or_name:
                    return con

        def add_owner(self, owner: "MockDockerClient"):
            self._owner = owner

    def __init__(
        self,
        images: List["MockDockerClient.Image"] = None,
        containers: List["MockDockerClient.Container"] = None,
    ):

        self.images = MockDockerClient.Images(images=images, owner=self)
        self.containers = MockDockerClient.Containers(containers=containers, owner=self)
        self._call_history = []

    def add_to_call_history(self, cls, name, args):
        self._call_history.append((cls, name, args))

    def has_called(self, cls, name) -> Optional[Tuple]:
        for old_cls, old_name, old_args in self._call_history:
            if cls == old_cls and name == old_name:
                return old_args

        return None

    def clear_history(self):
        self._call_history.clear()


@pytest.fixture
def mock_open_docker_client(monkeypatch: "pytest.MonkeyPatch"):

    # todo: this can't handle multi computer simulation as they will all share the
    #  same "docker client"

    client = None

    @contextmanager
    def _mock_opener(*args, **kwargs) -> Generator[MockDockerClient, None, None]:
        yield client

    def _create_shared_client(
        images: List[MockDockerClient.Image] = None,
        containers: List[MockDockerClient.Container] = None,
    ):

        nonlocal client
        client = MockDockerClient(images, containers)
        return client

    orig_opener = gleisbauer.container.open_docker_client
    monkeypatch.setattr(gleisbauer.container, "open_docker_client", _mock_opener)

    yield _create_shared_client

    monkeypatch.setattr(gleisbauer.container, "open_docker_client", orig_opener)


@pytest.fixture
def mock_add_user_to_docker_container(monkeypatch):

    called = False

    def _dummy(*args, **kwargs):
        nonlocal called
        called = True

    def _has_been_called() -> bool:
        nonlocal called
        return called

    orig = gleisbauer.container.add_user_to_docker_container
    monkeypatch.setattr(gleisbauer.container, "add_user_to_docker_container", _dummy)

    yield _has_been_called

    monkeypatch.setattr(gleisbauer.container, "add_user_to_docker_container", orig)


@pytest.fixture
def default_images():
    return [
        MockDockerClient.Image(5, ["foo.bar:latest"]),
        MockDockerClient.Image(10, ["foo.bar:3.14"]),
        MockDockerClient.Image(2, ["bar.foo:5.61"]),
        MockDockerClient.Image(12, ["bar.foo:latest"]),
    ]


@pytest.fixture
def default_containers(default_images):
    return [
        MockDockerClient.Container("running_foo.bar", default_images[0], "running"),
        MockDockerClient.Container("stopped_foo.bar", default_images[0], "exited"),
        MockDockerClient.Container("paused_foo.bar", default_images[0], "paused"),
    ]


@pytest.fixture
def empty_engine_and_client(
    mock_open_docker_client, mock_add_user_to_docker_container
) -> Tuple[gleisbauer.container.ContainerEngine, MockDockerClient]:

    client = mock_open_docker_client()
    return gleisbauer.container.DockerContainerEngine(), client


@pytest.fixture
def filled_engine_and_client(
    mock_open_docker_client,
    default_images,
    default_containers,
    mock_add_user_to_docker_container,
) -> Tuple[gleisbauer.container.ContainerEngine, MockDockerClient]:

    client = mock_open_docker_client(default_images, default_containers)
    return gleisbauer.container.DockerContainerEngine(), client


@pytest.fixture
def empty_engine(
    empty_engine_and_client,
) -> gleisbauer.container.ContainerEngine:

    engine, _ = empty_engine_and_client
    return engine


@pytest.fixture
def filled_engine(
    filled_engine_and_client,
) -> gleisbauer.container.ContainerEngine:

    engine, _ = filled_engine_and_client
    return engine


@pytest.fixture
def docker_build_file(tmp_path):
    dockerfile = tmp_path / "dockerfile"
    dockerfile.touch()
    return dockerfile


@pytest.fixture
def docker_build_tar_file(docker_build_file):

    tar_file = docker_build_file.with_suffix(".tar")
    with tarfile.open(tar_file.as_posix(), "w") as tar:
        tar.add(docker_build_file.as_posix(), docker_build_file.name)

    return tar_file


def test_get_images_with_empty_engine_returns_empty_list(empty_engine):
    assert len(empty_engine.get_images()) == 0


def test_get_images_with_engine_with_two_images_returns_them(
    filled_engine, default_images
):

    images = filled_engine.get_images()

    ids = [i.id for i in images]
    default_ids = [i.id for i in default_images]

    assert len(images) == len(default_images)
    assert ids == default_ids


def test_get_containers_with_empty_engine_returns_empty_list(empty_engine):
    assert len(empty_engine.get_containers()) == 0


def test_get_containers_with_engine_with_two_containers_returns_them(
    filled_engine, default_containers
):

    containers = filled_engine.get_containers()

    ids = [i.id for i in containers]
    default_ids = [i.id for i in default_containers]

    assert len(containers) == len(default_containers)
    assert ids == default_ids


def test_find_container_with_empty_engine_finds_none(empty_engine, default_containers):
    for con in default_containers:
        assert empty_engine.find_container(id=con.id) is None
        assert empty_engine.find_container(name=con.name) is None


def test_find_container_with_filled_engine_finds_all_default_containers(
    filled_engine, default_containers
):

    for con in default_containers:
        assert filled_engine.find_container(id=con.id).id == con.id
        assert filled_engine.find_container(name=con.name).id == con.id


def test_container_exists_with_empty_engine_is_always_false(
    empty_engine, default_containers
):

    for con in default_containers:
        assert not empty_engine.container_exists(id=con.id)
        assert not empty_engine.container_exists(name=con.name)


def test_container_exists_with_filled_engine_is_true_for_default_containers(
    filled_engine, default_containers
):

    for con in default_containers:
        assert filled_engine.container_exists(id=con.id)
        assert filled_engine.container_exists(name=con.name)


def test_find_image_with_empty_engine_is_always_false(empty_engine, default_images):
    for image in default_images:
        assert empty_engine.find_image(id=image.id) is None
        for tag in image.tags:
            assert empty_engine.find_image(name=tag) is None


def test_find_image_with_filled_engine_finds_default_images(
    filled_engine, default_images
):
    for image in default_images:
        assert filled_engine.find_image(id=image.id).id == image.id
        for tag in image.tags:
            assert filled_engine.find_image(name=tag).id == image.id


def test_image_exists_with_empty_engine_is_always_false(empty_engine, default_images):
    for image in default_images:
        assert not empty_engine.image_exists(id=image.id)
        for tag in image.tags:
            assert not empty_engine.image_exists(name=tag)


def test_image_exists_with_filled_engine_is_true_for_default_images(
    filled_engine, default_images
):
    for image in default_images:
        assert filled_engine.image_exists(id=image.id)
        for tag in image.tags:
            assert filled_engine.image_exists(name=tag)


def test_start_container_with_empty_engine_raises_ImageNotFound_for_all(
    empty_engine_and_client, default_images
):

    empty_engine, client = empty_engine_and_client

    for image in default_images:
        with pytest.raises(ImageNotFound):
            empty_engine.start_container(
                image.tags[0],
                "crazy advanced command",
                getpass.getuser(),
                os.getuid(),
                os.getgid(),
            )


def test_start_container_with_filled_engine_works_for_default_images(
    filled_engine_and_client, default_images, mock_add_user_to_docker_container
):

    filled_engine, client = filled_engine_and_client

    for image in default_images:
        container = filled_engine.start_container(
            image.tags[0],
            "crazy advanced command",
            getpass.getuser(),
            os.getuid(),
            os.getgid(),
        )

        assert container is not None
        assert (
            mock_add_user_to_docker_container()
        ), "add_user_to_docker_container_was has not been called"
        assert (
            client.has_called(MockDockerClient.Container, "start") is not None
        ), "start on container has not been called"


def test_stop_container_with_empty_engine_raises_ValueError_for_all(
    empty_engine, default_containers
):
    for con in default_containers:
        with pytest.raises(ValueError):
            empty_engine.stop_container(
                gleisbauer.container.Container(
                    con.name,
                    con.id,
                    gleisbauer.container.Image(
                        con.image.id, con.image.tags, con.image.attrs["Size"]
                    ),
                    "running",
                )
            )


def test_stop_container_with_filled_engine_calls_stop_on_them(
    filled_engine_and_client, default_containers
):
    filled_engine, client = filled_engine_and_client

    for con in default_containers:
        filled_engine.stop_container(filled_engine.find_container(con.id))
        client.has_called(MockDockerClient.Container, "remove")
        client.clear_history()


def test_build_image_with_buildfile(empty_engine_and_client, docker_build_file):
    empty_engine, client = empty_engine_and_client

    empty_engine.build_image(docker_build_file, "testing:latest")
    assert client.has_called(MockDockerClient.Images, "build")


def test_build_image_with_ctx_tar(empty_engine_and_client, docker_build_tar_file):
    empty_engine, client = empty_engine_and_client

    empty_engine.build_image(docker_build_tar_file, "testing:latest")
    assert client.has_called(MockDockerClient.Images, "build")


def test_build_image_with_filled_engine_and_exists_not_okay_and_dockerfile_raises_ValueError_on_existing_images(
    filled_engine_and_client, docker_build_file, default_images
):
    filled_engine, client = filled_engine_and_client

    for img in default_images:
        with pytest.raises(ValueError):
            filled_engine.build_image(docker_build_file, img.tags[0], False)


def test_build_image_with_filled_engine_and_exists_not_okay_and_ctx_tar_raises_ValueError_on_existing_images(
    filled_engine_and_client, docker_build_tar_file, default_images
):
    filled_engine, client = filled_engine_and_client

    for img in default_images:
        with pytest.raises(ValueError):
            filled_engine.build_image(docker_build_tar_file, img.tags[0], False)


def test_write_build_context_archive(empty_engine_and_client, docker_build_file):
    empty_engine, client = empty_engine_and_client

    bytes_io = BytesIO()
    empty_engine.write_build_context_archive(docker_build_file, bytes_io)

    assert len(bytes_io.getvalue()) != 0


def test_save_build_context_archive(
    empty_engine_and_client, docker_build_file, tmp_path
):
    empty_engine, client = empty_engine_and_client
    archive_file = tmp_path / "archive.tar"

    empty_engine.save_build_context_archive(docker_build_file, archive_file)

    assert archive_file.exists()


def test_load_image(empty_engine_and_client, docker_build_tar_file):
    empty_engine, client = empty_engine_and_client

    empty_engine.load_image(docker_build_tar_file)
    assert client.has_called(MockDockerClient.Images, "load") is not None


def test_pull_image(empty_engine_and_client):
    empty_engine, client = empty_engine_and_client

    assert empty_engine.pull_image("foo:latest") is not None
    assert client.has_called(MockDockerClient.Images, "pull") is not None


def test_save_image(filled_engine_and_client, default_images, tmp_path):
    filled_engine, client = filled_engine_and_client

    img_file = tmp_path / "img.tar"

    for d_img in default_images:
        filled_engine.save_image(filled_engine.find_image(d_img.id), img_file)

    assert client.has_called(MockDockerClient.Image, "save") is not None
    assert img_file.exists()
