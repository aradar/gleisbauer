import subprocess
from pathlib import Path

import fabric
import invoke
import pytest

from gleisbauer.utils.typing_ import Any, Dict, List, Type, Union


class RunResult:
    def __init__(
        self,
        command: str,
        connection: Union["fabric.Connection", "MockConnection"],
        return_code: int,
        stderr: str,
        stdout: str,
    ):
        self.command = command
        self.connection = connection
        self.stderr = stderr
        self.stdout = stdout
        self.return_code = return_code

    @property
    def exited(self) -> int:
        return self.return_code

    @property
    def failed(self) -> bool:
        return self.return_code != 0

    @property
    def ok(self) -> bool:
        return self.return_code == 0


class MockConnection:
    def __init__(
        self,
        host: str,
        user: str = None,
        port: int = None,
        config: Any = None,
        gateway: Any = None,
        forward_agent: bool = None,
        connect_timeout: int = None,
        connect_kwargs: Dict = None,
        inline_ssh_env: bool = None,
    ):

        self.host = host
        self.user = user
        self.port = port
        self.config = config
        self.gateway = gateway
        self.forward_agent = forward_agent
        self.connect_timeout = connect_timeout
        self.connect_kwargs = connect_kwargs
        self.inline_ssh_env = inline_ssh_env
        self._cmds_history: List[Dict[str, Any]] = []

    def _run(self, command: str, hide: bool = False, warn: bool = False) -> Any:
        # todo: should hide be handled?

        result = subprocess.run(
            args=[a for a in command.split(" ") if a],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        mock_result = RunResult(
            command, self, result.returncode, result.stderr, result.stdout
        )

        if not warn and mock_result.failed:
            raise invoke.exceptions.UnexpectedExit(mock_result)

        return mock_result

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def close(self):
        pass

    def open(self):
        pass

    def is_connected(self) -> bool:
        return True

    def get(self, remote: str, local: str = None, preserve_mode: bool = True) -> Any:
        raise NotImplementedError()

    def local(self, command: str, hide: bool = False, warn: bool = False) -> Any:
        self._cmds_history.append(
            dict(func="local", command=command, hide=hide, warn=warn)
        )
        return self._run(command, hide, warn)

    def put(self, local, remote=None, preserve_mode=True) -> Any:
        self._cmds_history.append(
            dict(func="put", local=local, remote=remote, preserve_mode=preserve_mode)
        )
        local_path = Path(local)
        remote_path = Path(remote)
        remote_path.write_bytes(local_path.read_bytes())

    def run(self, command: str, hide: bool = False, warn: bool = False) -> Any:
        self._cmds_history.append(
            dict(func="run", command=command, hide=hide, warn=warn)
        )
        return self._run(command, hide, warn)

    def forward_local(
        self,
        local_port: int,
        remote_port: int = None,
        remote_host: str = "localhost",
        local_host: str = "localhost",
    ):

        raise NotImplementedError()

    def forward_remote(
        self,
        remote_port: int,
        local_port: int = None,
        remote_host: str = "127.0.0.1",
        local_host: str = "localhost",
    ):

        raise NotImplementedError()

    def open_gateway(self):
        raise NotImplementedError()

    def sftp(self) -> Any:
        raise NotImplementedError()

    def sudo(self, command: str, hide: bool = False, warn: bool = False) -> Any:
        raise NotImplementedError()


@pytest.fixture
def mock_connection(monkeypatch: "pytest.MonkeyPatch") -> Type[MockConnection]:
    orig_connection = fabric.Connection
    monkeypatch.setattr(fabric, "Connection", MockConnection)

    yield MockConnection

    monkeypatch.setattr(fabric, "Connection", orig_connection)


@pytest.fixture
def open_ssh_connection(mock_connection):
    host = "localhost"
    last_user_id = 0

    def _open():
        nonlocal last_user_id
        con = mock_connection(host=host, user=f"foo_{last_user_id}")
        last_user_id += 1

        return con

    return _open
