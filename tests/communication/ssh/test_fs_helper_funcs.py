import pytest

from gleisbauer.communication.ssh import (
    _cp_to_remote,
    _get_remote_env,
    _mkdir_on_remote,
    _path_exists_on_remote,
)


def test_path_exists_with_existing_file(tmp_path, open_ssh_connection):
    file = tmp_path / "foo.bar"
    file.touch()
    con = open_ssh_connection()

    assert _path_exists_on_remote(file, con)


def test_path_exists_with_existing_dir(tmp_path, open_ssh_connection):
    directory = tmp_path / "foo"
    directory.mkdir()
    con = open_ssh_connection()

    assert _path_exists_on_remote(directory, con)


def test_path_exists_with_not_existing_file(tmp_path, open_ssh_connection):
    file = tmp_path / "foo.bar"
    con = open_ssh_connection()

    assert not _path_exists_on_remote(file, con)


def test_path_exists_with_not_existing_dir(tmp_path, open_ssh_connection):
    directory = tmp_path / "foo"
    con = open_ssh_connection()

    assert not _path_exists_on_remote(directory, con)


def test_cp_copies_the_file(tmp_path, open_ssh_connection):
    local_file = tmp_path / "foo.local"
    local_file.write_text("abcd")
    remote_file = tmp_path / "foo.remote"
    con = open_ssh_connection()

    _cp_to_remote(local_file, remote_file, con)

    assert remote_file.exists()
    assert remote_file.read_text() == "abcd"


def test_get_env_returns_not_empty_dict(open_ssh_connection):
    con = open_ssh_connection()
    env = _get_remote_env(con)

    assert type(env) == dict
    assert len(env) != 0


def test_mkdir_succeeds(open_ssh_connection, tmp_path):
    directory = tmp_path / "foo"
    _mkdir_on_remote(directory, open_ssh_connection())

    assert directory.exists()


def test_nested_mkdir_mkdir_with_parents_succeeds(open_ssh_connection, tmp_path):
    directory = tmp_path / "foo/bar"
    _mkdir_on_remote(directory, open_ssh_connection(), parents=True)

    assert directory.exists()


def test_nested_mkdir_and_not_parents_throws_exception(open_ssh_connection, tmp_path):
    directory = tmp_path / "foo/bar"

    with pytest.raises(FileNotFoundError):
        _mkdir_on_remote(directory, open_ssh_connection())
    assert not directory.exists()


def test_existing_dir_mkdir_and_exists_not_okay_throws_exception(
    open_ssh_connection, tmp_path
):
    directory = tmp_path / "foo"
    directory.mkdir()

    with pytest.raises(FileExistsError):
        _mkdir_on_remote(directory, open_ssh_connection())


def test_exists_ok_mkdir_succeeds(open_ssh_connection, tmp_path):
    directory = tmp_path / "foo"
    directory.mkdir()

    _mkdir_on_remote(directory, open_ssh_connection(), exist_ok=True)

    assert directory.exists()
