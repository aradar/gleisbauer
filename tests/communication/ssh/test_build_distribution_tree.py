import math

import pytest

from gleisbauer.communication.ssh import _build_distribution_tree

num_nodes = list(range(65))


@pytest.mark.parametrize("num_nodes", num_nodes)
def test_successful_building(num_nodes: int):
    tree = _build_distribution_tree(num_nodes)

    flat_tree = [i for layer in tree for i in layer]

    assert len(flat_tree) == num_nodes, "tree contains all elements"
    assert len(set(flat_tree)) == len(flat_tree), "tree contains only unique numbers"
    if num_nodes > 0:
        assert max(flat_tree) == num_nodes, "tree contains the highest requested node"
        assert len(tree[0]) == math.floor(
            math.log2(num_nodes) + 1
        ), "tree solves distribution in floor(log_2(num_nodes) + 1)"
