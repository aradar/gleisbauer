import queue
import threading
from pathlib import Path
from threading import Lock

import fabric
import pytest

import gleisbauer.cli.remote as remote
import gleisbauer.communication.ssh as ssh
import gleisbauer.utils.zipapp as zipapp

num_nodes = list(range(1, 16))


class TestSshNodeCommunicator:
    @pytest.fixture(autouse=True)
    def zipapp_run(self, tmp_path, monkeypatch):
        def patched_func():
            return mock_zipapp

        mock_zipapp = tmp_path / "foo-bar.pyz"
        mock_zipapp.touch()

        orig_func = zipapp.get_zipapp_path()
        monkeypatch.setattr(zipapp, "get_zipapp_path", patched_func)
        yield
        monkeypatch.setattr(zipapp, "get_zipapp_path", orig_func)

    @pytest.fixture(autouse=True)
    def multiple_local_homes(self, tmp_path, monkeypatch, mock_connection):
        def patched_func(connection: "fabric.Connection"):
            env = orig_func(connection)
            env["HOME"] = (
                tmp_path / connection.host / "home" / connection.user
            ).as_posix()
            env["USER"] = connection.user
            return env

        orig_func = ssh._get_remote_env
        monkeypatch.setattr(ssh, "_get_remote_env", patched_func)
        yield
        monkeypatch.setattr(ssh, "_get_remote_env", orig_func)

    @pytest.fixture()
    def cp_to_memory(self, monkeypatch):
        def patched_func(local, remote, connection):
            with lock:
                calls.append(dict(local=local, remote=remote, connection=connection))

        lock = Lock()
        calls = list()

        orig_func = ssh._cp_to_remote
        monkeypatch.setattr(ssh, "_cp_to_remote", patched_func)
        yield calls
        monkeypatch.setattr(ssh, "_cp_to_remote", orig_func)

    @pytest.fixture(autouse=True)
    def local_request_handling(self, monkeypatch):
        def patched_func(request_json, node):
            res_queue = queue.Queue()
            thread = threading.Thread(
                target=remote.main, kwargs=dict(args=[request_json])
            )
            thread.start()
            # todo: this breaks as soon as json return values are expected!
            return ssh.RequestResponse(res_queue, thread)

        orig_func = ssh._exc_node_request
        monkeypatch.setattr(ssh, "_exc_request_on_node", patched_func)
        yield
        monkeypatch.setattr(ssh, "_exc_request_on_node", orig_func)

    @pytest.fixture(params=num_nodes, autouse=True)
    def build_com(self, mock_connection, request):
        self.num_nodes = request.param
        self.com = ssh.SSHNodeCommunicator()
        for i in range(request.param):
            self.com.add_node(f"foo_{i}")

    def test_communicator_registered_all_nodes(self):
        assert len(self.com._nodes) == self.num_nodes

    def test_distribute_runtime_distributes_to_all_nodes(self, cp_to_memory):
        self.com.distribute_runtime()
        assert len(cp_to_memory) == self.num_nodes

    def test_distribute_data_tries_to_cp_all_files_to_all_nodes(self, cp_to_memory):

        files = [
            (Path("/local/foo/0"), Path("/remote/foo/0")),
            (Path("/local/foo/1"), Path("/remote/foo/1")),
            (Path("/local/foo/2"), Path("/remote/foo/2")),
            (Path("/local/foo/bar/0"), Path("/remote/foo/bar/0")),
        ]

        self.com.cp_to_all_from_client(files)

        assert len(cp_to_memory) == len(files) * self.num_nodes
