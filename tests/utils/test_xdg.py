from pathlib import Path

import pytest

from gleisbauer.utils.xdg import (
    Environ,
    _default_environ,
    xdg_cache_home,
    xdg_config_home,
    xdg_data_home,
    xdg_runtime_dir,
    xdg_state_home,
)


@pytest.fixture
def env_with_custom_locations() -> Environ:
    env = _default_environ()
    env["XDG_CACHE_HOME"] = "/foo/cache"
    env["XDG_CONFIG_HOME"] = "/foo/config"
    env["XDG_DATA_HOME"] = "/foo/local/share"
    env["XDG_RUNTIME_DIR"] = "/foo/var"
    env["XDG_STATE_HOME"] = "/foo/local/state"
    return env


@pytest.fixture
def env_with_empty_vars() -> Environ:
    env = _default_environ()
    env["XDG_CACHE_HOME"] = ""
    env["XDG_CONFIG_HOME"] = ""
    env["XDG_DATA_HOME"] = ""
    env["XDG_RUNTIME_DIR"] = ""
    env["XDG_STATE_HOME"] = ""
    return env


@pytest.fixture
def env_with_changed_home() -> Environ:
    env = _default_environ()
    env["HOME"] = "/foo/home"
    return env


def test_cache_home_with_default_env():
    path = xdg_cache_home()
    assert path.relative_to(Path.home())


def test_cache_home_with_custom_location_env(env_with_custom_locations):
    path = xdg_cache_home(env=env_with_custom_locations)
    assert path == Path("/foo/cache")


def test_cache_home_with_empty_vars_env(env_with_empty_vars):
    path = xdg_cache_home(env=env_with_empty_vars)
    assert path.relative_to(Path.home())


def test_cache_home_with_changed_home_env(env_with_changed_home):
    path = xdg_cache_home(env_with_changed_home)
    assert path.relative_to(Path("/foo/home"))


def test_config_home_with_default_env():
    path = xdg_config_home()
    assert path.relative_to(Path.home())


def test_config_home_with_custom_location_env(env_with_custom_locations):
    path = xdg_config_home(env=env_with_custom_locations)
    assert path == Path("/foo/config")


def test_config_home_with_empty_vars_env(env_with_empty_vars):
    path = xdg_config_home(env=env_with_empty_vars)
    assert path.relative_to(Path.home())


def test_config_home_with_changed_home_env(env_with_changed_home):
    path = xdg_config_home(env_with_changed_home)
    assert path.relative_to(Path("/foo/home"))


def test_data_home_with_default_env():
    path = xdg_data_home()
    assert path.relative_to(Path.home())


def test_data_home_with_custom_location_env(env_with_custom_locations):
    path = xdg_data_home(env=env_with_custom_locations)
    assert path == Path("/foo/local/share")


def test_data_home_with_empty_vars_env(env_with_empty_vars):
    path = xdg_data_home(env=env_with_empty_vars)
    assert path.relative_to(Path.home())


def test_data_home_with_changed_home_env(env_with_changed_home):
    path = xdg_data_home(env_with_changed_home)
    assert path.relative_to(Path("/foo/home"))


def test_runtime_dir_with_default_env():
    path = xdg_runtime_dir()
    assert path.relative_to(Path("/run/user"))


def test_runtime_dir_with_custom_location_env(env_with_custom_locations):
    path = xdg_runtime_dir(env=env_with_custom_locations)
    assert path == Path("/foo/var")


def test_runtime_dir_with_empty_vars_env(env_with_empty_vars):
    path = xdg_runtime_dir(env=env_with_empty_vars)
    assert path is None


def test_runtime_dir_with_changed_home_env(env_with_changed_home):
    path = xdg_runtime_dir(env_with_changed_home)
    assert path.relative_to(Path("/run/user"))


def test_state_home_with_default_env():
    path = xdg_state_home()
    assert path.relative_to(Path.home())


def test_state_home_with_custom_location_env(env_with_custom_locations):
    path = xdg_state_home(env=env_with_custom_locations)
    assert path == Path("/foo/local/state")


def test_state_home_with_empty_vars_env(env_with_empty_vars):
    path = xdg_state_home(env=env_with_empty_vars)
    assert path.relative_to(Path.home())


def test_state_home_with_changed_home_env(env_with_changed_home):
    path = xdg_state_home(env_with_changed_home)
    assert path.relative_to(Path("/foo/home"))
