import sys

import pytest

from gleisbauer.utils.zipapp import is_zipapp_run


def test_zipapp_proc(monkeypatch: "pytest.MonkeyPatch"):
    with monkeypatch.context() as m:
        m.setattr(sys, "file", sys.path + ["./foo-bar.pyz"])
        assert is_zipapp_run()


def test_not_a_zipapp_proc():
    assert not is_zipapp_run()
