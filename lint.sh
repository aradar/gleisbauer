#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

echo "---- Running: isort ----"
poetry run isort .
echo ""

echo "---- Running: black ----"
poetry run black .
echo ""

echo "---- Running: flake8 ----"
poetry run flake8 .
echo ""

echo "---- Running: mypy ----"
poetry run mypy .
