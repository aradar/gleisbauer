todo: write me

# Concepts

# Diagrams

## Node info (with 3 nodes)

```mermaid
sequenceDiagram
    participant Client
    participant Node_0
    participant Node_1
    participant Node_2
    
    par Client to Node_0:
        Client->>+Node_0:BuildReportMsg
        Node_0->>Node_0:Data gathering
        Node_0-->>-Client:ReportInfoMsg
    and Client to Node_1:
        Client->>+Node_1:BuildReportMsg
        Node_1->>Node_1:Data gathering
        Node_1-->>-Client:ReportInfoMsg
    and Client to Node_2:
        Client->>+Node_2:BuildReportMsg
        Node_2->>Node_2:Data gathering
        Node_2-->>-Client:ReportInfoMsg
    end
    
    Client->>Client: Format and print data
```

## Remote experiment start (with 3 nodes)

```mermaid
flowchart TD
    subgraph Client
        c_0[Requesting status from nodes]
        c_1[Calculating file hashes of local files]
        c_0 --> c_1
        
        c_2{local img building?}
        c_1 --> c_2
       
        c_3["Preparing docker image (local)"]
        c_2 --> |yes| c_3
        
        c_4["Preparing docker build context"]
        c_2 --> |no| c_4
        
        c_5[Selection of nodes]
        c_3 --> c_5
        c_4 --> c_5
        
        c_6[Selection of primary node] 
        c_5 --> c_6
        
        c_7[Distribution of needed data to Primary node] 
        c_6 --> c_7
    end
    
    subgraph "Node 2"
        journal_2[(Journal)]
        n_2_0[Starting experiment] 
        
        n_2_1[Adding exp to Journal]
        n_2_0 --> n_2_1
        n_2_1 -.- journal_2
        
        n_2_2[Wait until ressources are free]
        n_2_1 --> n_2_2
        
        n_2_3[Build and start worker container]
        n_2_2 --> n_2_3
        
        n_2_4[Update Journal]
        n_2_4 -.- journal_2
        n_2_3 --> n_2_4
    end
    
    
    subgraph "Node 0 (primary)"
        journal_0[(Journal)]
        
        n_0_0{img building?}
        
        n_0_1[Loading image]
        n_0_0 --> |yes| n_0_1
        
        n_0_2[Building and saving image]
        n_0_0 --> |no| n_0_2
        
        n_0_3[Data distribution to other nodes]
        n_0_1 --> n_0_3
        n_0_2 --> n_0_3
        
        n_0_4[Starting experiment] 
        n_0_3 --> n_0_4
        
        n_0_5[Adding exp to Journal]
        n_0_4 --> n_0_5
        n_0_5 -.- journal_0
        
        n_0_6[Wait until ressources are free]
        n_0_5 --> n_0_6
        
        n_0_7[Build and start worker container]
        n_0_6 --> n_0_7
        
        n_0_8[Update Journal]
        n_0_7 --> n_0_8
        n_0_8 -.- journal_0
    end
    
    c_7 --> n_0_0
    
    subgraph "Node 1"
        journal_1[(Journal)]
        n_1_0[Starting experiment] 
        
        n_1_1[Adding exp to Journal]
        n_1_0 --> n_1_1
        n_1_1 -.- journal_1
        
        n_1_2[Wait until ressources are free]
        n_1_1 --> n_1_2
        
        n_1_3[Build and start worker container]
        n_1_2 --> n_1_3
        
        n_1_4[Update Journal]
        n_1_4 -.- journal_1
        n_1_3 --> n_1_4
    end
    
    n_0_3 --> n_1_0
    n_0_3 --> n_2_0
```

## Remote data locations

- **data IDs**: Tuple of relative path to data dir and data hash
- **mapping.json**: Contains a list of mappings between data_id and original rel path as 
    well as which experiment currently uses this data. Accessing this file should 
    always happen with an acquired file lock.
- **journal.json**: Contains metadata for running experiments, currently waiting ones
    and previously run ones. Accessing this file should always happen with an 
    acquired file lock.

```
/var/tmp/dist-dl-train/
+-- data/
+------ <data_id_0>/...
+------ <data_id_1>/...
+------ <data_id_n>/...
+------ mapping.json/
+-- experiments/ 
+------ <exp_id_0>/
+---------- .ssh/...
+---------- out/...
+---------- src/...
+---------- dist-dl-train.toml
+------ <exp_id_1>/...
+------ <exp_id_n>/...
+-- tmp/
+------ image_hash>.tar
+------ ... 
/tmp/dist-dl-train/journal.json
```

# Installation

The project can be build and installed with the help of [poetry](https://python-poetry.org/docs/#installation). 
Therefor poetry needs to be installed on the system level first. This can either be done via 
[pipx](https://github.com/pypa/pipx), a package of your local package manager (apt, pacman, ...) or as a user 
installation with pip.

After the installation of poetry the project venv has to be created. This can be done by running 
`poetry env use python3.6` from the root dir of the repo. Doing so creates a python3.6 based venv located in the 
`.venv/` dir of the repo. Python3.6 is used as it is still in use in older still maintained Ubuntu versions which are 
often used for deep learning.

If you now have poetry and a project venv running you can install the project with `poetry install` and start coding :).
The default installation of poetry installs the project in dev mode with all its dev dependencies.

# Useful commands

## building executable

`poetry run tox -e py36-package` builds the project as a single file executable compatible with python 
interpreters >= 3.6 in the `dist/` dir. It can be run like every other binary on linux machines by calling 
`./dist-dl-train.pyz` in the dir.

## adding dependencies

`poetry add [-D] <name_of_package>`

## running tests

`poetry run tox -q`

## linting

You can run the linting tools either by running `./lint.sh` or through a cleanly build tox venv with 
`poetry run tox -e py36-lint`.

# Tests

The project uses `pytest` as test framework. See their [documentation](https://docs.pytest.org/en/6.2.x/contents.html#)
for more information about how to use it.

Tests are named based on the [plain English](https://enterprisecraftsmanship.com/posts/you-naming-tests-wrong/) schema
with a leading "test_" (needed for test discovery) and are placed in the `tests/` dir. This means that for example the
test function itself does not contain the name of the function it tests. Therefore, multiple tests for a specific
function are grouped by the creation of a test module for each function as otherwise the scope of the test module is
unclear. A simple example for those conventions is:

Example code from the `installer.py` module
```python
def build_installer_archive(...):
    pass
```

Corresponding test module `tests/installer/test_build_installer_archive.py`
```python
def test_creates_a_valid_archive(...):
    pass

def test_archive_contains_metadata_json(...):
    pass

def test_env_in_archive_is_runnable(...):
    pass

...
```

# running tests for different python versions

To check if the code really runs on python >=3.6 and <= 3.10 tox is used. It gets installed as a dev dependencies by
poetry and can than be used by calling `tox [-q]` (`-q` reduces the text noise between the test).
