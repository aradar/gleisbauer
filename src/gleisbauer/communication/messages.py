import datetime
import logging
import platform
import traceback
from logging import LogRecord

from dataclasses import dataclass

import gleisbauer.utils.json_ as json
from gleisbauer.errors import RemoteException
from gleisbauer.utils.io_ import fully_qualified_name
from gleisbauer.utils.typing_ import (
    Dict,
    List,
    Optional,
    Protocol,
    TextIO,
    runtime_checkable,
)


class MessageReader:
    def __init__(
        self, stream: TextIO, raise_exceptions: bool = True, pipe_logs: bool = True
    ):
        self._stream = stream
        self._raise_exceptions = raise_exceptions
        self._pipe_logs = pipe_logs
        self.line_buffer = list()

    def read(self) -> Optional["Message"]:
        line = self._stream.readline()
        return self._read(line)

    def readlines(self) -> List["Message"]:
        lines = self._stream.readlines()
        msgs = [self._read(l) for l in lines]
        msgs = [m for m in msgs if m is not None]
        return msgs

    def _read(self, line: str) -> Optional["Message"]:
        if not line:
            return None

        # received a message which has been separated by network packages, a filled
        # buffer or is malformed
        if not line.endswith("\n"):
            self.line_buffer.append(line)
            return None

        if self.line_buffer:
            conc_line = "".join(self.line_buffer) + line
            msg = json.loads(conc_line)
            self.line_buffer.clear()
        else:
            msg = json.loads(line)

        return None if self._handle_special_msg(msg) else msg

    def _handle_special_msg(self, msg: "Message") -> bool:
        if self._pipe_logs and isinstance(msg, LogRecordMsg):
            msg.log()
        elif self._raise_exceptions and isinstance(msg, ExceptionMsg):
            raise RemoteException(msg.value, msg.value_is_traceback)
        else:
            return False

        return True


class MessageWriter:
    def __init__(self, stream: TextIO):
        self._stream = stream

    def write(self, msg: "Message"):
        self._stream.write(json.dumps(msg))
        self._stream.write("\n")
        self._stream.flush()


@runtime_checkable
class Message(json.Serializable, Protocol):
    def handle(self, writer: MessageWriter):
        raise NotImplementedError()

    def _asdict(self) -> Dict:
        return vars(self)


class InfoMsg(Message):
    def handle(self, writer: MessageWriter):
        pass


@dataclass
class ExceptionMsg(InfoMsg):
    value: str
    value_is_traceback: bool = True


@dataclass
class LogRecordMsg(InfoMsg):
    name: str
    level: int
    pathname: str
    lineno: int
    msg: str
    func_name: str
    created: datetime.datetime
    exc_type_name: Optional[str] = None
    exc_traceback: Optional[str] = None

    def log(self):
        logger = logging.getLogger(self.name)
        logger.handle(self.as_record())

    @classmethod
    def from_record(cls, record: logging.LogRecord) -> "LogRecordMsg":
        exc_type_name = None
        exc_traceback = None
        if record.exc_info:
            exc_t, exc_v, exc_tb = record.exc_info
            exc_type_name = fully_qualified_name(exc_v)
            exc_traceback = traceback.format_exception(exc_t, value=exc_v, tb=exc_tb)

        return cls(
            name=f"{platform.node()}.{record.name}",
            level=record.levelno,
            pathname=record.pathname,
            lineno=record.lineno,
            msg=record.msg if record.args is None else record.msg % record.args,
            func_name=record.funcName,
            exc_type_name=exc_type_name,
            exc_traceback=exc_traceback,
            created=datetime.datetime.fromtimestamp(record.created),
        )

    def as_record(self) -> logging.LogRecord:
        return LogRecord(
            name=self.name,
            level=self.level,
            pathname=self.pathname,
            lineno=self.lineno,
            msg=self.msg,
            args=None,
            exc_info=None,
            func=self.func_name,
        )


class RemoteLoggingHandler(logging.Handler):
    def __init__(self, writer: MessageWriter):
        super().__init__()
        self._writer = writer

    def emit(self, record: LogRecord):
        self._writer.write(LogRecordMsg.from_record(record))
