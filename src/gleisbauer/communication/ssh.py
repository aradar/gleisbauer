import base64
import getpass
import logging
import threading
import time
from pathlib import Path
from queue import Queue

import fabric
from scp import SCPClient

import gleisbauer.utils.json_ as json
from gleisbauer.communication import messages
from gleisbauer.errors import RemoteProcessException, RemoteException
from gleisbauer.utils import xdg, zipapp
from gleisbauer.utils.io_ import RWStringIO
from gleisbauer.utils.typing_ import (
    BinaryIO,
    Iterator,
    List,
    NamedTuple,
    Optional,
    Sequence,
    TextIO,
    Tuple,
    Union,
)


logger = logging.getLogger(__name__)


def _build_distribution_tree(num_nodes: int) -> List[List[int]]:
    if num_nodes == 0:
        return []
    if num_nodes == 1:
        return [[1]]
    if num_nodes == 2:
        return [[1, 2]]

    tree: List[List[int]] = []
    i = 0
    indices = list(range(1, num_nodes + 1))
    while indices:
        num_new_lists = 2**i - len(tree)
        tree += [[] for _ in range(min(num_new_lists, len(indices) - num_new_lists))]

        for k in range(min(len(tree), len(indices))):
            tree[k].append(indices.pop(0))

        i += 1

    return tree


class SSHConnectionInfo(NamedTuple):
    hostname: str
    port: int = 22
    user: str = ""

    def as_connection(self, forward_agent: bool = False) -> "fabric.Connection":
        return fabric.Connection(
            host=self.hostname,
            user=self.user,
            port=self.port,
            forward_agent=forward_agent,
        )

    def as_string(self) -> str:
        return f"{self.user}@{self.hostname}:{self.port}"

    @staticmethod
    def from_string(value: str) -> "SSHConnectionInfo":
        user = getpass.getuser()
        port = 22
        if "@" in value:
            user, value = value.split("@", maxsplit=1)
        if ":" in value:
            value, port_str = value.split(":", maxsplit=1)
            port = int(port_str)
        hostname = value

        return SSHConnectionInfo(hostname=hostname, port=port, user=user)


def _path_exists_on_remote(
    path: Path, connection: "fabric.Connection", size: int = -1
) -> bool:

    exists = not connection.run(
        f"test -e {path.as_posix()}", hide=True, warn=True
    ).failed

    if exists and size > -1:
        remote_size = int(
            connection.run(f'stat --printf="%s" {path.as_posix()}', hide=True)
            .stdout.strip()
            .rstrip("\n")
        )

        if size != remote_size:
            # todo: log this
            return False

    return exists


def _mkdir_on_remote(
    path: Path,
    connection: "fabric.Connection",
    mode: int = 0o755,
    parents: bool = False,
    exist_ok: bool = False,
):

    if not parents:
        if not _path_exists_on_remote(path=path.parent, connection=connection):
            raise FileNotFoundError(f"Parent of {path.as_posix()} doesn't exist!")

    if not exist_ok:
        if _path_exists_on_remote(path=path, connection=connection):
            raise FileExistsError(
                f"File {path.as_posix()} already exists on {connection.host}"
            )
    else:
        if _path_exists_on_remote(path=path, connection=connection):
            return

    if parents:
        cmd = f"mkdir --parents --mode {mode:o} {path.as_posix()}"
    else:
        cmd = f"mkdir --mode {mode:o} {path.as_posix()}"

    connection.run(cmd)


def _cp_to_remote(
    local: Union[Path, BinaryIO],
    remote: Path,
    connection: "fabric.Connection",
    skip_if_exists: bool = False,
):

    sftp_client = connection.sftp()

    if skip_if_exists:
        try:
            remote_stats = sftp_client.stat(remote.as_posix())
            local_stats = local.stat()
            if remote_stats.st_size == local_stats.st_size:
                return
        except FileNotFoundError:
            pass

    # workaround as the default sftp client is super slow (for a loopback copy 220
    # MBit/s), but it is also not that great (manages 620 MBit/s)
    scp_client = SCPClient(connection.client.get_transport())
    if isinstance(local, Path):
        scp_client.put(files=local.as_posix(), remote_path=remote.as_posix())
    else:
        scp_client.putfo(fl=local, remote_path=remote.as_posix())

    sftp_client.chmod(remote.as_posix(), local.stat().st_mode)

    # alternative tried versions:

    # buffer_size = 128 * 4096
    #
    # with sftp_client.file(filename=remote.as_posix(), mode="wb", bufsize=0) as file:
    #     file.set_pipelined(True)
    #
    #     if isinstance(local, Path):
    #         with local.open("rb") as l_file:
    #             buffer = l_file.read(buffer_size)
    #             while buffer:
    #                 file.write(buffer)
    #                 buffer = l_file.read(buffer_size)
    #     else:
    #         buffer = local.read(buffer_size)
    #         while buffer:
    #             file.write(buffer)
    #             buffer = local.read(buffer_size)
    #
    #     file.chmod(local.stat().st_mode)

    # if isinstance(local, Path):
    #     sftp_client.put(localpath=local.as_posix(), remotepath=remote.as_posix())
    # else:
    #     sftp_client.putfo(fl=local, remotepath=remote.as_posix())
    # sftp_client.chmod(remote.as_posix(), local.stat().st_mode)


def _ln_on_remote(
    source: Path,
    link: Path,
    connection: "fabric.Connection",
):

    connection.run(f"ln -s {source.as_posix()} {link.as_posix()}", hide=True)


def _get_remote_env(connection: "fabric.Connection") -> xdg.Environ:
    result: "fabric.Result" = connection.run("env", hide=True)
    env_lines = result.stdout.split("\n")
    env = dict()
    for line in env_lines:
        if line:
            name, value = line.split("=", maxsplit=1)
            env[name] = value
    return env


class SSHRemoteProcess:
    def __init__(
        self,
        node: SSHConnectionInfo,
        result_queue: "Queue[fabric.Result]",
        runner_thread: threading.Thread,
        out_stream: RWStringIO,
    ):
        self._node = node
        self._result_queue = result_queue
        self._runner_thread = runner_thread
        self._result: Optional[fabric.Result] = None
        self._out_stream = out_stream

    @property
    def node(self):
        return self._node

    def join(self, timeout: Optional[float] = None):
        self._runner_thread.join(timeout)

        if not self.is_alive() and self._result is None:
            self._result = self._result_queue.get()

        if self.exit_code() != 0:
            # read from_start to find potential exception messages
            self.read(from_start=True)

    def is_alive(self) -> bool:
        return self._runner_thread.is_alive()

    def msg_reader(self, from_start: bool = False) -> messages.MessageReader:
        if from_start:
            self._out_stream.reset_read_pos()
        return messages.MessageReader(self._out_stream)

    def exit_code(self) -> int:
        if self.is_alive() and self._result_queue.empty():
            raise ValueError(
                "exit_code can only be accessed if the SSHRemoteProcess finished! Call"
                " join and wait for it to finish first."
            )

        if self._result is None:
            # if the result_queue just returns None the job has been disowned and
            # returned without knowing the true exit code. in this case a 0 is
            # assumed.
            return 0

        return self._result.exited

    def __del__(self):
        if self.is_alive():
            self._runner_thread.join()

    @classmethod
    def mirror_stdouts(cls, processes: List["SSHRemoteProcess"], delay: float = 0.2):
        readers = [p.msg_reader() for p in processes]

        while any([p.is_alive() for p in processes]):
            for r in readers:
                msg = r.read()
                if msg:
                    # todo: maybe give the function a stdout stream as argument
                    print(json.dumps(msg))

            time.sleep(delay)

    @classmethod
    def join_all(
        cls, processes: Sequence["SSHRemoteProcess"], timeout: Optional[float] = None
    ):
        for p in processes:
            p.join(timeout)

    def read(self, from_start: bool = False) -> List[messages.Message]:
        reader = self.msg_reader(from_start)
        try:
            read_msgs = reader.readlines()
        except RemoteException as e:
            raise RemoteProcessException(self.node, e.value)

        return read_msgs

    @classmethod
    def read_all(
        cls,
        processes: Sequence["SSHRemoteProcess"],
        join_before: bool = False,
    ) -> List[List[messages.Message]]:

        if join_before:
            cls.join_all(processes)

        if any([p.is_alive() for p in processes]):
            raise ValueError(
                "read_all can only be called after all process stopped running!"
            )

        return [p.read() for p in processes]


def _send_msg(
    msg: messages.Message,
    node: SSHConnectionInfo,
    forward_agent: bool = True,
    disown: bool = False,
) -> SSHRemoteProcess:
    """
    Sends a Message object to the given node via SSH and returns a SSHRemoteProcess
    object which allows checking its status and stdout.

    If forward_agent is set the current ssh key agent gets forwarded and allows the
    remote to authenticate new connections through it. The keys itself never leave the
    machine where they are originally stored but an auth request will be sent back
    to the original ssh agent. This means that a rogue root user on the node can also
    use this forwarded agent to connect as the current user to other nodes and install
    additional auth keys!

    If disown is set the returned SSHRemoteProcess contains no messages and is already
    no longer alive as it gets returned. But this also means that the process spawned
    by this will run until it finishes even if the SSH session gets disconnected.
    """

    def _run(encoded_msg: str, result_q: Queue, out_stream: TextIO, disown: bool):
        with node.as_connection(forward_agent=forward_agent) as con:
            target_runtime = zipapp.remote_runtime_path(_get_remote_env(con))
            result_q.put(
                con.run(
                    f"{target_runtime} remote-client {encoded_msg}",
                    warn=True,
                    out_stream=out_stream if not disown else None,
                    disown=disown,
                )
            )

    result_q: Queue[fabric.Result] = Queue()
    out_stream = RWStringIO()
    thread = threading.Thread(
        target=_run,
        args=(
            base64.b64encode(json.dumps(msg).encode()).decode(),
            result_q,
            out_stream,
            disown,
        ),
        daemon=True,
    )
    thread.start()

    proc = SSHRemoteProcess(node, result_q, thread, out_stream)

    # this prevents dangling threads as a disowned proc instantly returns and does
    # not really need a thread to run it but keeps the API cleaner
    if disown:
        proc.join()

    return proc


class DistributeDataMsg(messages.Message):
    def __init__(
        self,
        nodes: List[str],
        dist_idx: int,
        dist_tree: List[List[int]],
        files: List[Path],
    ):

        self.nodes = nodes
        self.dist_idx = dist_idx
        self.dist_tree = dist_tree
        self.files = files

    def handle(self, writer: messages.MessageWriter):
        logger.debug("DistributeDataMsg handling")

        # todo: if at some point an alternative NodeCommunicator implementation
        #  exists and NodeCommunicator is just the interface the instance creation
        #  has to be changed.
        com = SSHNodeCommunicator()
        for n in self.nodes:
            com.add_node_from_str(n)

        com._node_distribute_data(
            self.dist_idx, self.dist_tree, [Path(f) for f in self.files]
        )


class SSHNodeCommunicator:
    def __init__(self):
        self._nodes: List[SSHConnectionInfo] = []

    @property
    def primary_node(self) -> SSHConnectionInfo:
        return self._nodes[0]  # todo: replace this with a smarter logic

    @property
    def worker_nodes(self) -> List[SSHConnectionInfo]:
        return self._nodes[1:]  # todo: replace this with a smarter logic

    @property
    def nodes(self) -> List[SSHConnectionInfo]:
        return self._nodes.copy()

    def add_node(self, hostname: str, port: int = 22, user: str = None):
        self._nodes.append(
            SSHConnectionInfo(hostname, port, user if user else getpass.getuser())
        )

    def add_node_from_str(self, node_str: str):
        self._nodes.append(SSHConnectionInfo.from_string(node_str))

    def distribute_runtime(self) -> Iterator[Tuple[int, int]]:
        finished_transfers = 0

        for node in self._nodes:
            with node.as_connection() as con:
                env = _get_remote_env(con)
                local_runtime = zipapp.get_zipapp_path()
                target_runtime = zipapp.remote_runtime_path(env)

                if not _path_exists_on_remote(target_runtime.parent, con):
                    _mkdir_on_remote(target_runtime.parent, con, parents=True)
                _cp_to_remote(
                    local=local_runtime, remote=target_runtime, connection=con
                )

                yield finished_transfers, len(self._nodes)
                finished_transfers += 1

    def mkdir_on_all(self, path: Path, mode: int = 0o755, exist_ok: bool = True):
        for node in self.nodes:
            with node.as_connection() as con:
                _mkdir_on_remote(
                    path=path, connection=con, mode=mode, exist_ok=exist_ok
                )
                logger.debug(
                    f"created dir {path.as_posix()} with mode {mode} and exist_ok"
                    f" {exist_ok}"
                )

    def ln_on_all(self, source: Path, link: Path):
        for node in self.nodes:
            with node.as_connection() as con:
                _ln_on_remote(source, link, con)
                logger.debug(
                    f"created symlink from {source.as_posix()} to {link.as_posix()}"
                )

    def cp(self, node: SSHConnectionInfo, cp_jobs: Sequence[Tuple[Path, Path]]):
        with node.as_connection() as con:
            for local, remote in cp_jobs:
                _mkdir_on_remote(remote.parent, con, parents=True, exist_ok=True)
                _cp_to_remote(local, remote, con, skip_if_exists=True)
                logger.debug(f"copied from {local.as_posix()} to {remote.as_posix()}")

    def cp_to_all_from_client(
        self, files_to_copy: List[Tuple[Path, Path]]
    ) -> Iterator[Tuple[int, int]]:

        # todo: change this to a function which only transfers from node to node

        # todo: can this files_to_copy tuple logic be dropped? wouldn't a rel file
        #  with a remote parent be enough and smarter?

        # todo: this is a bit broken with the introduction of a primary node

        if not self._nodes:
            raise ValueError(
                "NodeCommunicator instance has no registered nodes at this moment!"
            )

        finished_transfers = 0
        start_node = self._nodes[0]
        dist_tree = _build_distribution_tree(len(self._nodes) - 1)

        logger.debug(f"sending {len(files_to_copy)} files to {start_node.as_string()}")

        self.cp(start_node, files_to_copy)

        yield finished_transfers, len(self._nodes)
        finished_transfers += 1

        if len(self._nodes) > 1:
            msg = DistributeDataMsg(
                dist_idx=0,
                nodes=[n.as_string() for n in self._nodes],
                dist_tree=dist_tree,
                files=[t[1] for t in files_to_copy],
            )

            process = _send_msg(msg, start_node)

            reader = process.msg_reader()
            while process.is_alive():
                res_msg = reader.read()
                if res_msg:
                    yield finished_transfers, len(self._nodes)
                    finished_transfers += 1

            process.join()

    def send(
        self,
        node: SSHConnectionInfo,
        msg: messages.Message,
        forward_agent: bool = True,
        disown: bool = False,
    ) -> SSHRemoteProcess:
        # todo: move to forward_agent = False by default
        # todo: better primary node selection would be cool
        return _send_msg(msg, node, forward_agent, disown)

    def send_to_primary(
        self, msg: messages.Message, forward_agent: bool = True, disown: bool = False
    ) -> SSHRemoteProcess:
        # todo: move to forward_agent = False by default
        # todo: better primary node selection would be cool
        return _send_msg(msg, self.primary_node, forward_agent, disown)

    def send_to_workers(
        self, msg: messages.Message, forward_agent: bool = True, disown: bool = False
    ) -> List[SSHRemoteProcess]:
        # todo: move to forward_agent = False by default
        # todo: better primary node selection would be cool
        return [_send_msg(msg, n, forward_agent, disown) for n in self.worker_nodes]

    def send_to_all(
        self, msg: messages.Message, forward_agent: bool = True, disown: bool = False
    ) -> List[SSHRemoteProcess]:
        # todo: move to forward_agent = False by default
        return [_send_msg(msg, n, forward_agent, disown) for n in self._nodes]

    def _node_distribute_data(
        self, dist_idx: int, dist_tree: List[List[int]], files: List[Path]
    ):
        if dist_idx >= len(dist_tree):
            return

        file_pairs = [(f, f) for f in files]

        processes = []
        target_indices = dist_tree[dist_idx]
        targets_nodes = [self._nodes[i] for i in target_indices]
        for node, i in zip(targets_nodes, target_indices):
            self.cp(node, file_pairs)

            if i < len(dist_tree):
                msg = DistributeDataMsg(
                    dist_idx=i,
                    nodes=[n.as_string() for n in self._nodes],
                    dist_tree=dist_tree,
                    files=files,
                )

                processes.append(_send_msg(msg, node))

        SSHRemoteProcess.mirror_stdouts(processes)
