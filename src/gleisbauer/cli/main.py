import shlex
import sys

from gleisbauer.cli import local, remote


def bootstrap_modules():
    # todo: maybe there is a better way to do this
    import gleisbauer.container as container

    container.set_default_engine(container.DockerContainerEngine())


def main():
    bootstrap_modules()

    # todo: changing sys.argv maybe changes the name argparse shows in the cli

    args = sys.argv
    args.pop(0)

    if len(args) > 0 and args[0].lower() == "remote-client":
        args.pop(0)
        exit(remote.main(args))

    splitted_args = shlex.split(
        " ".join([a if " " not in a else f'"{a}"' for a in args])
    )
    entrypoint_args = None
    if "--" in splitted_args:
        cutoff = splitted_args.index("--")
        entrypoint_args = " ".join(splitted_args[cutoff + 1 :])
        args = splitted_args[:cutoff]

    exit(local.main(args, entrypoint_args))


if __name__ == "__main__":
    main()
