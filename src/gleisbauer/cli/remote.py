import base64
import logging
import os
import platform
import sys
import traceback
from pathlib import Path
from typing import List

import gleisbauer.utils.json_ as json
from gleisbauer.communication.messages import (
    ExceptionMsg,
    Message,
    MessageWriter,
    RemoteLoggingHandler,
)
from gleisbauer.utils.io_ import volatile_tmp_home
from gleisbauer.utils.project import proj_bin_name


def main(args: List[str]) -> int:
    with Path("/tmp/what").open("w") as f:
        f.write("what")

    root_logger = logging.getLogger()
    root_logger.setLevel(0)  # todo: set this somehow

    writer = MessageWriter(sys.stdout)
    root_logger.addHandler(RemoteLoggingHandler(writer))

    logger = logging.getLogger(proj_bin_name())
    try:
        # test if the stdout pipe is writeable, if not logging is moved to a file
        logger.info(f"starting remote client on {platform.node()}")
    except BrokenPipeError:
        root_logger.handlers.clear()
        log_file_io = (volatile_tmp_home() / f"client_{os.getpid()}.log").open("w")
        writer = MessageWriter(log_file_io)
        root_logger.addHandler(RemoteLoggingHandler(writer))
        logger.info(f"starting remote client on {platform.node()}")

    try:
        message_json = base64.b64decode("".join(args)).decode()
        message: Message = json.loads(message_json)
    except Exception:
        writer.write(ExceptionMsg(traceback.format_exc()))
        return 1

    if not isinstance(message, Message):
        writer.write(
            ExceptionMsg(
                "Received json message is not an instance of a object which implements"
                " the Message protocol!",
                False,
            )
        )
        return 1

    try:
        message.handle(writer)
    except Exception:
        writer.write(ExceptionMsg(traceback.format_exc()))
        return 1

    return 0
