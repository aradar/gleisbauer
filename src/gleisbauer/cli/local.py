import argparse
import getpass
import logging
import subprocess
import sys
from enum import Enum
from pathlib import Path

from gleisbauer.communication.ssh import SSHNodeCommunicator
from gleisbauer.experiments.mgmt import (
    run_exp,
    init_exp, query_nodes, query_exp, pull_exp,
)
from gleisbauer.utils import project, zipapp
from gleisbauer.utils.typing_ import List, NamedTuple, Sequence, Tuple


def _override_zipapp_path_handling(pkg_cmd: str, dist_dir: Path, rebuild_runtime: bool):
    zipapp_files = list(dist_dir.glob("*.pyz"))

    if rebuild_runtime and zipapp_files:
        for file in zipapp_files:
            file.unlink()
        zipapp_files.clear()

    if not zipapp_files:
        result = subprocess.run(pkg_cmd.split(" "), stdout=subprocess.DEVNULL)

        if result.returncode != 0:
            raise RuntimeError(
                "Building of the zipapp failed! Please run the building "
                "command manually and check the error."
            )
    else:
        pass
        # todo: add a logger entry that an existing runtime package gets reused

    zipapp_files = list(dist_dir.glob("*.pyz"))
    if len(zipapp_files) > 1:
        raise RuntimeError(
            "The runtime dir contains more than one runtime! Please clean it and retry."
        )

    zipapp_file = zipapp_files[0]

    def _get_dev_zipapp_path():
        return zipapp_file

    zipapp.get_zipapp_path = _get_dev_zipapp_path


# todo: maybe just keep the one in communication.ssh?
class NodeConfig(NamedTuple):
    user: str
    hostname: str
    port: int = 22

    @classmethod
    def parse_cli_arg(cls, cli_value: str) -> "NodeConfig":
        user_sep = "@"
        port_sep = ":"

        remaining_args = cli_value

        user = getpass.getuser()
        if user_sep in remaining_args:
            [user, remaining_args] = remaining_args.split(user_sep, maxsplit=1)

        port = 22
        if port_sep in remaining_args:
            [remaining_args, port_str] = remaining_args.split(port_sep, maxsplit=1)
            port = int(port_str)

        for c in [user_sep, port_sep]:
            if c in remaining_args:
                raise ValueError()  # todo: write an error message

        hostname = remaining_args

        if (
            hostname.lower() == "localhost"
            or hostname.startswith("127.")
            or hostname == "::1"
            or hostname == "0:0:0:0:0:0:0:1"
        ):
            raise argparse.ArgumentTypeError(
                f"hostname {hostname} points to a loopback device and can't be used as"
                " a node hostname!"
            )

        # todo: maybe add a connection check here to see if connecting to the
        #  hostname and port works

        return NodeConfig(user=user, hostname=hostname, port=port)


class Task(Enum):
    init = "init"
    query_nodes = "query-nodes"
    query_exp = "query-exp"
    run = "run"
    pull = "pull"

    def __str__(self) -> str:
        return self.name.replace("_", "-")


class Arguments(NamedTuple):
    task: Task
    nodes: List[NodeConfig] = []
    num_gpus: int = 0
    gpu_min_mem: int = 0
    no_job_queuing: bool = False
    dev_runtime_pkg_cmd: str = ""
    dev_runtime_dist_dir: str = ""
    dev_always_rebuild_runtime: bool = False

    @classmethod
    def parser(cls) -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser(prog=project.proj_bin_name())

        parser.add_argument("task", type=Task, choices=list(Task))

        parser.add_argument(
            "nodes",
            type=NodeConfig.parse_cli_arg,
            nargs="*",
        )
        parser.add_argument(
            "--num-gpus",
            type=int,
            default=0,
        )
        parser.add_argument(
            "--gpu-min-mem",
            type=int,
            default=0,
        )
        parser.add_argument("--no-job-queuing", action="store_true", default=False)

        parser.add_argument("--dev-runtime-pkg-cmd", type=str, default="")
        parser.add_argument("--dev-runtime-dist-dir", type=str, default="")
        parser.add_argument(
            "--dev-always-rebuild-runtime", action="store_true", default=False
        )

        return parser

    @classmethod
    def from_args(
        cls, args: Sequence[str]
    ) -> Tuple[argparse.ArgumentParser, "Arguments"]:
        parser = cls.parser()
        return parser, Arguments(**parser.parse_args(args=args).__dict__)


def main(args: List[str], entrypoint_args: str) -> int:
    parser, parsed_args = Arguments.from_args(args)

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(
        "[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s"
    )
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)

    logger = logging.getLogger(__file__)

    def filter_log_records(record: logging.LogRecord) -> bool:
        name = record.name
        if name.startswith(project.proj_bin_name()):
            return True
        if "." in name:
            name = name.split(".", maxsplit=1)[1]
            if name.startswith(project.proj_bin_name()):
                return True

        return False

    handler.addFilter(filter_log_records)

    if parsed_args.dev_runtime_pkg_cmd:
        _override_zipapp_path_handling(
            parsed_args.dev_runtime_pkg_cmd,
            Path(parsed_args.dev_runtime_dist_dir),
            parsed_args.dev_always_rebuild_runtime,
        )

    if parsed_args.task is Task.init:
        init_exp()
    else:
        if not parsed_args.nodes:
            parser.print_help()
            return 1  # todo: print an error?

        communicator = SSHNodeCommunicator()
        for n in parsed_args.nodes:
            communicator.add_node(hostname=n.hostname, port=n.port, user=n.user)

        for i, _ in communicator.distribute_runtime():
            logger.info(f"distributed runtime to {parsed_args.nodes[i]}")

        if parsed_args.task is Task.query_nodes:
            query_nodes(communicator)
        elif parsed_args.task is Task.query_exp:
            query_exp(communicator)
        elif parsed_args.task is Task.run:
            run_exp(
                communicator,
                num_gpus=parsed_args.num_gpus,
                gpu_min_mem=parsed_args.gpu_min_mem,
                no_job_queuing=parsed_args.no_job_queuing,
                entrypoint_args=entrypoint_args,
            )
        elif parsed_args.task is Task.pull:
            pull_exp(communicator)
        else:
            raise NotImplementedError()

    return 0
