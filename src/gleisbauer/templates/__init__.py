import re
from pathlib import Path

from gleisbauer.utils.typing_ import Any, Dict


def read(name: str, template_kwargs: Dict[str, Any] = None) -> str:
    def _replace(orig_str: str, placeholder: str, value: str) -> str:
        return re.sub(f"{{{{ *{placeholder} *}}}}", value, orig_str)

    content = Path(__file__).with_name(name).read_text()

    if template_kwargs:
        for key, value in template_kwargs.items():
            content = _replace(content, key, value)

    return content
