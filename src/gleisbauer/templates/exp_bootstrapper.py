#!/usr/bin/env python3

import argparse
import datetime
import logging
import os
import shlex
import subprocess
import sys
import time
from pathlib import Path
import socket
from typing import Any, Sequence

sshd_pid_file = Path("/var/tmp/sshd.pid")
exp_dir = Path("/experiment")
bootstrapper_log_file = Path("/experiment/out/bootstrapper.log")


logging.basicConfig(
    filename=bootstrapper_log_file.as_posix(),
    level=logging.INFO,
    format="[%(asctime)s] [%(process)d] [%(levelname)s]: %(message)s",
)


def stop_worker(host: str) -> bool:
    """
    Calls the bootstrapper with the stop subcommand on the given worker by
    connecting to it via ssh.

    This terminates the running sshd daemon as well as the bootstrapper process
    which results in the clean termination of the container.
    """

    file = Path(__file__).resolve().as_posix()
    args = f"ssh {host} `cd /; {file} stop`"
    proc = subprocess.run(args=args.split(" "))
    return proc.returncode == 0


def start_sshd(port: int, timeout: float = 5.0):
    home = Path.home()
    cmd = (
        "/usr/sbin/sshd "
        f"-h {(home / '.ssh/host_rsa_key').as_posix()} "
        f"-f {(home / '.ssh/sshd_config').as_posix()} "
        f"-o PidFile={sshd_pid_file.as_posix()} "
        f"-p {port}"
    )
    subprocess.run(
        args=shlex.split(cmd),
        check=True,
    )

    start = datetime.datetime.now()
    while not sshd_pid_file.exists():
        time.sleep(1)
        if (datetime.datetime.now() - start).total_seconds() >= timeout:
            raise TimeoutError(
                f"sshd failed to create its pid file in {timeout} seconds!"
            )


def reachable(host: str, port: int, delay: float = 15.0, max_tries: int = 5) -> bool:
    for i in range(max_tries):
        try:
            logging.info(
                f"testing if {host}:{port} is reachable (try {i + 1} of"
                f" {max_tries}) ..."
            )
            test_socket = socket.create_connection(address=(host, port))
            test_socket.close()
            logging.info(f"{host}:{port} is reachable.")
            return True
        except ConnectionRefusedError:
            logging.info(
                f"{host}:{port} is currently not reachable trying again in"
                f" {delay} seconds."
            )
            time.sleep(delay)

    logging.warning(f"{host}:{port} is not reachable!")
    return False


def horovodrun(
    hosts: Sequence[str],
    num_gpus: Sequence[int],
    excluded_if_names: Sequence[str],
    entrypoint_file: Path,
    entrypoint_args: str = None,
) -> subprocess.Popen:

    # todo: lars, lamb and adasum tests for big batch sizes

    # todo: add support for the following optional arguments. probably more can
    #  be build by prefixing the cli flag with HOROVOD_
    #   - HOROVOD_START_TIMEOUT
    #   - HOROVOD_FUSION_THRESHOLD=33554432
    #   - HOROVOD_CYCLE_TIME=3.5
    #   - HOROVOD_TIMELINE_MARK_CYCLES = 1
    #   - HOROVOD_AUTOTUNE=1
    #   - HOROVOD_AUTOTUNE_LOG=/tmp/autotune_log.csv
    #   - ...

    # mpirun gets called instead of horovodrun as otherwise worker hosts with
    # different network interface names cause an error. horovodrun introduced the
    # --network-interface arg to solve this, but this only allows horovodrun to
    # ignore the problem and the mpi process triggered by it crashes than a few
    # seconds later.

    # the problem is apparently that non routable interface like the loopback
    # interface or docker interface cause mpi to crash. to solve this mpirun gets
    # started with a list of interfaces to exclude instead of a list of interfaces to
    # include like horovodrun does it.

    if num_gpus:
        num_procs = num_gpus
    else:
        num_procs = [1] * len(hosts)

    procs_sum = sum(num_procs)

    hosts_str = ",".join([f"{h}:{n}" for h, n in zip(hosts, num_procs)])

    cmd = (
        "mpirun "
        f"-np {procs_sum} "
        f"--host {hosts_str} "
        f"--mca btl_tcp_if_exclude {','.join(excluded_if_names)} "
        f"-x NCCL_SOCKET_IFNAME=^{','.join(excluded_if_names)} "
        "--bind-to none "
        "--map-by slot "
        "--mca pml ob1 "
        "--mca btl ^openib "
        "-x NCCL_DEBUG=INFO "
        "-x LD_LIBRARY_PATH "
        "-x PATH "
        f"{entrypoint_file.as_posix()}"
    )

    if entrypoint_args:
        cmd += f" {entrypoint_args}"

    return subprocess.Popen(
        args=shlex.split(cmd),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )


def primary_main(args: Any):
    logging.info("received a primary start command. starting sshd daemon now...")
    start_sshd(args.sshd_port)

    logging.info("starting task on nodes...")

    hosts = [args.primary_host]

    if args.secondary_hosts:
        hosts += args.secondary_hosts

        reachable_hosts = []
        for h, p in zip(args.secondary_hosts, args.secondary_ports):
            if reachable(h, p):
                reachable_hosts.append(h)
        if len(reachable_hosts) != len(args.secondary_hosts):
            not_reachable_hosts = list(
                set(reachable_hosts).difference(args.secondary_hosts)
            )
            for h in not_reachable_hosts:
                logging.error(
                    f"secondary host {h} not reachable. stopping sshd, container..."
                )
            logging.error(f"stopping sshd on other workers...")
            for h in reachable_hosts:
                stop_worker(h)
            stop_main(None)
            return

    proc = horovodrun(
        hosts,
        args.num_gpus_per_host,
        args.excluded_if_names,
        args.entrypoint_file,
        args.entrypoint_args,
    )

    # switching stdout and stderr of proc to non-blocking mode as otherwise reading
    # from one of them stalls everything until the process dies.
    os.set_blocking(proc.stdout.fileno(), False)
    os.set_blocking(proc.stderr.fileno(), False)

    # todo: write stdout and stderr to a different file and do the normal logging in
    #  a different file as text
    while proc.poll() is None:
        for line in proc.stdout:
            logging.info(line.rstrip("\n"))
        for line in proc.stderr:
            logging.warning(line.rstrip("\n"))
        # fast ticks to prevent overflows in the non blocking pipes
        time.sleep(0.1)

    for line in proc.stdout:
        logging.info(line.rstrip("\n"))
    for line in proc.stderr:
        logging.warning(line.rstrip("\n"))

    logging.log(
        logging.INFO if proc.returncode == 0 else logging.ERROR,
        f"primary worker process stopped with exit code {proc.returncode}",
    )

    logging.info("stopping ssh daemons on all nodes...")

    for host in hosts:
        try:
            stop_worker(host)
        except Exception:
            logging.exception(f"stopping worker {host} caused the following exception!")


def worker_main(args: Any):
    logging.info("received a worker start command. starting sshd daemon now...")
    start_sshd(args.sshd_port)

    while sshd_pid_file.exists():
        time.sleep(10)

    logging.info("sshd pid file no longer exists. exiting now...")


def stop_main(args: Any):
    logging.info("received a stop command. stopping now...")
    pid = int(sshd_pid_file.read_text())
    args = f"/usr/bin/kill {pid}"
    subprocess.run(args=args.split(" "))


def main():
    logging.info("starting bootstrapping")
    try:
        logging.info(sys.argv)

        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers()

        prim_parser = subparsers.add_parser("primary")
        prim_parser.add_argument("--sshd-port", type=int, required=True)
        prim_parser.add_argument("--primary-host", type=str, required=True)
        prim_parser.add_argument("--secondary-hosts", type=str, nargs="*")
        prim_parser.add_argument("--secondary-ports", type=int, nargs="*")
        prim_parser.add_argument("--num-gpus-per-host", type=int, nargs="*")
        prim_parser.add_argument("--excluded-if-names", type=str, nargs="*")
        prim_parser.add_argument("--entrypoint-file", type=Path, required=True)
        prim_parser.add_argument("--entrypoint-args", type=str)
        prim_parser.set_defaults(func=primary_main)

        worker_parser = subparsers.add_parser("worker")
        worker_parser.add_argument("--sshd-port", type=int, required=True)
        worker_parser.add_argument("--primary-host", type=str, required=True)
        worker_parser.add_argument("--primary-sshd-port", type=str, required=True)
        worker_parser.set_defaults(func=worker_main)

        stop_parser = subparsers.add_parser("stop")
        stop_parser.set_defaults(func=stop_main)

        args = parser.parse_args()
        logging.info(f"parsed the following args: {args}")
        args.func(args)
    except:
        logging.exception(
            "the following unhandled exception occurred during bootstrapping and caused"
            " it to fail."
        )
        logging.shutdown()
        exit(1)

    logging.info("bootstrapper stopped gracefully")
    logging.shutdown()
    exit(0)


if __name__ == "__main__":
    main()
