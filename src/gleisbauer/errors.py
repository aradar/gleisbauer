from gleisbauer.utils.typing_ import Any


class ConfigParseException(Exception):
    pass


class ExpInitException(Exception):
    pass


class RemoteException(Exception):
    value: Any
    value_is_stacktrace: bool

    def __init__(self, value: Any, value_is_stacktrace: bool):
        self.value = value
        self.value_is_stacktrace = value_is_stacktrace


class RemoteProcessException(Exception):
    node: Any
    remote_stacktrace: str

    def __init__(self, node, remote_stacktrace):
        self.node = node
        self.remote_stacktrace = remote_stacktrace

    def __str__(self) -> str:
        return (
            "Received the following exception during the execution of a remote process"
            f" on {self.node}:\n\n{self.remote_stacktrace}"
        )


class NotRunFromZipappException(Exception):
    pass


class AllPortsAllocatedException(Exception):
    pass
