import datetime
import getpass
import hashlib
import os
import random
import shutil
import tarfile
import threading
import time
from io import StringIO, UnsupportedOperation
from pathlib import Path
from types import TracebackType

from gleisbauer.utils import xdg
from gleisbauer.utils.project import proj_bin_name
from gleisbauer.utils.typing_ import (
    IO,
    Any,
    AnyStr,
    BinaryIO,
    Iterable,
    Iterator,
    List,
    NamedTuple,
    Optional,
    TextIO,
    Type,
    Literal,
    Tuple,
    Union,
)

BYTE_UNITS = Literal[
    "B",
    "KB",
    "MB",
    "GB",
    "TB",
    "PB",
    "EB",
    "ZB",
    "YB",
    "KiB",
    "MiB",
    "GiB",
    "TiB",
    "PiB",
    "EiB",
    "ZiB",
    "YiB",
]


def split_byte_unit_str(byte_unit_str: str) -> Tuple[int, BYTE_UNITS]:
    possible_units: List[BYTE_UNITS] = [
        "KB",
        "MB",
        "GB",
        "TB",
        "PB",
        "EB",
        "ZB",
        "YB",
        "KiB",
        "MiB",
        "GiB",
        "TiB",
        "PiB",
        "EiB",
        "ZiB",
        "YiB",
        "B",
    ]
    byte_unit_str = byte_unit_str.strip()
    for unit in possible_units:
        if byte_unit_str.endswith(unit):
            return int(byte_unit_str.replace(unit, "").strip()), unit

    return int(byte_unit_str), "B"


def byte_unit_multiplier(unit: BYTE_UNITS) -> int:
    base = 10
    pow_inc = 3
    if "i" in unit:
        base = 2
        pow_inc = 10

    order = ["k", "m", "g", "t", "p", "e", "z", "y"]
    i = order.index(unit.lower()[0]) + 1 if unit != "B" else 0

    return base ** (i * pow_inc)


def byte_unit_str_to_int(num_bytes: str) -> int:
    num, unit = split_byte_unit_str(num_bytes)
    multiplier = byte_unit_multiplier(unit)

    return num * multiplier


def convert_byte_unit(
    num: int, orig_unit: BYTE_UNITS, target_unit: BYTE_UNITS
) -> Union[int, float]:
    if orig_unit != "B":
        num = num * byte_unit_multiplier(orig_unit)
    return num / byte_unit_multiplier(target_unit)


def fits_on_disk(path: Path, num_bytes: int) -> bool:
    query_path = path.resolve()
    while not query_path.exists():
        query_path = query_path.parent

    total, _, free = shutil.disk_usage(query_path.as_posix())

    if total < num_bytes:
        return False

    if free < num_bytes:
        return False

    return True


def volatile_tmp_home() -> Path:
    tmp_dir = Path(f"/tmp/{proj_bin_name()}")
    return tmp_dir


def tmp_home() -> Path:
    tmp_dir = Path(f"/var/tmp/{proj_bin_name()}")
    return tmp_dir


def volatile_user_tmp_home() -> Path:
    tmp_dir = xdg.xdg_runtime_dir()
    if tmp_dir is None:
        user = getpass.getuser()
        tmp_dir = Path(f"/tmp/{proj_bin_name()}/{user}")

    return tmp_dir


def exists_in_tar(path: Path, tar: tarfile.TarFile) -> bool:
    """
    Helper function which checks if a given path exists in a TarFile object and if
    so returns true and otherwise false.
    """
    try:
        _ = tar.getmember(path.as_posix())
    except KeyError:
        return False
    return True


def mkdir_in_tar(
    path: Path,
    tar: tarfile.TarFile,
    uid: int = None,
    gid: int = None,
    mode: int = 0o755,
    parents: bool = False,
    exist_ok: bool = False,
):
    """
    Helper function to easily add a dir to a open TarFile object with a defined owner
    and mode.

    It has the same params as the normal python mkdir function. If a dir with the
    name/ path already exists and exist_ok is set the func returns without doing
    anything.
    """

    if exists_in_tar(path, tar):
        if exist_ok:
            return
        else:
            raise ValueError(
                f"A dir or file with name {path} already exists in the tar!"
            )

    if len(path.parts) > 1:
        parent_paths = list(path.parents)[:-1]
        parent_paths.reverse()

        if not parents:
            if not exists_in_tar(parent_paths[-1], tar):
                raise ValueError(f"Parent for {path} is missing in the tar!")
        else:
            for pp in parent_paths:
                mkdir_in_tar(pp, tar, uid, gid, mode, exist_ok=True)

    if uid is None:
        uid = os.getuid()
    if gid is None:
        gid = os.getgid()

    info = tarfile.TarInfo(path.as_posix())
    info.type = tarfile.DIRTYPE
    info.uid = uid
    info.gid = gid
    info.mode = mode
    info.mtime = int(datetime.datetime.now().timestamp())

    tar.addfile(info)


def path_is_relative_to(subject: Path, potential_parent: Path) -> bool:
    """
    Checks if the subject Path is a child as potential_parent and therefore can be
    expressed as a relative path to potential_parent.

    The potential_parent argument must point to a directory!
    """

    try:
        subject.relative_to(potential_parent)
    except ValueError:
        return False
    return True


def path_relation(subject: Path, potential_parent: Path) -> float:
    """
    Maps how strongly related two paths are to a float from 0.0 to 1.0.

    Both arguments must be Path instance which are pointing to directories as
    otherwise the result has no meaning!
    """

    try:
        rel_subject = subject.relative_to(potential_parent)
    except ValueError:
        return 0.0

    subject_depth = len(subject.parts)
    rel_subject_depth = len(rel_subject.parts)

    return (subject_depth - rel_subject_depth) / subject_depth


class FileLock:
    """
    Simple class which enables OS independent and pythonic exp_root locking. Acquiring
    of result exp_root journal_lock happens by successfully being able to create
    result .journal_lock exp_root with the same name and location as the target
    exp_root. An instance of the class can be used as result context manager and also
    allows getting result context manager for the exp_root itself after locking it
    with the help of the open method.
    """

    def __init__(
        self, file: Path, timeout: float = -1, seconds_between_tries: float = 0.5
    ):

        self.file = file
        self.lock_file = file.with_suffix(f"{file.suffix}.lock")
        self.is_locked = False
        self.seconds_between_tries = seconds_between_tries
        self.timeout = timeout

    @property
    def acquired(self) -> bool:
        return self.is_locked

    def acquire_lock(self):
        start = time.time()
        while not self.is_locked:
            try:
                with self.lock_file.open("x+") as f:
                    f.write(str(os.getpid()))
                    self.is_locked = True
            except FileExistsError:
                time.sleep(self.seconds_between_tries + random.uniform(0.0, 0.1))
                if self.timeout >= 0.0:
                    if (time.time() - start) > self.timeout:
                        raise TimeoutError()  # todo: custom error?

    def release_lock(self):
        if self.lock_file.exists():
            self.lock_file.unlink()
        self.is_locked = False

    def __enter__(self) -> "FileLock":
        if not self.is_locked:
            self.acquire_lock()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.is_locked:
            self.release_lock()

    def __del__(self):
        if self.is_locked:
            self.release_lock()

    @property
    def target(self) -> Path:
        return self.file

    def open_target(
        self,
        mode: str = "r",
        buffering: int = -1,
        encoding: str = None,
        errors: str = None,
        newline: str = None,
    ) -> IO[Any]:

        """
        Open the exp_root locked by this instance and returns result exp_root object, as
        the built-in open() function does.
        """

        if not self.is_locked:
            raise ValueError(
                "open can only be called after acquiring the journal_lock to the file!"
            )

        return self.file.open(
            mode=mode,
            buffering=buffering,
            encoding=encoding,
            errors=errors,
            newline=newline,
        )


class RWStringIO(TextIO):
    """
    Simple stream/ file-like object which supports reading and writing at different
    positions to a StringIO object in a thread safe manner.
    """

    def __init__(self):
        self._stream = StringIO()
        self._read_pos = 0
        self._write_pos = 0
        self._lock = threading.Lock()

    # region props and util funcs
    @property
    def buffer(self) -> BinaryIO:
        return self._stream.buffer

    @property
    def closed(self) -> bool:
        return self._stream.closed

    @property
    def errors(self) -> Optional[str]:
        return self._stream.errors

    @property
    def line_buffering(self) -> bool:
        return self._stream.line_buffering

    @property
    def newlines(self) -> Any:
        return self._stream.newlines

    # region file props
    @property
    def encoding(self) -> str:
        return self._stream.encoding

    @property
    def mode(self) -> str:
        return self._stream.mode

    @property
    def name(self) -> str:
        return self._stream.name

    def fileno(self) -> int:
        return self._stream.fileno()

    # endregion file props

    def flush(self) -> None:
        self._stream.flush()

    def isatty(self) -> bool:
        return self._stream.isatty()

    # endregion props and util funcs

    # region read functions
    def readable(self) -> bool:
        return True

    def read(self, size: int = -1) -> AnyStr:
        with self._lock:
            self._stream.seek(self._read_pos)
            value = self._stream.read(size)
            self._read_pos = self._stream.tell()
            return value

    def readline(self, size: int = -1) -> AnyStr:
        with self._lock:
            self._stream.seek(self._read_pos)
            value = self._stream.readline(size)
            self._read_pos = self._stream.tell()
            return value

    def readlines(self, hint: int = -1) -> List[AnyStr]:
        with self._lock:
            self._stream.seek(self._read_pos)
            value = self._stream.readlines(hint)
            self._read_pos = self._stream.tell()
            return value

    # endregion read functions

    # region write functions
    def writable(self) -> bool:
        return True

    def write(self, s: AnyStr) -> int:
        with self._lock:
            self._stream.seek(self._write_pos)
            value = self._stream.write(s)
            self._write_pos = self._stream.tell()
            return value

    def writelines(self, lines: Iterable[AnyStr]) -> None:
        with self._lock:
            self._stream.seek(self._write_pos)
            self._stream.writelines(lines)
            self._write_pos = self._stream.tell()

    # endregion write functions

    # region seek functions
    def seekable(self) -> bool:
        return False

    def seek(self, offset: int, whence: int = 0) -> int:
        raise UnsupportedOperation()

    def tell(self) -> int:
        raise UnsupportedOperation()

    def truncate(self, size: Optional[int] = None) -> int:
        raise UnsupportedOperation()

    # endregion seek functions

    # region iterator functions
    def __next__(self) -> str:
        return self.readline()

    def __iter__(self) -> Iterator[str]:
        return self

    # endregion iterator functions

    # region context manager functions
    def __enter__(self) -> TextIO:
        return self

    def close(self) -> None:
        self._stream.close()

    def __exit__(
        self,
        t: Optional[Type[BaseException]],
        value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> Optional[bool]:
        self.close()
        return True

    # endregion context manager functions

    # region non protocol funcs
    def reset_read_pos(self):
        self._read_pos = 0

    # endregion non protocol funcs


class DataPathInfo(NamedTuple):
    file: Path
    size: int
    hash_digest: Optional[bytes]
    partially_hashed: Optional[bool]

    @staticmethod
    def _hash_file(
        path: Path,
        sample_after: int = 1048576,
        sample_size: int = 16384,
        num_samples: int = 5,
    ) -> Tuple[bytes, bool]:

        # todo: usage of a "fast" hashing algo like xxhash would be cool, but using
        #  it pulls in a extension module which gets compiled against the python
        #  interpreter. for the moment blake2 seems to be the fastest algo in the std
        #  lib. https://www.blake2.net/
        hasher = hashlib.blake2b()

        size = path.stat().st_size

        partial_hashing = size > sample_after
        if sample_after < 0:
            partial_hashing = False

        if not partial_hashing:
            hasher.update(path.read_bytes())
        else:
            with path.open("rb") as f:
                for i in range(num_samples - 1):
                    f.seek(i * sample_size)
                    hasher.update(f.read(sample_size))
                # get slice at the absolute end
                f.seek(size - sample_size)
                hasher.update(f.read(sample_size))

        return hasher.digest(), partial_hashing

    @classmethod
    def from_file_path(
        cls,
        path: Path,
        sample_after: int = 1048576,
        sample_size: int = 16384,
        num_samples: int = 5,
        root_dir: Optional[Path] = None,
    ) -> "DataPathInfo":

        if not path.exists():
            raise ValueError(f"{path.as_posix()} doesn't exist!")

        if not path.is_file():
            raise ValueError(f"{path.as_posix()} is not a file!")

        size = path.stat().st_size

        if size == 0:
            digest = None
            partial_hashing = None
        else:
            digest, partial_hashing = cls._hash_file(
                path, sample_after, sample_size, num_samples
            )

        if root_dir is not None:
            path = path.relative_to(root_dir)

        return cls(
            file=path,
            size=size,
            hash_digest=digest,
            partially_hashed=partial_hashing if partial_hashing is not None else None,
        )


class DataDirId:
    def __init__(
        self,
        data_dir: Path,
        infos: List[DataPathInfo],
        sample_after: int = 1048576,
        sample_size: int = 16384,
        num_samples: int = 5,
    ):

        self._data_dir = data_dir
        self._infos = infos
        self._sample_after = sample_after
        self._sample_size = sample_size
        self._num_samples = num_samples
        self._digest = None

    @property
    def digest(self) -> bytes:
        """
        Generates a sequence of bytes which can be used to check if the same data
        exists in different locations or machines.

        Big files get hashed partially according to the sample_after, sample_size
        and num_samples arguments. This makes the hashing faster and should be good
        enough for comparison as the file size is also included in the hash
        calculation.
        """

        if self._digest is not None:
            return self._digest

        config_bytes = (
            self._num_samples.to_bytes(4, "big")
            + self._sample_after.to_bytes(4, "big")
            + self._sample_size.to_bytes(4, "big")
        )

        hash_algo = hashlib.blake2s(config_bytes)

        for info in self._infos:
            update_bytes = info.file.as_posix().encode()
            if info.hash_digest is not None:
                update_bytes += info.hash_digest
                hash_algo.update(update_bytes)

        self._digest = hash_algo.digest()
        return self._digest

    @property
    def hexdigest(self) -> str:
        """
        Hex string representation of the data returned by the digest property.
        """

        return self.digest.hex()

    @classmethod
    def build(
        cls,
        data_dir: Path,
        sample_after: int = 1048576,
        sample_size: int = 16384,
        num_samples: int = 5,
    ) -> "DataDirId":

        """
        Builds an DataDirId object for the given data_dir.

        data_dir should point to an existing directory as otherwise ValueErrors are
        raised!

        sample_after, sample_size and num_samples should only be changed if you really
        intend to do so as this changes the computed digest even if the files need no
        partial hashing!
        """

        if not data_dir.exists():
            raise ValueError(f"{data_dir.as_posix()} doesn't exist!")
        if not data_dir.is_dir():
            raise ValueError(f"{data_dir.as_posix()} is not a directory!")

        data_dir = data_dir.resolve()

        subpaths = [p for p in data_dir.rglob("*") if p.is_file()]
        subpaths.sort()

        infos = [
            DataPathInfo.from_file_path(
                p, sample_after, sample_size, num_samples, data_dir
            )
            for p in subpaths
        ]

        return cls(data_dir, infos, sample_after, sample_size, num_samples)


def fully_qualified_name(object: Any):
    cls = object.__class__
    module = cls.__module__

    if module == "builtins":
        return cls.__qualname__

    return module + "." + cls.__qualname__
