"""
This module allows building and running result runnable and distributable archive of the
currently running version of this package (dl_train_distributor), its python
interpreter and python dependencies.

This archive can be distributed to result different linux machine and can there be run
without needing anything besides system dependencies.
"""

import functools
import hashlib
import sys
from pathlib import Path

from gleisbauer.errors import NotRunFromZipappException
from gleisbauer.utils import xdg
from gleisbauer.utils.project import (
    proj_bin_name,
    proj_root_dir,
    proj_version,
)
from gleisbauer.utils.typing_ import Dict, NamedTuple, Tuple, TypeAlias

# todo: update this

RuntimeId: TypeAlias = str
FileFingerprints: TypeAlias = Dict[str, str]
ProjFingerprint: TypeAlias = str


# todo: add docstrings
# todo: write tests


class RuntimeMetadata(NamedTuple):
    version: str
    proj_fingerprint: ProjFingerprint
    file_fingerprints: FileFingerprints


def is_zipapp_run() -> bool:
    """Helper function to check if the current python interpreter process is the
    result of a zipapp execution."""

    return get_zipapp_path() is not None


def get_zipapp_path() -> Path:
    """Helper function to get a Path instance pointing to the zipapp which contains
    this code. If this code is not executed from within a zipapp None is returned."""

    paths = [Path(p) for p in sys.path]

    for p in paths:
        if p.suffix.lower() == ".pyz":
            return p.resolve()

    raise NotRunFromZipappException()


def runtimes_home(env: xdg.Environ = None) -> Path:
    import gleisbauer

    bin_name = gleisbauer.__proj_bin_name__
    return xdg.xdg_cache_home(env=env) / bin_name / "runtimes"


def remote_runtime_path(remote_env: xdg.Environ) -> Path:
    return runtimes_home(remote_env) / f"{current_runtime_id()}.pyz"


def collect_metadata() -> RuntimeMetadata:
    fingerprint, file_fingerprints = _collect_fingerprints(
        tuple(sorted(proj_root_dir().rglob("*.py")))
    )

    return RuntimeMetadata(proj_version(), fingerprint, file_fingerprints)


def current_runtime_id() -> RuntimeId:
    metadata = collect_metadata()
    return f"{proj_bin_name()}_v{metadata.version}"


@functools.lru_cache(maxsize=None)
def _collect_fingerprints(
    modules: Tuple[Path],
) -> Tuple[ProjFingerprint, FileFingerprints]:

    file_fingerprints = dict()
    proj_hash = hashlib.sha256()

    for m_file in modules:
        file_bytes = m_file.read_bytes()
        proj_hash.update(file_bytes)
        file_fingerprints[m_file.as_posix()] = hashlib.sha256(file_bytes).hexdigest()

    return proj_hash.hexdigest(), file_fingerprints
