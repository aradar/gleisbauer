import base64
import importlib
import inspect
import json
from datetime import datetime
from pathlib import Path
from types import ModuleType
from uuid import UUID

import dataclasses

from gleisbauer.utils.typing_ import (
    Any,
    Callable,
    Dict,
    Protocol,
    TextIO,
    Tuple,
    Type,
    Union,
    runtime_checkable,
)


@runtime_checkable
class Serializable(Protocol):
    def _asdict(self) -> Dict:
        raise NotImplementedError()


class Encoder(json.JSONEncoder):
    @staticmethod
    def to_type_dict(
        value: Any,
        serializer: Callable = str,
        factory_func: Callable[[Any], Any] = None,
    ) -> Union[Any, Dict[str, Any]]:

        _class = getattr(value, "__class__", None)

        if not _class:
            return value

        type_dict = {
            "__type__": f"{_class.__module__}.{_class.__qualname__}",
            "value": serializer(value),
        }

        if factory_func:
            type_dict["__factory__"] = f"{factory_func.__name__}"

        return type_dict

    def default(self, o: Any) -> Any:
        # called after a down sweep with the _encode method to convert not encoded
        # types if possible. at this point the types of this project should be
        # already converted.

        default_types = (dict, list, tuple, str, int, float, bool)
        if isinstance(o, default_types):
            return super().default(o)

        if isinstance(o, (Path, UUID)):
            return Encoder.to_type_dict(o)
        if isinstance(o, datetime):
            return Encoder.to_type_dict(
                o, lambda d: d.timestamp(), datetime.fromtimestamp
            )
        if isinstance(o, bytes):
            return Encoder.to_type_dict(o, lambda b: base64.b64encode(b).decode())
        if dataclasses.is_dataclass(o):
            return Encoder.to_type_dict(o, lambda d: vars(d))

        return super().default(o)

    def _encode(self, o: Any) -> Any:
        prim_types = (str, int, float, bool)

        if isinstance(o, prim_types):
            return o

        if isinstance(o, dict):
            return {self._encode(k): self._encode(v) for k, v in o.items()}

        if isinstance(o, list):
            return [self._encode(v) for v in o]

        if isinstance(o, set):
            return {self._encode(v) for v in o}

        if isinstance(o, tuple):
            if getattr(o, "_fields", None) and getattr(o, "_asdict", None):
                return self._encode(Encoder.to_type_dict(o, lambda t: t._asdict()))
            return tuple([v for v in o])

        if isinstance(o, Serializable):
            return self._encode(Encoder.to_type_dict(o, lambda s: s._asdict()))

        return o

    def encode(self, o: Any) -> str:
        return super().encode(self._encode(o))


def _get_type_by_name(type_str: str) -> Tuple[ModuleType, Type]:
    module_name, class_name = type_str.rsplit(".", maxsplit=1)

    imported = False
    while not imported:
        try:
            module = importlib.import_module(module_name)
            imported = True
        except ModuleNotFoundError:
            module_name, class_name_prefix = module_name.rsplit(".", maxsplit=1)
            class_name = f"{class_name_prefix}.{class_name}"

    if "." not in class_name:
        _class = getattr(module, class_name)
    else:
        _class = None
        for p_class in class_name.split("."):
            _class = getattr(_class if _class is not None else module, p_class)

    return module, _class


def decode(o: Any) -> Any:
    if isinstance(o, dict):
        _type = o.get("__type__", None)
        value = o.get("value", None)
        factory = o.get("__factory__", None)
        if _type:
            module, _class = _get_type_by_name(_type)

            # special case for bytes. they get encoded to base64 and therefore need
            # to be decoded and there is currently no simple way to express this with
            # the factory argument.
            if _class is bytes:
                return bytes(base64.b64decode(value))

            if factory:
                constructor = getattr(_class, factory)
            else:
                constructor = _class

            if isinstance(value, dict):
                return constructor(**value)
            if isinstance(value, (list, tuple, set)):
                return constructor(*value)
            return constructor(value)

    return o


def dumps(
    obj: Any,
    skipkeys: bool = False,
    ensure_ascii: bool = True,
    check_circular: bool = True,
    allow_nan: bool = True,
    indent: int = None,
    separators: Tuple[str, str] = None,
    sort_keys: bool = False,
) -> str:

    """
    Same as `json.dumps(...)` with the exception that it tries to keep more type infos
    for the deserialization. Using the dump(s) and load(s) pair from this module
    allows therefore to get the same object after deserialization without additional
    conversion steps (at least for datatypes defined and used in the project).
    """

    return json.dumps(
        obj,
        skipkeys=skipkeys,
        ensure_ascii=ensure_ascii,
        check_circular=check_circular,
        allow_nan=allow_nan,
        indent=indent,
        separators=separators,
        sort_keys=sort_keys,
        cls=Encoder,
    )


def dump(
    obj: Any,
    fp: TextIO,
    skipkeys: bool = False,
    ensure_ascii: bool = True,
    check_circular: bool = True,
    allow_nan: bool = True,
    indent: int = None,
    separators: Tuple[str, str] = None,
    sort_keys: bool = False,
):

    """
    Same as `json.dump(...)` with the exception that it tries to keep more type infos
    for the deserialization. Using the dump(s) and load(s) pair from this module
    allows therefore to get the same object after deserialization without additional
    conversion steps (at least for datatypes defined and used in the project).
    """

    json.dump(
        obj,
        fp,
        skipkeys=skipkeys,
        ensure_ascii=ensure_ascii,
        check_circular=check_circular,
        allow_nan=allow_nan,
        indent=indent,
        separators=separators,
        sort_keys=sort_keys,
        cls=Encoder,
    )


def loads(s: Union[str, bytes]):
    """
    Same as `json.loads(...)` with the exception that it tries to restore objects back
    to the original types and not python primitives. Using the dump(s) and load(s)
    pair from this module allows therefore to get the same object after
    deserialization without additional conversion steps (at least for datatypes
    defined and used in the project).
    """

    return json.loads(s, object_hook=decode)


def load(fp: TextIO):
    """
    Same as `json.load(...)` with the exception that it tries to restore objects back
    to the original types and not python primitives. Using the dump(s) and load(s)
    pair from this module allows therefore to get the same object after
    deserialization without additional conversion steps (at least for datatypes
    defined and used in the project).
    """

    return json.load(fp, object_hook=decode)
