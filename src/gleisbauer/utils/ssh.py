from io import StringIO

import paramiko

from gleisbauer.utils.typing_ import Tuple, TypeAlias

PrivateKey: TypeAlias = bytes
PublicKey: TypeAlias = bytes


def gen_key_pair(bits: int = 4096) -> Tuple[PrivateKey, PublicKey]:
    """
    Generates a key RSA keypair with the given number of bits.
    """

    key = paramiko.RSAKey.generate(bits)

    with StringIO() as f:
        key.write_private_key(f)
        private_key = f.getvalue().encode()
    pub_key = f"ssh-rsa {key.get_base64()}".encode()

    return private_key, pub_key
