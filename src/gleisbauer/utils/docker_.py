import getpass
import os
import pwd
import tarfile
from contextlib import contextmanager
from datetime import datetime
from io import BytesIO
from pathlib import Path

import docker
from docker.models.containers import Container as DockerContainer

from gleisbauer.utils.typing_ import (
    Any,
    BinaryIO,
    FileMode,
    Generator,
    GroupID,
    List,
    Sequence,
    Tuple,
    TypeAlias,
    Union,
    UserID,
)


def not_ignored_files(root_dir: Path, ignore_file: Path = None) -> List[Path]:
    """
    Helper function to read and apply dockerignore files to a directory. It returns
    all files in root_dir which are not ignored by the dickerignore file if there is
    one.

    If the ignore_file param is None by default a ".dockerignore" file in the root_dir
    will be read if possible.
    """

    if not root_dir.is_dir() and root_dir.exists():
        raise ValueError(f"{root_dir.as_posix()} is not a dir or doesn't exist!")

    if ignore_file is None:
        ignore_file = root_dir / ".dockerignore"

    if not ignore_file.is_file() and ignore_file.exists():
        raise ValueError(f"{ignore_file.as_posix()} is not a file or doesn't exist!")

    patterns = [line.strip() for line in ignore_file.read_text().splitlines() if line]
    patterns = [line for line in patterns if not line.startswith("#")]

    files = {p for p in root_dir.rglob("*") if p.is_file()}
    for p_line in patterns:
        drop_results = not p_line.startswith("!")

        p_match = set(root_dir.glob(p_line.lstrip("!/")))
        matched_files = set()
        for m in p_match:
            if m.is_dir():
                matched_files.update({p for p in m.rglob("*") if p.is_file()})
            else:
                matched_files.add(m)

        if drop_results:
            files.difference_update(matched_files)
        else:
            files.update(matched_files)

    not_ignored = list(files)
    not_ignored.sort()
    return not_ignored


def minify_buildfile(buildfile: Path) -> str:
    """
    Helper function to read a docker buildfile and drop all lines and spaces without
    meaning and return the remaining content as string.
    """

    def is_metadata_line(line: str) -> bool:
        if line.lstrip().startswith("#") or not line.strip():
            return False
        return True

    lines = [
        line.strip()
        for line in buildfile.read_text().splitlines()
        if is_metadata_line(line)
    ]
    return "\n".join(lines)


@contextmanager
def open_docker_client(
    timeout: int = None,
) -> Generator[docker.DockerClient, None, None]:
    """
    Helper function to get docker.DockerClient instance managed by context manager.
    This means that if used with the "with" syntax the client gets auto closed if no
    longer needed.
    """

    client = None
    try:
        client = docker.from_env(timeout=timeout)
        yield client
    finally:
        if client is not None:
            client.close()


def read_from_docker_container(path: Path, container: DockerContainer) -> bytes:
    """
    Simple helper function which reads a file from the given path of a container and
    returns the read bytes.
    """

    tar_iter, _ = container.get_archive(path.as_posix())

    tar_bytes = BytesIO()
    for chunk in tar_iter:
        tar_bytes.write(chunk)
    tar_bytes.seek(0)

    with tarfile.open(mode="r", fileobj=tar_bytes) as tar:
        requested_data = tar.extractfile(path.name)

        if requested_data is None:
            raise ValueError(
                f"The requested file {path.as_posix()} doesn't exist in the container!"
            )

    return requested_data.read()


DataSource: TypeAlias = Union[Path, BinaryIO, bytes, None]
CopyJobs: TypeAlias = Sequence[
    Union[
        Tuple[Path, DataSource],
        Tuple[Path, DataSource, FileMode],
        Tuple[Path, DataSource, FileMode, UserID, GroupID],
    ]
]
CopyJobList: TypeAlias = List[
    Union[
        Tuple[Path, DataSource],
        Tuple[Path, DataSource, FileMode],
        Tuple[Path, DataSource, FileMode, UserID, GroupID],
    ]
]


def _unpack_cp_job(
    job: Union[
        Tuple[Path, DataSource],
        Tuple[Path, DataSource, FileMode],
        Tuple[Path, DataSource, FileMode, UserID, GroupID],
    ],
    default_uid: int,
    default_gid: int,
    default_mode: int = 0o644,
) -> Tuple[Path, DataSource, FileMode, UserID, GroupID]:

    any_job: Any = job

    uid = default_uid
    gid = default_gid
    mode = default_mode

    path = any_job[0]
    data = any_job[1]

    if len(any_job) == 3:
        mode = any_job[2]
    elif len(any_job) == 5:
        mode = any_job[2]
        uid = any_job[3]
        gid = any_job[4]
    elif not len(any_job) == 2:
        raise ValueError(
            "Jobs only accepts a sequence of tuples with either 2, 3 or 5 values!"
        )

    return path, data, mode, uid, gid


def _read_data_source(data: DataSource) -> bytes:
    if isinstance(data, Path):
        data_bytes = data.read_bytes()
    elif isinstance(data, BinaryIO):
        data_bytes = data.read()
    elif isinstance(data, bytes):
        data_bytes = data
    else:
        raise ValueError(
            "The DataSource is not supported as it is not of type Path, BinaryIO "
            "or bytes!"
        )

    return data_bytes


def cp_into_docker_container(
    jobs: CopyJobs,
    container: DockerContainer,
    target_dir: Path = None,
    default_uid: int = None,
    default_gid: int = None,
    default_mode: int = 0o644,
):
    """
    Helper function to copy a single file or multiple ones into a docker container.

    Each entry of the jobs argument must point to a file and *not* a directory!

    If target_dir is not given all jobs will be written relative to the root dir!

    If the DataSource part of a copy job is of type Path mode, uid and gid
    are taken from it. Otherwise, defaults will be used or data supplied with the 3
    element tuple or 5 element tuple.

    Directories with special settings for uid, gid or mode can be created by adding
    them with a DataSource value of None.
    """

    if not jobs:
        return

    if target_dir is None:
        target_dir = Path("/")

    if default_uid is None:
        default_uid = os.getuid()

    if default_gid is None:
        default_gid = os.getgid()

    now = int(datetime.now().timestamp())
    tar_bytes = BytesIO()
    with tarfile.open(mode="w", fileobj=tar_bytes) as tar:
        for job_tuple in jobs:
            path, data, mode, uid, gid = _unpack_cp_job(
                job_tuple, default_uid, default_gid, default_mode
            )

            data_bytes = None
            if data is not None:
                data_bytes = _read_data_source(data)

            if isinstance(data, Path):
                stat = data.stat()
                mode = stat.st_mode
                uid = stat.st_uid
                gid = stat.st_gid

            info = tarfile.TarInfo(path.as_posix())
            info.mtime = now
            info.uid = uid
            info.gid = gid
            info.mode = mode

            if data_bytes is None:
                info.type = tarfile.DIRTYPE
                data_obj = None
            else:
                data_obj = BytesIO(data_bytes)
                data_obj.seek(0)
                info.size = len(data_bytes)

            tar.addfile(info, data_obj)

    tar_bytes.seek(0)
    if not container.put_archive(target_dir.as_posix(), tar_bytes.getvalue()):
        raise RuntimeError("Writing to the container failed!")


def add_user_to_docker_container(container: DockerContainer, username: str = None):
    """
    Adds the given user to the container without requiring that it is running.

    If no username is given the one running this script is used.

    The function also gives the newly created user a password, but as it gets set
    through a manually randomly generated hash it is impossible to use it as the
    password itself is unknown.
    """

    if username is None:
        username = getpass.getuser()

    home_dir = Path("home") / username

    user_struct = pwd.getpwnam(username)
    uid = user_struct.pw_uid
    gid = user_struct.pw_gid

    passwd = read_from_docker_container(Path("/etc/passwd"), container).decode()
    if not passwd.endswith("\n"):
        passwd += "\n"
    passwd += f"{username}:x:{uid}:{gid}::{Path('/') / home_dir.as_posix()}:/bin/bash"

    shadow = read_from_docker_container(Path("/etc/shadow"), container).decode()
    if not shadow.endswith("\n"):
        shadow += "\n"
    random_sha512_pw_hash = (
        "$6$tFSHbSayQHBMoV7g$o7a3fBPDRB6qX2POHYz316ZeOcYTIWetzRK9"
        "fAVexif0jjqFoSjqg2etn7KEN3CifwEOATX6vhNlWo4FX30wEx"
    )
    # shadow += f"{username}:{encrypted_password}:{last_change}:0:99999:7:::"
    shadow += f"{username}:{random_sha512_pw_hash}::::::::"

    group = read_from_docker_container(Path("/etc/group"), container).decode()
    if not group.endswith("\n"):
        group += "\n"
    group += f"{username}:x:{gid}:"

    gshadow = read_from_docker_container(Path("/etc/gshadow"), container).decode()
    if not gshadow.endswith("\n"):
        gshadow += "\n"
    gshadow += f"{username}:!::"

    cp_into_docker_container(
        [
            (Path("etc/passwd"), passwd.encode()),
            (Path("etc/shadow"), shadow.encode()),
            (Path("etc/group"), group.encode()),
            (Path("etc/gshadow"), gshadow.encode()),
            (home_dir, None, 0o700, uid, gid),
        ],
        container,
        Path("/"),
    )
