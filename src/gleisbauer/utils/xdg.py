import os
from pathlib import Path

from gleisbauer.utils.typing_ import Dict, Optional, TypeAlias, Union

Environ: TypeAlias = Dict[str, str]


def _default_environ() -> Environ:
    return dict(**os.environ)


def _build_user_path(name: str, env: Environ = None) -> Optional[Path]:

    if env is None:
        env = _default_environ()

    val = env.get(name, "")

    return Path(val) if val else None


def _build_user_path_with_default(
    name: str, default: Union[str, Path], env: Environ = None
) -> Path:

    if env is None:
        env = _default_environ()

    path = _build_user_path(name, env)

    if path is None:
        home_var = env.get("HOME")
        if not home_var:
            # todo: fallback which should never occur due to the POSIX spec
            home_var = "/"
        path = Path(home_var) / default

    return path


def xdg_cache_home(env: Environ = None) -> Path:
    """
    Returns the base directory relative to which user-specific non-essential files_to_copy
    files should be stored.
    """
    return _build_user_path_with_default(
        name="XDG_CACHE_HOME", default=Path(".cache"), env=env
    )


def xdg_config_home(env: Environ = None) -> Path:
    """
    Returns the base directory relative to which user-specific configuration files
    should be stored.
    """
    return _build_user_path_with_default(
        name="XDG_CONFIG_HOME", default=Path(".config"), env=env
    )


def xdg_data_home(env: Environ = None) -> Path:
    """
    Returns the base directory relative to which user-specific files_to_copy files should be
    stored.
    """
    return _build_user_path_with_default(
        name="XDG_DATA_HOME", default=Path(".local/share"), env=env
    )


def xdg_runtime_dir(env: Environ = None) -> Optional[Path]:
    """
    Returns the base directory relative to which user-specific non-essential runtime
    files and other exp_root objects (such as sockets, named pipes, ...) should be stored.
    """
    return _build_user_path(name="XDG_RUNTIME_DIR", env=env)


def xdg_state_home(env: Environ = None) -> Path:
    """
    Returns the base directory which contains state files_to_copy that should persist between
    (application) restarts, but that is not important or portable enough to the user
    that it should be stored in xdg_data_home().
    """
    return _build_user_path_with_default(
        name="XDG_STATE_HOME", default=Path(".local/state"), env=env
    )
