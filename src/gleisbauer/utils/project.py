from pathlib import Path


def proj_root_dir() -> Path:
    import gleisbauer

    return Path(gleisbauer.__file__).parent


def proj_bin_name() -> str:
    import gleisbauer

    return gleisbauer.__proj_bin_name__


def proj_version() -> str:
    import gleisbauer

    return gleisbauer.__version__
