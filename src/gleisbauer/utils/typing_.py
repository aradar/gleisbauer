"""This module contains all typing imports and uses as many as possible from
typing_extensions instead of typing to allow backwards compatibility as far
as possible.

All other modules have to use this module to import parts of the typing
namespace to prevent problems between different python interpreter versions!"""


from typing import (
    IO,
    Any,
    AnyStr,
    BinaryIO,
    Callable,
    Dict,
    Generator,
    Iterable,
    Iterator,
    List,
    Set,
    NamedTuple,
    Optional,
    Sequence,
    TextIO,
    Tuple,
    Union,
)

from typing_extensions import (
    ContextManager,
    Literal,
    Protocol,
    Type,
    TypeAlias,
    runtime_checkable,
)

UserID: TypeAlias = int
GroupID: TypeAlias = int
FileMode: TypeAlias = int

__all__ = [
    "IO",
    "Any",
    "AnyStr",
    "BinaryIO",
    "Callable",
    "Dict",
    "Iterable",
    "Iterator",
    "List",
    "Set",
    "NamedTuple",
    "Optional",
    "Sequence",
    "TextIO",
    "Tuple",
    "Union",
    "Generator",
    "Literal",
    "Protocol",
    "Type",
    "TypeAlias",
    "runtime_checkable",
    "ContextManager",
    "UserID",
    "GroupID",
    "FileMode",
]
