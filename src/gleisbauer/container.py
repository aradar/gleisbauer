import getpass
import hashlib
import tarfile
from io import BytesIO
from pathlib import Path

from docker.types import DeviceRequest as DockerDeviceRequest
from docker.types import Mount as DockerMount
from docker.types import Ulimit as DockerUlimit

from gleisbauer.utils import docker_
from gleisbauer.utils.docker_ import (
    CopyJobs,
    add_user_to_docker_container,
    cp_into_docker_container,
    open_docker_client,
)
from gleisbauer.utils.io_ import fits_on_disk
from gleisbauer.utils.typing_ import (
    Any,
    BinaryIO,
    GroupID,
    List,
    Literal,
    NamedTuple,
    Optional,
    Protocol,
    Sequence,
    Tuple,
    UserID,
)

#  todo: update naming. image_name = fq img name (repo + "image_name")


Status = Literal["restarting", "running", "paused", "exited"]
EngineName = Literal["docker"]

_engine: Optional["ContainerEngine"] = None


def default_engine() -> "ContainerEngine":
    if _engine is None:
        # todo: add a debug log
        set_engine("docker")

    engine: Any = _engine

    return engine


def set_default_engine(engine: "ContainerEngine"):
    global _engine
    _engine = engine


def set_engine(name: EngineName):
    eng_map = dict(docker=DockerContainerEngine)
    engine_cls = eng_map.get(name, None)

    if not engine_cls:
        raise ValueError(f"{name} is not a supported container engine name!")

    set_default_engine(engine_cls())


class Image(NamedTuple):
    id: str
    tags: List[str]
    size: int


class Container(NamedTuple):
    name: str
    id: str
    image: Image
    status: Status


class HostMount(NamedTuple):
    source: Path
    target: Path
    read_only: bool = True


class ContainerEngine(Protocol):
    def get_containers(self) -> List[Container]:
        raise NotImplementedError()

    def get_images(self) -> List[Image]:
        raise NotImplementedError()

    def start_container(
        self,
        image_name: str,
        command: str,
        username: str,
        uid: UserID,
        gid: GroupID,
        name: str = None,
        device_ids: Sequence[str] = None,
        host_mounts: Sequence[HostMount] = None,
        copy_jobs: CopyJobs = None,
    ) -> Container:
        raise NotImplementedError()

    def stop_container(self, container: Container):
        raise NotImplementedError()

    def find_container(self, id: str = None, name: str = None) -> Optional[Container]:
        raise NotImplementedError()

    def container_exists(self, id: str = None, name: str = None) -> bool:
        raise NotImplementedError()

    def build_image(self, build_file: Path, name: str) -> Image:
        raise NotImplementedError()

    def write_build_context_archive(self, build_file: Path, file: BinaryIO):
        raise NotImplementedError()

    def save_build_context_archive(self, build_file: Path, archive_file: Path):
        raise NotImplementedError()

    def load_image(self, image_file: Path):
        raise NotImplementedError()

    def find_image(self, id: str = None, name: str = None) -> Optional[Image]:
        raise NotImplementedError()

    def image_exists(self, id: str = None, name: str = None) -> bool:
        raise NotImplementedError()

    def pull_image(self, image_name: str) -> Image:
        raise NotImplementedError()

    def save_image(self, image: Image, out_image_file: Path):
        raise NotImplementedError()


class DockerContainerEngine(ContainerEngine):
    @staticmethod
    def _conv_container_obj(docker_container: Any) -> Container:
        return Container(
            name=docker_container.name,
            id=docker_container.id,
            image=DockerContainerEngine._conv_image_obj(docker_container.image),
            status=docker_container.status,
        )

    @staticmethod
    def _conv_image_obj(docker_image: Any) -> Image:
        return Image(
            id=docker_image.id, tags=docker_image.tags, size=docker_image.attrs["Size"]
        )

    @staticmethod
    def _split_img_name(name: str, default_tag: str = None) -> Tuple[str, str]:
        repo = name
        if ":" in name:
            repo, tag = name.split(":", maxsplit=1)
        else:
            tag = default_tag if default_tag else "latest"

        return repo, tag

    @staticmethod
    def _fq_img_name(name: str, default_tag: str = None) -> str:
        repo, tag = DockerContainerEngine._split_img_name(name, default_tag)
        return f"{repo}:{tag}"

    def unique_fq_img_name(self, name: str, build_file: Path):
        repo, _ = DockerContainerEngine._split_img_name(name)

        tar_bytes = BytesIO()
        if build_file.suffix.lower() == ".tar":
            tar_bytes.write(build_file.read_bytes())
        else:
            self.write_build_context_archive(build_file, tar_bytes)

        tar_bytes.seek(0)
        ctx_hash = hashlib.blake2s()
        with tarfile.open(fileobj=tar_bytes, mode="r") as tar:
            for mem in tar.getmembers():
                file_io = tar.extractfile(mem)
                if file_io:
                    ctx_hash.update(file_io.read())

        return f"{repo}:{ctx_hash.hexdigest()}"

    def _gen_container_name(self, image_name: str) -> str:
        image_name, _ = DockerContainerEngine._split_img_name(image_name)
        user_name = getpass.getuser()

        cand_name = f"{image_name}_{user_name}"
        idx = 1
        while self.container_exists(name=cand_name):
            cand_name = f"{image_name}_{user_name}_{idx}"
            idx += 1

        return cand_name

    def get_containers(self) -> List[Container]:
        with open_docker_client() as client:
            return [
                DockerContainerEngine._conv_container_obj(c)
                for c in client.containers.list(True)
            ]

    def get_images(self) -> List[Image]:
        with open_docker_client() as client:
            return [
                DockerContainerEngine._conv_image_obj(i) for i in client.images.list()
            ]

    def start_container(
        self,
        image_name: str,
        command: str,
        username: str,
        uid: UserID,
        gid: GroupID,
        name: str = None,
        device_ids: Sequence[str] = None,
        host_mounts: Sequence[HostMount] = None,
        copy_jobs: CopyJobs = None,
    ) -> Container:

        fq_img_name = DockerContainerEngine._fq_img_name(image_name)

        if not name:
            name = self._gen_container_name(image_name)

        if self.container_exists(name=name):
            raise ValueError(f"A container with name {name} already exists!")

        with open_docker_client() as client:
            d_con = client.containers.create(
                image=fq_img_name,
                command=command,
                name=name,
                user=f"{uid}:{gid}",
                auto_remove=False,
                detach=True,
                network_mode="host",
                device_requests=[
                    # capabilities are according to the docu optional but are needed
                    # as otherwise the daemon crashes internally
                    DockerDeviceRequest(
                        device_ids=device_ids,
                        capabilities=[["gpu"], ["utility"], ["compute"]],
                    )
                ]
                if device_ids
                else [],
                mounts=[
                    DockerMount(
                        target=m.target.as_posix(),
                        source=m.source.as_posix(),
                        type="bind",
                        read_only=m.read_only,
                    )
                    for m in host_mounts
                ]
                if host_mounts
                else [],
                # recommendations from nvidia for running GPU containers:
                ipc_mode="host",
                shm_size="1G",
                ulimits=[
                    # soft and hard both have to be set as otherwise the daemon
                    # raises an internal exception. the documentation claims that
                    # setting one of the values should set the other one with the
                    # same value, but this is sadly a lie :(
                    DockerUlimit(name="memlock", soft=-1, hard=-1),
                    DockerUlimit(name="stack", soft=67108864, hard=67108864),
                ],
                #  todo: usage of labels=[...] could be cool for container tracking
            )

            add_user_to_docker_container(d_con, username)

            if copy_jobs:
                cp_into_docker_container(copy_jobs, d_con, Path("/"))

            # todo: add a try catch which removes the already build container if start fails
            d_con.start()

            return DockerContainerEngine._conv_container_obj(
                client.containers.get(d_con.id)
            )

    def stop_container(self, container: Container, delete: bool = True):
        with open_docker_client() as client:
            d_con = client.containers.get(container.id)

            if d_con is None:
                raise ValueError("Container doesn't exist!")

            if delete:
                d_con.remove(force=True)
            else:
                d_con.kill()

    def find_container(self, id: str = None, name: str = None) -> Optional[Container]:
        if not id and not name:
            raise ValueError("Either id or name must be given!")

        for con in self.get_containers():
            if id:
                if id == con.id:
                    return con
            else:
                if name == con.name:
                    return con

        return None

    def container_exists(self, id: str = None, name: str = None) -> bool:
        return self.find_container(id, name) is not None

    def build_image(
        self, build_file: Path, name: str, exists_okay: bool = True
    ) -> Image:

        # fq_img_name = DockerContainerEngine._fq_img_name(name)
        fq_img_name = self.unique_fq_img_name(name, build_file)

        if not exists_okay and self.image_exists(name=name):
            raise ValueError(
                f"Image {fq_img_name} already exists and exists_okay = False!"
            )

        with open_docker_client() as client:
            # build from build context archive
            if build_file.suffix.lower() == ".tar":
                with build_file.open("rb") as f:
                    d_img, d_log = client.images.build(
                        fileobj=f,
                        custom_context=True,
                        tag=fq_img_name,
                        rm=True,
                        forcerm=True,
                    )
            # build with auto created build context
            else:
                context_dir = build_file
                dockerfile = "Dockerfile"
                if build_file.is_file():
                    context_dir = build_file.parent
                    dockerfile = build_file.name

                d_img, d_log = client.images.build(
                    path=context_dir.as_posix(),
                    dockerfile=dockerfile,
                    tag=fq_img_name,
                    rm=True,
                    forcerm=True,
                )  # todo: additional tag with the hash of the build context would be cool

            repo, _ = DockerContainerEngine._split_img_name(name)
            d_img.tag(repo, "latest")

            return DockerContainerEngine._conv_image_obj(d_img)

    def write_build_context_archive(self, build_file: Path, file: BinaryIO):
        if build_file.with_name(".dockerignore").exists():
            files = docker_.not_ignored_files(build_file.parent)
        else:
            files = list(build_file.parent.rglob("*"))

        # without sorting the generated tar is not deterministic and the checksum
        # of its content might be different and therefore breaks generating a hash
        # based on it.
        files.sort()

        with tarfile.open(fileobj=file, mode="w") as tar:
            # drops all unimportant lines and chars from buildfile to minimise to
            # amount of hash changes
            bf_bytes = BytesIO(docker_.minify_buildfile(build_file).encode())
            bf_bytes.seek(0)

            info = tar.gettarinfo(
                name=build_file.as_posix(),
                arcname=build_file.relative_to(build_file.parent).as_posix(),
            )
            info.size = len(bf_bytes.getvalue())

            tar.addfile(info, bf_bytes)

            for f in files:
                tar.add(
                    name=f.as_posix(),
                    arcname=f.relative_to(build_file.parent).as_posix(),
                )

    def save_build_context_archive(self, build_file: Path, archive_file: Path):
        archive_file.parent.mkdir(parents=True, exist_ok=True)
        with archive_file.open("wb") as f:
            self.write_build_context_archive(build_file, f)

    def load_image(self, image_file: Path):
        with open_docker_client() as client:
            with image_file.open("rb") as f:
                client.images.load(f)

    def find_image(self, id: str = None, name: str = None) -> Optional[Image]:
        if id is None and name is None:
            raise ValueError("Either id or name must be set!")

        if name:
            name = DockerContainerEngine._fq_img_name(name)
        images = self.get_images()

        for img in images:
            if id:
                if id == img.id:
                    return img
            else:
                if name in img.tags:
                    return img

        return None

    def image_exists(self, id: str = None, name: str = None) -> bool:
        return self.find_image(id, name) is not None

    def pull_image(self, name: str) -> Image:
        with open_docker_client() as client:
            img_name, tag = DockerContainerEngine._split_img_name(name)
            d_img = client.images.pull(img_name, tag)
            return DockerContainerEngine._conv_image_obj(d_img)

    def save_image(self, image: Image, out_image_file: Path):
        if not fits_on_disk(out_image_file, image.size * 2):
            raise ValueError(
                f"Not enough free space to write image {image.tags[0]} to"
                f" {out_image_file.as_posix()}!"
            )

        out_image_file.parent.mkdir(parents=True, exist_ok=True)

        with open_docker_client(timeout=60 * 10) as client:
            d_img = client.images.get(image.id)

            with out_image_file.open("wb") as f:
                for chunk in d_img.save(named=True):
                    f.write(chunk)
