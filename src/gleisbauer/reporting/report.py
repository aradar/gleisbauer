from pathlib import Path

from gleisbauer.experiments.journal import Journal
from gleisbauer.reporting.container import ContainerEngineInfo
from gleisbauer.reporting.generic import (
    CpuInfo,
    MemoryInfo,
    MountInfo,
    JournalInfo,
    DataIDInfo,
)
from gleisbauer.reporting.gpu import GPUInfo
from gleisbauer.utils.io_ import path_relation, convert_byte_unit
from gleisbauer.utils.typing_ import List, NamedTuple, Optional


class Report(NamedTuple):
    cpu: CpuInfo
    mem: MemoryInfo
    mounts: List[MountInfo]
    container_engine: ContainerEngineInfo
    gpus: List[GPUInfo]
    journal: JournalInfo
    data_ids: DataIDInfo

    @classmethod
    def gather(
        cls, data_root_dir: Optional[Path] = None, journal: Optional[Journal] = None
    ) -> "Report":
        return cls(
            cpu=CpuInfo.gather(),
            mem=MemoryInfo.gather(),
            mounts=MountInfo.gather(),
            container_engine=ContainerEngineInfo.gather(),
            gpus=GPUInfo.gather(),
            journal=JournalInfo.gather(journal),
            data_ids=DataIDInfo.gather(data_root_dir),
        )

    def container_image_exists(self, id: str = None, name: str = None) -> bool:
        for img in self.container_engine.images:
            if id:
                if id == img.id:
                    return True
            else:
                if name in img.tags:
                    return True

        return False

    def has_enough_disk_space(self, path: Path, needed_space: int) -> bool:
        if not self.mounts:
            return False

        mount_relations = [
            (path_relation(path, info.path), info) for info in self.mounts
        ]
        mount_relations.sort(key=lambda t: t[0])

        relation, info = mount_relations[-1]
        if relation == 0:
            return False

        return info.free > needed_space

    @property
    def free_gpu_ids(self) -> List[str]:
        no_procs_ids = set([g.uuid for g in self.gpus if not g.processes])
        used_by_exp_ids = self.journal.used_device_ids
        return list(no_procs_ids.difference(used_by_exp_ids))

    @property
    def num_free_gpus(self) -> int:
        return len(self.free_gpu_ids)

    @property
    def num_gpus(self) -> int:
        return len(self.gpus)

    def gpu_ids_with_min_mem(self, num_bytes: int) -> List[str]:
        return [gpu.uuid for gpu in self.gpus if gpu.total_memory >= num_bytes]

    def num_gpus_with_min_mem(self, num_bytes: int) -> int:
        return len(self.gpu_ids_with_min_mem(num_bytes))

    def print(self):
        print(
            f"CPU: num cores {self.cpu.cpu_count}, load (1) {self.cpu.load1} (5)"
            f" {self.cpu.load5} (15) {self.cpu.load15}"
        )

        print(
            "\nMemory:\n - RAM usage"
            f" {(1 - self.mem.mem_available / self.mem.mem_total) * 100:0.2f} %, free"
            f" {convert_byte_unit(self.mem.mem_available, 'B', 'GB'):0.2f} GB, total"
            f" {convert_byte_unit(self.mem.mem_total, 'B', 'GB'):0.2f} GB\n - swap"
            f" usage {(1 - self.mem.swap_free / self.mem.swap_total) * 100:0.2f} %"
        )

        print("\nDisk space:")
        for mount in sorted(self.mounts):
            print(
                f' - mount point "{mount.path.as_posix()}",'
                f" free {convert_byte_unit(mount.free, 'B', 'GB'):0.2f} GB,"
                f" total {convert_byte_unit(mount.total, 'B', 'GB'):0.2f} GB"
            )

        print(
            "\nContainer engine: running containers"
            f" {self.container_engine.num_running_containers}, not running containers"
            f" {self.container_engine.num_not_running_containers}"
        )

        if self.gpus:
            print("\nGPU:")
            for gpu in self.gpus:
                print(
                    f" - {gpu.name}, utilization {gpu.utilization * 100:0.2f} %, mem"
                    f" total {convert_byte_unit(gpu.total_memory, 'B', 'GB'):0.2f} GB,"
                    f" mem usage {(gpu.used_memory / gpu.total_memory) * 100:0.2f} %"
                )

        if self.journal.journal.active_experiments:
            print(
                "\nJournal: num running experiments"
                f" {len(self.journal.journal.running)}, num waiting experiments"
                f" {len(self.journal.journal.waiting)}, num previously ran experiments"
                f" {len(self.journal.journal.history)}"
            )
        else:
            print("\nJournal: no experiments ran so far")
