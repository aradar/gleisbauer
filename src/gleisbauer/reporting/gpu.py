import csv
import io
import shlex
import shutil
import subprocess
from typing import Dict, List, NamedTuple

from gleisbauer.utils.io_ import byte_unit_str_to_int


def _is_nvidia_smi_installed() -> bool:
    smi_bin = shutil.which("nvidia-smi")
    return smi_bin is not None


class GPUProcess(NamedTuple):
    pid: int
    name: str
    used_memory: int


class GPUInfo(NamedTuple):
    uuid: str
    pci_bus_id: str
    name: str
    utilization: float
    total_memory: int
    free_memory: int
    used_memory: int
    driver_version: str
    processes: List[GPUProcess]

    @classmethod
    def _process_infos(cls) -> Dict[str, GPUProcess]:
        query_proc = subprocess.run(
            args=shlex.split(
                "nvidia-smi --format=noheader,csv"
                " --query-compute-apps=gpu_uuid,pid,name,used_memory"
            ),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        if query_proc.returncode != 0:
            raise RuntimeError(query_proc.stdout.decode())

        infos = dict()
        for row in csv.reader(io.StringIO(query_proc.stdout.decode())):
            gpu_id = row[0].strip()
            pid = int(row[1].strip())
            name = row[2].strip()
            used_memory = byte_unit_str_to_int(row[3].strip())

            infos[gpu_id] = GPUProcess(pid=pid, name=name, used_memory=used_memory)

        return infos

    @classmethod
    def gather(cls) -> List["GPUInfo"]:
        if not _is_nvidia_smi_installed():
            return list()

        query_proc = subprocess.run(
            args=shlex.split(
                "nvidia-smi --format=noheader,csv "
                "--query-gpu=uuid,pci.bus_id,name,utilization.gpu,memory.total,"
                "memory.free,memory.used,driver_version"
            ),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        if query_proc.returncode != 0:
            raise RuntimeError(query_proc.stdout.decode())

        proc_infos = cls._process_infos()
        infos = list()
        for row in csv.reader(io.StringIO(query_proc.stdout.decode())):
            infos.append(
                cls(
                    uuid=row[0].strip(),
                    pci_bus_id=row[1].strip(),
                    name=row[2].strip(),
                    utilization=int(row[3].replace("%", "").strip()),
                    total_memory=byte_unit_str_to_int(row[4].strip()),
                    free_memory=byte_unit_str_to_int(row[5].strip()),
                    used_memory=byte_unit_str_to_int(row[6].strip()),
                    driver_version=row[7].strip(),
                    processes=proc_infos.get(row[0].strip(), list()),
                )
            )

        return infos
