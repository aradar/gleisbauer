import os
import platform
import shutil
from pathlib import Path

from gleisbauer.experiments.journal import Journal, default_journal_lock
from gleisbauer.utils.io_ import DataDirId
from gleisbauer.utils.typing_ import (
    List,
    Literal,
    NamedTuple,
    Sequence,
    Union,
    Optional,
)


class MemoryInfo(NamedTuple):
    mem_total: Union[int, float]
    mem_free: Union[int, float]
    mem_available: Union[int, float]
    swap_total: Union[int, float]
    swap_free: Union[int, float]

    @classmethod
    def gather(cls) -> "MemoryInfo":
        if platform.system() != "Linux":
            raise NotImplementedError(
                "gather_mem_info() currently only supports Linux!"
            )

        meminfo_file = Path("/proc/meminfo")

        str_pairs = {
            t[0]: t[1]
            for t in [
                [e.strip() for e in line.split(":", maxsplit=1)]
                for line in meminfo_file.read_text().splitlines()
            ]
        }

        meminfo = {k: int(v.replace(" kB", "")) * 1024 for k, v in str_pairs.items()}

        return cls(
            meminfo["MemTotal"],
            meminfo["MemFree"],
            meminfo["MemAvailable"],
            meminfo["SwapTotal"],
            meminfo["SwapFree"],
        )


class CpuInfo(NamedTuple):
    cpu_count: int
    load1: float
    load5: float
    load15: float

    @classmethod
    def gather(cls) -> "CpuInfo":
        if platform.system() != "Linux":
            raise NotImplementedError(
                "gather_cpu_info() currently only supports Linux!"
            )

        cpu_count = os.cpu_count()
        if not cpu_count:
            cpu_count = 0

        load1, load5, load15 = os.getloadavg()

        return cls(cpu_count, load1, load5, load15)


class MountInfo(NamedTuple):
    path: Path
    total: int
    used: int
    free: int

    @classmethod
    def gather(cls) -> List["MountInfo"]:
        def startswith_one_of(value: str, starts: Sequence[str]) -> bool:
            for s in starts:
                if value.startswith(s):
                    return True
            return False

        if platform.system() != "Linux":
            raise NotImplementedError(
                "gather_mount_info() currently only supports Linux!"
            )

        mounts_file = Path("/proc/mounts")
        mounts = [line.split(" ") for line in mounts_file.read_text().splitlines()]

        ignored_roots = ["/proc", "/sys", "/dev"]
        mount_paths = {
            Path(m[1]) for m in mounts if not startswith_one_of(m[1], ignored_roots)
        }

        mount_infos = []
        for path in mount_paths:
            try:
                total, used, free = shutil.disk_usage(path)
                mount_infos.append(MountInfo(path, total, used, free))
            except PermissionError:
                pass
            except OSError:
                pass

        return mount_infos


class JournalInfo(NamedTuple):
    journal: Journal

    @classmethod
    def gather(cls, journal: Optional[Journal] = None) -> "JournalInfo":
        if journal is not None:
            return cls(journal)

        with default_journal_lock() as lock:
            return cls(Journal.read(lock))

    @property
    def used_device_ids(self) -> List[str]:
        ids = []
        if self.journal:
            for exp in self.journal.running.values():
                ids += exp.metadata.device_ids

        return ids


class DataIDInfo(NamedTuple):

    dir_digests: List[str]

    @classmethod
    def gather(cls, root_dir: Optional[Path] = None) -> "DataIDInfo":

        if root_dir is None:
            return cls(list())

        digests = list()
        for path in root_dir.iterdir():
            if not path.is_dir():
                continue

            data_id = DataDirId.build(path)
            digests.append(data_id.hexdigest)

        return cls(digests)
