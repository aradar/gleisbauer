from gleisbauer.container import Container, Image, default_engine
from gleisbauer.utils.typing_ import List, NamedTuple

# todo: find container storage path


class ContainerEngineInfo(NamedTuple):
    containers: List[Container]
    images: List[Image]

    @classmethod
    def gather(cls) -> "ContainerEngineInfo":
        engine = default_engine()

        return ContainerEngineInfo(engine.get_containers(), engine.get_images())

    @property
    def num_running_containers(self) -> int:
        return sum([1 for c in self.containers if c.status == "running"])

    @property
    def num_containers(self) -> int:
        return len(self.containers)

    @property
    def num_not_running_containers(self) -> int:
        return self.num_containers - self.num_running_containers
