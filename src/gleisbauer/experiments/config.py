from pathlib import Path

import tomli

from gleisbauer.container import EngineName
from gleisbauer.errors import ConfigParseException
from gleisbauer.experiments.journal import ExpId
from gleisbauer.utils.project import proj_bin_name
from gleisbauer.utils.typing_ import (
    Dict,
    List,
    NamedTuple,
    Optional,
    Union,
    Any,
    Type,
    Tuple,
    Callable,
)


class ContainerConfig(NamedTuple):
    image_name: str
    buildfile: Optional[Path] = None
    always_rebuild: bool = False
    engine: EngineName = "docker"
    build_on_remote: bool = True  # todo: parse this

    @property
    def needs_building(self) -> bool:
        return self.buildfile is None

    @classmethod
    def from_obj(cls, obj: Union[Dict, str]) -> "ContainerConfig":
        # todo: raise exception when image_name is fully qualified (has a tag)
        if isinstance(obj, dict):
            kwargs = obj.copy()
            buildfile = Path(kwargs.pop("buildfile"))

            return ContainerConfig(buildfile=buildfile, **kwargs)
        elif not isinstance(obj, str):
            raise ConfigParseException(
                "Field container must either be a string or a dict with information"
                " about how to build a container!"
            )

        return ContainerConfig(image_name=obj)


class Config(NamedTuple):
    name: str
    app_root_dir: Path
    app_file_globs: Optional[List[str]]
    data_root_dir: Path
    data_file_globs: Optional[List[str]]
    entrypoint: Path
    remote_root: Path
    container: ContainerConfig

    @staticmethod
    def glob_files(
        root_dir: Path, globs: List[str], include_root: bool = False
    ) -> List[Path]:

        globed_files = set()
        for glob_pattern in globs:
            globed_files.update(root_dir.glob(glob_pattern))

        files = [p for p in globed_files if p.is_file()]
        files.sort()

        if not include_root:
            files = [p.relative_to(root_dir) for p in files]

        return files

    @property
    def app_files(self) -> List[Path]:
        return Config.glob_files(
            self.app_root_dir, self.app_file_globs if self.app_file_globs else ["**/*"]
        )

    @property
    def app_files_with_root(self) -> List[Path]:
        return Config.glob_files(
            self.app_root_dir,
            self.app_file_globs if self.app_file_globs else ["**/*"],
            True,
        )

    @property
    def data_files(self) -> List[Path]:
        return Config.glob_files(
            self.data_root_dir,
            self.data_file_globs if self.data_file_globs else ["**/*"],
        )

    @property
    def data_files_with_root(self) -> List[Path]:
        return Config.glob_files(
            self.data_root_dir,
            self.data_file_globs if self.data_file_globs else ["**/*"],
            True,
        )

    @property
    def remote_data_root_dir(self) -> Path:
        return self.remote_root / "data"

    @property
    def remote_exp_root_dir(self) -> Path:
        return self.remote_root / "experiments"

    def remote_exp_dir(self, exp_id: ExpId) -> Path:
        return self.remote_exp_root_dir / exp_id

    def remote_tmp_dir(self, exp_id: ExpId) -> Path:
        return self.remote_exp_root_dir / exp_id / "tmp"

    def remote_exp_app_dir(self, exp_id: ExpId) -> Path:
        return self.remote_exp_root_dir / exp_id / self.app_root_dir

    def remote_exp_data_dir(self, exp_id: ExpId) -> Path:
        return self.remote_exp_root_dir / exp_id / self.data_root_dir

    def remote_exp_out_dir(self, exp_id: ExpId) -> Path:
        return self.remote_exp_root_dir / exp_id / "out"

    @classmethod
    def _process_toml_field(
        cls,
        config_dict: Dict,
        field: str,
        field_type: Union[Type, Tuple[Type, Type]],
        error_desc: str,
        needed: bool = True,
        default: Optional[Any] = None,
        type_factory: Optional[Callable[[Any], Any]] = None,
    ):

        if type_factory is None:
            if isinstance(field_type, tuple):
                type_factory = field_type[1]
            else:
                type_factory = field_type

        if field not in config_dict:
            if default is not None:
                config_dict[field] = default
            elif needed:
                raise ConfigParseException(
                    f"Toml string is missing required field {field}!"
                )
        else:
            value = config_dict[field]

            try:
                if isinstance(field_type, tuple):
                    seq_type = field_type[0]
                    inner_type = field_type[1]
                    tmp_value = value
                    if not isinstance(value, seq_type):
                        tmp_value = seq_type(value)
                    for idx in range(len(tmp_value)):
                        if not isinstance(tmp_value[idx], inner_type):
                            tmp_value[idx] = type_factory(tmp_value[idx])
                    config_dict[field] = tmp_value
                else:
                    if not isinstance(value, field_type):
                        config_dict[field] = type_factory(value)
            except Exception:
                raise ConfigParseException(f"Field {field} must contain {error_desc}!")

    @classmethod
    def from_toml_string(cls, config_str: str) -> "Config":
        config_dict = tomli.loads(config_str)

        cls._process_toml_field(
            config_dict, field="name", field_type=str, error_desc="a string"
        )

        cls._process_toml_field(
            config_dict,
            field="remote_root",
            field_type=Path,
            error_desc="an absolute path used on the nodes",
            default=Path(f"/var/tmp/{proj_bin_name()}"),
        )

        cls._process_toml_field(
            config_dict,
            field="app_root_dir",
            field_type=Path,
            error_desc=(
                "a relative path starting from the dir of the config pointing to a"
                " directory with the application code of the experiment"
            ),
            default=Path("app/"),
        )

        cls._process_toml_field(
            config_dict,
            field="app_file_globs",
            field_type=(list, str),
            error_desc="a list of glob patterns to use to select application code",
            default=["**/*"],
        )

        cls._process_toml_field(
            config_dict,
            field="data_root_dir",
            field_type=Path,
            error_desc=(
                "a relative path starting from the dir of the config pointing to a"
                " directory with the needed static data for the experiment"
            ),
            default=Path("data/"),
        )

        cls._process_toml_field(
            config_dict,
            field="data_file_globs",
            field_type=(list, str),
            error_desc="a list of glob patterns to use to select static data",
            default=["**/*"],
        )

        cls._process_toml_field(
            config_dict,
            field="entrypoint",
            field_type=Path,
            error_desc=(
                "a relative path starting from app_root_dir which points to the"
                " entrypoint executable of the experiment"
            ),
        )

        cls._process_toml_field(
            config_dict,
            field="container",
            field_type=ContainerConfig,
            error_desc="a Container config block",
            type_factory=ContainerConfig.from_obj,
        )

        try:
            return cls(**config_dict)
        except Exception:
            raise ConfigParseException("Unhandled config parse error occurred!")
