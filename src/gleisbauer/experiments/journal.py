"""
This module contains everything needed to provide a journal of all currently running
and previously ran experiments. At the moment, data gets serialized as a JSON object
and written to a fixed location. Every read and write requires the allocation of a
UNIX style file lock to prevent multiple reads and writes at the same time. This
provides in a very broad usage of the term a ACID like system.

If at some point problems with this occur, porting it to SQLite or something else
with stricter ACID handling should be simple with minimal code changes for other
parts of the code base which use it.
"""

import getpass
import os
import socket
import time
import uuid
from datetime import datetime
from json import JSONDecodeError

from dataclasses import dataclass, field

import gleisbauer.utils.json_ as json
from gleisbauer import container
from gleisbauer.errors import AllPortsAllocatedException
from gleisbauer.utils.io_ import FileLock, volatile_tmp_home
from gleisbauer.utils.typing_ import (
    Dict,
    List,
    Optional,
    Sequence,
    TypeAlias,
    Literal,
    Callable,
    Tuple,
)


ExpId: TypeAlias = str

_journal_path = volatile_tmp_home() / "journal.json"
_sshd_port_range = (14000, 15000)

# todo: this is a bit weird as it creates the dir in every case, but creating a
#  empty dir in the volatile tmp dir is probably fine and solving this in a cleaner
#  way seems quite convoluted
_journal_path.parent.mkdir(mode=0o777, exist_ok=True)


def gen_exp_id() -> ExpId:
    return uuid.uuid4().hex


# todo: move the following three functions to a utils module?


def _proc_exists(pid: int) -> bool:
    try:
        os.kill(pid, 0)
        return True
    except OSError:
        return False


def _tcp_port_is_free(port: int) -> bool:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.bind(("localhost", port))
            return True
        except OSError:
            return False


def _next_free_sshd_port(allocated_ports: Sequence[int]) -> int:
    port_set = set(allocated_ports)

    for port in range(*_sshd_port_range):
        if port in port_set:
            continue
        if _tcp_port_is_free(port):
            return port

    raise AllPortsAllocatedException()


def default_journal_lock() -> FileLock:
    return FileLock(_journal_path)


Status = Literal[
    "preparing", "waiting", "running", "finished", "stopped", "failed", "unknown"
]


@dataclass
class ContainerRunMetadata:
    container_id: str
    start_time: datetime
    primary_host: str
    device_ids: List[str] = field(default_factory=list)
    other_hosts: List[str] = field(default_factory=list)


@dataclass
class Experiment:
    id: ExpId
    owner: str
    handler_pid: int
    status: Status
    sshd_port: int
    time: datetime
    metadata: Optional[ContainerRunMetadata] = None

    def update(self, **kwargs) -> "Experiment":
        new_kwargs: Dict = vars(self)

        for k, v in kwargs.items():
            new_kwargs[k] = v

        return Experiment(**new_kwargs)


@dataclass
class Journal:
    preparing: Dict[ExpId, Experiment] = field(default_factory=dict)
    waiting: Dict[ExpId, Experiment] = field(default_factory=dict)
    running: Dict[ExpId, Experiment] = field(default_factory=dict)
    # todo: maybe as a dict as a list it is sorting dependent
    wait_queue: List[ExpId] = field(default_factory=list)
    history: List[Experiment] = field(default_factory=list)

    def _clean(self):
        """
        Cleans the Journal from finished experiments and experiments where the process
        spawner exited before the experiment started. Removed experiments get moved
        into the history field.
        """

        engine = container.default_engine()
        for exp_id in list(self.running.keys()):
            exp = self.running[exp_id]
            # todo: seems to doesn't work
            con = engine.find_container(id=exp.metadata.container_id)

            if con is None or con.status == "exited":
                del self.running[exp_id]
                exp = exp.update(
                    status="unknown"
                )  # todo: get the correct exit code and set status based on this
                self.history.append(exp)

        for exp_id in list(self.waiting.keys()):
            exp = self.waiting[exp_id]
            if not _proc_exists(exp.handler_pid):
                del self.waiting[exp_id]
                self.wait_queue.remove(exp_id)
                exp = exp.update(status="stopped")
                self.history.append(exp)

    def write(self, journal_lock: FileLock):
        """
        Writes Journal as json to disk.

        The given journal_lock instance has to be in the acquired state as otherwise
        a ValueError gets raised!
        """

        if not journal_lock.acquired:
            raise ValueError("FileLock is not in the 'acquired' state!")

        self._clean()

        if not journal_lock.target.exists():
            journal_lock.target.touch()
            journal_lock.target.chmod(0o666)

        json_rpr = json.dumps(self, indent=4)
        with journal_lock.open_target("w") as f:
            f.write(json_rpr)

    @property
    def active_experiments(self) -> Dict[ExpId, Experiment]:
        """
        All currently relevant experiments as a single dict instance.
        """

        exps = dict()
        exps.update(self.preparing)
        exps.update(self.waiting)
        exps.update(self.running)
        return exps

    @classmethod
    def read(cls, journal_lock: FileLock) -> "Journal":
        """
        Reads a Journal from a file and returns the instance it creates from the data.

        The given journal_lock instance has to be in the acquired state as otherwise
        a ValueError gets raised!

        If the file locked by the journal_lock doesn't exist an empty Journal gets
        created.
        """

        if not journal_lock.acquired:
            raise ValueError("FileLock is not in the 'acquired' state!")

        if not _journal_path.exists():
            return cls()
        with journal_lock.open_target() as f:
            journal_str = f.read().rstrip()

            if not journal_str:
                return cls()

            try:
                journal = json.loads(journal_str)
            except JSONDecodeError:
                journal_lock.target.unlink()
                return cls()

            if not isinstance(journal, cls):
                ValueError(
                    "The file at the position of the journal doesn't contain a valid"
                    " journal!"
                )
            return journal

    @classmethod
    def init_exp(cls, journal_lock: FileLock, exp_id: ExpId) -> Experiment:
        """
        Reads the Journal from disk and initializes a new experiment with the given
        exp_id. The initialized experiment has a reserved sshd_port but will not do
        anything until try_exp_start gets called for it.

        If the given exp_id is already known a ValueError gets raised.

        This function updates the journal file on the disk and will therefore also
        remove no longer needed experiments.
        """

        journal = cls.read(journal_lock)
        active_exps = journal.active_experiments

        if exp_id in active_exps:
            raise ValueError(f"exp_id {exp_id} is already in use!")

        sshd_port = _next_free_sshd_port([e.sshd_port for e in active_exps.values()])

        exp = Experiment(
            id=exp_id,
            owner=getpass.getuser(),
            handler_pid=os.getpid(),
            status="preparing",
            sshd_port=sshd_port,
            time=datetime.now(),
        )

        journal.preparing[exp_id] = exp

        journal.write(journal_lock)

        return exp

    @classmethod
    def try_exp_start(
        cls,
        journal_lock: FileLock,
        exp_id: ExpId,
        start_exp_container: Callable[
            ["Journal"], Optional[Tuple[str, List[str], List[str], str]]
        ],
    ) -> bool:
        """
        Reads the Journal from disk and checks if the Experiment belonging to the
        exp_id can be changed to the \"running\" status. If the experiment was
        previously in the \"preparing\" state it will either be started or be put
        into the waiting queue. If the experiment has status \"waiting\" and the
        function gets called for it, it will check if it is the first experiment in
        the queue and if so will call the starter_func function and change the
        status to \"running\".

        This function updates the journal file on the disk and will therefore also
        remove no longer needed experiments.
        """

        journal = cls.read(journal_lock)
        active_exps = journal.active_experiments

        exp = active_exps.get(exp_id, None)
        if not exp:
            raise ValueError(f"exp_id {exp_id} is not a currently active experiment!")

        if exp.status not in ["preparing", "waiting"]:
            raise ValueError(
                "experiments can only be started if they have a status of value"
                ' "preparing" or "waiting"'
            )

        if exp.status == "preparing":
            exp = exp.update(status="waiting", handler_pid=os.getpid())

            del journal.preparing[exp_id]
            journal.waiting[exp_id] = exp
            journal.wait_queue.append(exp_id)
            journal.write(journal_lock)

        if len(journal.wait_queue) == 0:
            raise ValueError(
                f"experiment {exp_id} is not in the wait queue but has status"
                ' "waiting"!'
            )

        if journal.wait_queue[0] == exp_id:
            metadata_args = start_exp_container(journal)

            if metadata_args is not None:
                container_id, device_ids, other_hosts, primary_host = metadata_args

                exp = exp.update(
                    status="running",
                    metadata=ContainerRunMetadata(
                        container_id=container_id,
                        device_ids=device_ids,
                        start_time=datetime.now(),
                        other_hosts=other_hosts,
                        primary_host=primary_host,
                    ),
                )

                del journal.waiting[exp_id]
                journal.wait_queue.remove(exp_id)
                journal.running[exp_id] = exp
                journal.write(journal_lock)

                return True

        return False

    @classmethod
    def continuously_try_exp_start(
        cls,
        exp_id: ExpId,
        start_exp_container: Callable[
            ["Journal"], Optional[Tuple[str, List[str], List[str], str]]
        ],
        journal_lock: Optional[FileLock] = None,
        wait_time: float = 30.0,
    ):
        """
        Continuously tries to start an experiment until it succeeds. Each try it
        locks the journal and releases the lock for it after checking if it can be
        started.
        """

        if not journal_lock:
            journal_lock = default_journal_lock()

        if journal_lock.is_locked:
            raise ValueError("journal_lock should not be in the 'acquired' state!")

        while True:
            with journal_lock:
                if cls.try_exp_start(journal_lock, exp_id, start_exp_container):
                    return
            time.sleep(wait_time)
