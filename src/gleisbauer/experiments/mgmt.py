import getpass
import logging
import os
import shlex
import shutil
import stat
from pathlib import Path

from dataclasses import dataclass, field

from gleisbauer import container, reporting, templates
from gleisbauer.communication import messages, ssh
from gleisbauer.errors import ExpInitException
from gleisbauer.experiments.config import Config
from gleisbauer.experiments import journal
from gleisbauer.utils import project
from gleisbauer.utils.docker_ import CopyJobList, CopyJobs
from gleisbauer.utils.io_ import DataDirId
from gleisbauer.utils.ssh import gen_key_pair
from gleisbauer.utils.typing_ import (
    Any,
    List,
    Optional,
    Sequence,
    Tuple,
    Union,
)


logger = logging.getLogger(__name__)


@dataclass
class BuildReportMsg(messages.Message):

    data_root_dir: Path

    @dataclass
    class ResponseMsg(messages.InfoMsg):
        report: "reporting.Report"

    def handle(self, writer: messages.MessageWriter):
        logger.debug("Handling BuildReportMsg")
        writer.write(
            BuildReportMsg.ResponseMsg(reporting.Report.gather(self.data_root_dir))
        )


def gather_reports_from_nodes(
    coms: ssh.SSHNodeCommunicator,
    data_root_dir: Path,
) -> List[Tuple[ssh.SSHConnectionInfo, reporting.Report]]:

    report_procs = coms.send_to_all(BuildReportMsg(data_root_dir))
    proc_replies = ssh.SSHRemoteProcess.read_all(report_procs, join_before=True)

    reports = []
    for replies in proc_replies:
        if len(replies) != 1:
            raise RuntimeError("todo: write a better error")

        msg = replies[0]
        if not isinstance(msg, BuildReportMsg.ResponseMsg):
            raise RuntimeError("todo: write a better error")

        reports.append(msg.report)

    return list(zip([p.node for p in report_procs], reports))


@dataclass
class LoadImageMsg(messages.Message):
    """
    Message which just loads a container image from a given path and returns.
    """

    img_file: Path

    def handle(self, writer: messages.MessageWriter):
        logger.debug("Handling LoadImageMsg")
        container.default_engine().load_image(self.img_file)


@dataclass
class DistributeImageMsg(messages.Message):
    """
    Message which either builds + dumps, loads or just dumps an existing image on a
    node and after this transfers the image in question to other nodes and loads it
    there.

    The img_file doesn't get removed on the nodes and have to be cleaned by another
    message!
    """

    recipient_nodes: List[ssh.SSHConnectionInfo]
    img_name: str
    build_img: bool
    img_file: Path

    def handle(self, writer: messages.MessageWriter):
        logger.debug("Handling DistributeImageMsg")
        engine = container.default_engine()

        if self.build_img:
            constructed_img = engine.build_image(self.img_file, self.img_name)
            if self.recipient_nodes:
                self.img_file.unlink()
                engine.save_image(constructed_img, self.img_file)
        elif self.img_name and not self.img_file.exists() and self.recipient_nodes:
            found_img = engine.find_image(name=self.img_name)
            if not found_img:
                raise ValueError(
                    f"Requested image with name {self.img_name} not found!"
                )
            engine.save_image(found_img, self.img_file)
        else:
            _ = engine.load_image(self.img_file)

        if self.recipient_nodes:
            coms = ssh.SSHNodeCommunicator()
            for node in self.recipient_nodes:
                coms.add_node(node.hostname, node.port, node.user)

            # todo: coms.cp_to_all(...) would be cool, but is with the current cp_to_all
            #  implementation complicated
            for node in self.recipient_nodes:
                coms.cp(node, [(self.img_file, self.img_file)])

            ssh.SSHRemoteProcess.join_all(coms.send_to_all(LoadImageMsg(self.img_file)))


@dataclass
class RemoveTreeMsg(messages.Message):
    path_or_paths: Union[Path, Sequence[Path]]
    missing_okay: bool = True

    def handle(self, writer: messages.MessageWriter):
        logger.debug("Handling RemoveTreeMsg")
        if isinstance(self.path_or_paths, Path):
            self.path_or_paths = [self.path_or_paths]

        for path in self.path_or_paths:
            if self.missing_okay and not path.exists():
                continue
            shutil.rmtree(path.as_posix())
            # todo: correct error handling would be to catch exceptions and send them
            #  all back if they occur


def _prepare_worker_image(
    exp_id: journal.ExpId,
    config: Config,
    reports_per_node: Sequence[Tuple[ssh.SSHConnectionInfo, reporting.Report]],
    coms: ssh.SSHNodeCommunicator,
) -> str:

    engine = container.default_engine()

    logger.debug(f"building image {config.container.image_name}")

    # building is needed to get the size and id of the resulting img
    img = engine.build_image(config.container.buildfile, config.container.image_name)

    fq_img_name = img.tags[0]

    logger.debug(f"constructed image {fq_img_name}")

    remote_tmp_dir = config.remote_tmp_dir(exp_id)

    # todo: fill me with something different
    local_img_file = remote_tmp_dir / f"{img.tags[0]}.tar"
    remote_img_file = remote_tmp_dir / f"{img.tags[0]}.tar"

    nodes_with_img = [
        n for n, r in reports_per_node if r.container_image_exists(name=fq_img_name)
    ]
    nodes_without_img = [
        n for n, r in reports_per_node if not r.container_image_exists(name=fq_img_name)
    ]

    if nodes_without_img:
        logger.debug(f"distributing image {fq_img_name}")

        # todo: do a file system check with 2 * img.size (tar + loaded img) --> needs
        #  location to check for space of docker images

        if nodes_with_img:
            coms.send(
                nodes_with_img[0],
                DistributeImageMsg(
                    nodes_without_img,
                    config.container.image_name,
                    False,
                    remote_img_file,
                ),
            ).join()
        else:
            if not config.container.build_on_remote:
                engine.save_image(img, local_img_file)
            else:
                engine.save_build_context_archive(
                    config.container.buildfile, local_img_file
                )

            coms.cp(nodes_without_img[0], [(local_img_file, remote_img_file)])
            coms.send(
                nodes_without_img[0],
                DistributeImageMsg(
                    nodes_without_img[1:],
                    config.container.image_name,
                    config.container.build_on_remote,
                    remote_img_file,
                ),
            ).join()

    return fq_img_name


def _prepare_exp(
    config: Config,
    config_file: Path,
    coms: ssh.SSHNodeCommunicator,
) -> journal.ExpId:

    # todo: do fs space checks
    data_dir_id = DataDirId.build(config.data_root_dir)

    exp_id = journal.gen_exp_id()

    remote_app_files = [config.remote_exp_app_dir(exp_id) / f for f in config.app_files]
    remote_config_file = config.remote_exp_dir(exp_id) / config_file.name

    remote_data_files = [
        config.remote_data_root_dir / data_dir_id.hexdigest / f
        for f in config.data_files
    ]

    copy_jobs = (
        list(zip(config.app_files_with_root, remote_app_files))
        + list(zip(config.data_files_with_root, remote_data_files))
        + [(config_file, remote_config_file)]
    )

    # todo: returning a iterator results in a strange api
    list(coms.cp_to_all_from_client(copy_jobs))

    coms.ln_on_all(
        config.remote_data_root_dir / data_dir_id.hexdigest,
        config.remote_exp_data_dir(exp_id),
    )

    return exp_id


def _prepare_ssh_copy_jobs(
    home_dir: Path, hostnames: Sequence[str], ports: Sequence[int], username: str
) -> Tuple[CopyJobList, CopyJobList]:

    """
    Creates copy jobs for files which are needed for the ssh server and ssh
    authentication.

    All copy jobs are created without uid and gid to make them generic for all nodes.
    """

    private_key, public_key = gen_key_pair()
    private_host_key, public_host_key = gen_key_pair()

    # creates an authorized_keys file which only allows access from the other nodes
    # based on their hostnames
    # hostnames = ",".join([t[0] for t in worker_names_and_ports])
    # todo: restricting to hosts fails if there is no reverse dns entry :(. maybe
    #  resolving hostname to ip is a better approach?
    # authorized_keys = f'from="{hostnames}" {public_key.decode()}'.encode()
    authorized_keys = public_key

    client_config_lines = []
    for hostname, port in zip(hostnames, ports):
        client_config_lines.append(f"Host {hostname}")
        client_config_lines.append(f"    Port {port}")
        client_config_lines.append("    StrictHostKeyChecking no")
        client_config_lines.append("    IdentityFile ~/.ssh/id")
    client_config = "\n".join(client_config_lines).encode()

    sshd_config = templates.read(
        "sshd_config", template_kwargs=dict(user=username)
    ).encode()

    return [
        (home_dir / ".ssh", None, 0o700),
        (home_dir / ".ssh/id.pub", public_key, 0o644),
        (home_dir / ".ssh/host_rsa_key", private_host_key, 0o600),
        (home_dir / ".ssh/host_rsa_key.pub", public_host_key, 0o644),
        (home_dir / ".ssh/authorized_keys", authorized_keys, 0o600),
        (home_dir / ".ssh/sshd_config", sshd_config, 0o644),
        (home_dir / ".ssh/config", client_config, 0o600),
    ], [
        (home_dir / ".ssh/id", private_key, 0o600),
    ]


@dataclass
class StartExpMsg(messages.Message):

    exp_id: journal.ExpId
    fq_img_name: str
    config: Config
    nodes: List[ssh.SSHConnectionInfo]
    num_gpus_per_node: List[int]
    entrypoint_args: Optional[str]

    @dataclass
    class PrepareExpRespMsg(messages.InfoMsg):
        exp: journal.Experiment

    @dataclass
    class PrepareExpMsg(messages.Message):
        exp_id: journal.ExpId

        def handle(self, writer: messages.MessageWriter):
            with journal.default_journal_lock() as lock:
                exp = journal.Journal.init_exp(lock, self.exp_id)
                writer.write(StartExpMsg.PrepareExpRespMsg(exp))

    @dataclass
    class SpawnExpWorkerContainerMsg(messages.Message):
        exp_id: journal.ExpId
        fq_img_name: str
        username: str
        remote_out_root: Path
        primary_host: str
        primary_port: int
        sshd_port: int
        num_gpus: int
        mounts: List[container.HostMount] = field(default_factory=list)
        copy_jobs: CopyJobs = field(default_factory=list)
        primary: bool = False
        # fields after this are only needed if primary is set to true
        secondary_hosts: List[str] = field(default_factory=list)
        secondary_ports: List[int] = field(default_factory=list)
        excluded_if_names: List[str] = field(default_factory=list)
        abs_entrypoint_file: Path = None
        entrypoint_args: Optional[str] = None
        num_gpus_per_host: List[int] = field(default_factory=list)

        def handle(self, writer: messages.MessageWriter):
            logger.debug("Handling SpawnExpWorkerContainerMsg")
            container_name = f"{self.username}_{self.exp_id}"

            self.remote_out_root.mkdir()

            if self.primary:
                if self.abs_entrypoint_file is None:
                    raise ValueError(
                        "if primary is set hosts and entrypoint_file must also be set!"
                    )

                if self.num_gpus_per_host and (
                    (len(self.secondary_hosts) + 1) != len(self.num_gpus_per_host)
                ):
                    raise ValueError(
                        "if num_gpus_per_host is given it must contain as many entries"
                        " as there are hosts!"
                    )

                bootstrapper_args = (
                    f"--primary-host {self.primary_host} --entrypoint-file"
                    f" {self.abs_entrypoint_file.as_posix()} --sshd-port"
                    f" {self.sshd_port} --num-gpus-per-host"
                    f" {' '.join([str(n) for n in self.num_gpus_per_host])}"
                )

                if self.entrypoint_args:
                    bootstrapper_args += (
                        f" --entrypoint-args {shlex.quote(self.entrypoint_args)}"
                    )

                if self.secondary_hosts:
                    bootstrapper_args += (
                        " --secondary-hosts"
                        f" {' '.join(self.secondary_hosts)} --secondary-ports"
                        f" {' '.join([str(p) for p in self.secondary_ports])}"
                    )

                if self.excluded_if_names:
                    bootstrapper_args += (
                        f" --excluded-if-names {' '.join(self.excluded_if_names)}"
                    )

                cmd = (
                    'bash -c "python3 /exp_bootstrapper.py primary'
                    f' {bootstrapper_args}"'
                )
            else:
                cmd = (
                    'bash -c "python3 /exp_bootstrapper.py worker --sshd-port'
                    f" {self.sshd_port} --primary-host"
                    f' {self.primary_host} --primary-sshd-port {self.primary_port}"'
                )

            logging.debug(f"container cmd {cmd}")

            def _start(
                journal_: journal.Journal,
            ) -> Optional[Tuple[str, List[str], List[str], str]]:
                report = reporting.Report.gather(journal=journal_)

                if report.num_free_gpus < self.num_gpus:
                    return None

                device_ids = report.free_gpu_ids[: self.num_gpus]

                engine = container.default_engine()
                con = engine.start_container(
                    image_name=self.fq_img_name,
                    command=cmd,
                    username=self.username,
                    uid=os.getuid(),
                    gid=os.getgid(),
                    name=container_name,
                    device_ids=device_ids,
                    host_mounts=self.mounts,
                    copy_jobs=self.copy_jobs,
                )

                return (
                    con.id,
                    device_ids,
                    self.secondary_hosts,
                    self.primary_host,
                )

            journal.Journal.continuously_try_exp_start(self.exp_id, _start)

    @staticmethod
    def build_copy_jobs(
        username: str, hostnames: Sequence[str], ports: Sequence[int]
    ) -> Tuple[CopyJobList, CopyJobList]:

        home_dir = Path("home") / username

        generic_cps: Any = [
            (home_dir, None, 0o700),
            (Path("experiment"), None, 0o700),
            (
                Path("exp_bootstrapper.py"),
                templates.read("exp_bootstrapper.py").encode(),
                0o555,
            ),
        ]
        prim_only_cps: Any = []

        generic_ssh_copy_jobs, primary_ssh_copy_jobs = _prepare_ssh_copy_jobs(
            home_dir, hostnames, ports, username
        )

        generic_cps += generic_ssh_copy_jobs
        prim_only_cps += primary_ssh_copy_jobs

        return generic_cps, prim_only_cps

    def build_experiment_data_mounts(self) -> List[container.HostMount]:
        return [
            container.HostMount(
                source=self.config.remote_exp_app_dir(self.exp_id).resolve(),
                target=Path("/experiment/") / self.config.app_root_dir,
            ),
            container.HostMount(
                source=self.config.remote_exp_data_dir(self.exp_id).resolve(),
                target=Path("/experiment/") / self.config.data_root_dir,
            ),
            container.HostMount(
                source=self.config.remote_exp_out_dir(self.exp_id).resolve(),
                target=Path("/experiment/out"),
                read_only=False,
            ),
        ]

    def handle(self, writer: messages.MessageWriter):
        logger.debug("Handling StartExpMsg")

        coms = ssh.SSHNodeCommunicator()
        for node in self.nodes:
            coms.add_node(node.hostname, node.port, node.user)

        username = getpass.getuser()

        primary_host = coms.primary_node.hostname
        primary_port = coms.primary_node.port
        hostnames = [n.hostname for n in self.nodes]

        prep_procs = coms.send_to_all(StartExpMsg.PrepareExpMsg(self.exp_id))
        prep_resps: Any = ssh.SSHRemoteProcess.read_all(prep_procs, join_before=True)
        ports = [resp[0].exp.sshd_port for resp in prep_resps]

        generic_cps, prim_only_cps = self.build_copy_jobs(username, hostnames, ports)
        mounts = self.build_experiment_data_mounts()

        def _build_msg(primary: bool, num_gpus: int, port: int) -> messages.Message:
            return StartExpMsg.SpawnExpWorkerContainerMsg(
                exp_id=self.exp_id,
                fq_img_name=self.fq_img_name,
                username=username,
                remote_out_root=self.config.remote_exp_out_dir(self.exp_id),
                primary_host=primary_host,
                primary_port=primary_port,
                sshd_port=port,
                mounts=mounts,
                copy_jobs=generic_cps if not primary else generic_cps + prim_only_cps,
                primary=primary,
                secondary_hosts=[n.hostname for n in coms.worker_nodes],
                secondary_ports=ports[
                    1:
                ],  # todo: this need knowledge about which node is the "primary" one
                excluded_if_names=["lo", "docker0"],
                abs_entrypoint_file=Path("/experiment")
                / self.config.app_root_dir
                / self.config.entrypoint,
                num_gpus_per_host=self.num_gpus_per_node,
                num_gpus=num_gpus,
                entrypoint_args=self.entrypoint_args,
            )

        for i, node in enumerate(coms.nodes):
            coms.send(
                node,
                _build_msg(
                    primary=node.hostname == primary_host,
                    num_gpus=self.num_gpus_per_node[i],
                    port=ports[i],
                ),
                disown=True,
            )


def _select_gpu_cluster_nodes(
    reports_per_node: List[Tuple[ssh.SSHConnectionInfo, reporting.Report]],
    num_gpus: int,
    no_job_queuing: bool,
    gpu_min_mem: int,
) -> Tuple[
    ssh.SSHNodeCommunicator,
    List[Tuple[ssh.SSHConnectionInfo, reporting.Report]],
    List[int],
]:

    reports_per_node = [
        (n, r) for n, r in reports_per_node if r.num_gpus_with_min_mem(gpu_min_mem) > 0
    ]
    reports_per_node.sort(key=lambda t: t[1].num_free_gpus, reverse=True)

    cluster_coms = ssh.SSHNodeCommunicator()

    # todo: add support for cpu only trainings
    gpu_selection = []
    for node, report in reports_per_node:
        if sum(gpu_selection) >= num_gpus:
            break

        if no_job_queuing:
            gpu_selection.append(report.num_free_gpus)
        else:
            gpu_selection.append(report.num_gpus)

        cluster_coms.add_node(node.hostname, node.port, node.user)

    if sum(gpu_selection) > num_gpus:
        gpu_selection[-1] = sum(gpu_selection) - num_gpus

    if sum(gpu_selection) < num_gpus:
        raise RuntimeError(f"not enough free GPUs available on the nodes: {' '.join([t[0].as_string() for t in reports_per_node])}")

    return cluster_coms, reports_per_node, gpu_selection


def run_exp(
    coms: ssh.SSHNodeCommunicator,
    num_gpus: int,
    gpu_min_mem: int,
    no_job_queuing: bool,
    entrypoint_args: Optional[str],
) -> journal.ExpId:

    config_file = Path(f"{project.proj_bin_name()}.toml")
    config = Config.from_toml_string(config_file.read_text())

    # todo: move this into a function which all can call at the start
    coms.mkdir_on_all(config.remote_root, mode=0o777)
    coms.mkdir_on_all(config.remote_data_root_dir, mode=0o777)
    coms.mkdir_on_all(config.remote_exp_root_dir, mode=0o777)
    coms.mkdir_on_all(journal.default_journal_lock().target.parent, mode=0o777)

    container.set_engine(config.container.engine)

    reports_per_node = gather_reports_from_nodes(coms, config.remote_data_root_dir)

    logger.info("selecting nodes with gpus")

    cluster_coms, cluster_rep_per_node, gpu_selection = _select_gpu_cluster_nodes(
        reports_per_node=reports_per_node,
        num_gpus=num_gpus,
        no_job_queuing=no_job_queuing,
        gpu_min_mem=gpu_min_mem,
    )

    logger.info(f"selected {cluster_coms.primary_node.as_string()} as primary node")
    if cluster_coms.worker_nodes:
        logger.info(
            "selected"
            f" {','.join([n.as_string() for n in cluster_coms.worker_nodes])} as"
            " secondary node"
        )

    logger.info("preparing experiment")

    exp_id = _prepare_exp(
        config=config,
        config_file=config_file,
        coms=cluster_coms,
    )

    logger.info(f"prepared experiment {exp_id}")

    try:

        logger.info(f"preparing worker image")

        fq_img_name = _prepare_worker_image(
            exp_id=exp_id,
            config=config,
            reports_per_node=cluster_rep_per_node,
            coms=cluster_coms,
        )

        logger.info(f"prepared worker image")

        logger.info(
            "starting containers on"
            f" {','.join([n.as_string() for n in cluster_coms.nodes])}"
        )

        cluster_coms.send_to_primary(
            StartExpMsg(
                exp_id=exp_id,
                fq_img_name=fq_img_name,
                config=config,
                num_gpus_per_node=gpu_selection,
                nodes=cluster_coms.nodes,
                entrypoint_args=entrypoint_args,
            )
        ).join()
    finally:
        ssh.SSHRemoteProcess.join_all(
            coms.send_to_all(RemoveTreeMsg(config.remote_tmp_dir(exp_id)))
        )

    history_file = Path(f".{project.proj_bin_name()}_history")
    with history_file.open("a") as f:
        new_line = f"{exp_id};started;{cluster_coms.primary_node.as_string()}"
        if cluster_coms.worker_nodes:
            new_line += (
                f";{';'.join([n.as_string() for n in cluster_coms.worker_nodes])}"
            )
        f.write(new_line + "\n")

    return exp_id


def query_nodes(coms: ssh.SSHNodeCommunicator):
    config_file = Path(f"{project.proj_bin_name()}.toml")
    config = Config.from_toml_string(config_file.read_text())

    reports_per_node = gather_reports_from_nodes(
            coms, config.remote_data_root_dir
    )

    for i, (node, report) in enumerate(reports_per_node):
        if i != 0:
            print("")
        print("-" * 80)
        print(node)
        print("-" * 80)
        print("")

        report.print()


def query_exp(coms: ssh.SSHNodeCommunicator):
    raise NotImplementedError()


def pull_exp(coms: ssh.SSHNodeCommunicator):
    raise NotImplementedError()


def init_exp():
    curr_dir = Path.cwd().resolve()

    if next(curr_dir.iterdir(), None):
        raise ExpInitException(f"{curr_dir.as_posix()} is not empty!")

    name = curr_dir.name
    entrypoint = Path("app/main.py")
    data_dir = Path("data")

    with Path(f"{project.proj_bin_name()}.toml").open("x") as f:
        f.write(
                templates.read("config.toml", dict(name=name, entrypoint=entrypoint.name))
        )

    with Path("Dockerfile").open("x") as f:
        f.write(templates.read("Dockerfile"))

    with Path(".dockerignore").open("x") as f:
        f.write(templates.read("dockerignore"))

    entrypoint.parent.mkdir()
    with Path(entrypoint).open("x") as f:
        f.write(templates.read("entrypoint.py"))
        entrypoint.chmod(entrypoint.stat().st_mode | stat.S_IEXEC)

    data_dir.mkdir()

    print(f'initialized empty experiment dir "{curr_dir.as_posix()}"')
