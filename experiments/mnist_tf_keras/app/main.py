#!/usr/bin/env python3
import datetime
from pathlib import Path

from tensorflow import keras
import tensorflow as tf
import tensorflow_datasets as tfds
import horovod.tensorflow.keras as hvd


exp_dir = Path(__file__).resolve().parent.parent


class TimerCallback(keras.callbacks.Callback):
    def __init__(self):
        self.start = None

    def on_epoch_begin(self, epoch, logs=None):
        self.start = datetime.datetime.now()

    def on_epoch_end(self, epoch, logs=None):
        end = datetime.datetime.now()
        print(f"Epoch {epoch} of training took {end - self.start}")


def main():
    hvd.init()

    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        tf.config.set_visible_devices(gpus[hvd.local_rank()], device_type="GPU")

    (ds_train, ds_test), ds_info = tfds.load(
        "mnist",
        split=["train", "test"],
        shuffle_files=True,
        as_supervised=True,
        with_info=True,
        data_dir=(exp_dir / "data").as_posix(),
    )

    def normalize_img(image, label):
        return tf.cast(image, tf.float32) / 255.0, label

    ds_train = ds_train.shard(hvd.size(), hvd.rank())
    ds_train = ds_train.map(normalize_img, num_parallel_calls=tf.data.AUTOTUNE)
    ds_train = ds_train.cache()
    ds_train = ds_train.shuffle(ds_info.splits["train"].num_examples)
    ds_train = ds_train.batch(128)
    ds_train = ds_train.prefetch(tf.data.AUTOTUNE)

    ds_test = ds_test.map(normalize_img, num_parallel_calls=tf.data.AUTOTUNE)
    ds_test = ds_test.batch(128)
    ds_test = ds_test.cache()
    ds_test = ds_test.prefetch(tf.data.AUTOTUNE)

    # model = tf.keras.models.Sequential(
    #     [
    #         tf.keras.layers.Flatten(input_shape=(28, 28)),
    #         tf.keras.layers.Dense(128, activation="relu"),
    #         tf.keras.layers.Dense(10),
    #     ]
    # )

    model = tf.keras.applications.MobileNetV2(
        input_shape=(28, 28, 1), classes=10, weights=None
    )

    raw_opt = tf.keras.optimizers.Adam(0.001 * hvd.size())
    dist_opt = hvd.DistributedOptimizer(raw_opt)

    if hvd.rank() == 0:
        model.summary()

    model.compile(
        optimizer=dist_opt,
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
        experimental_run_tf_function=False,
    )

    callbacks = [
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
        TimerCallback(),
    ]

    if hvd.rank() == 0:
        callbacks.append(
            keras.callbacks.ModelCheckpoint(exp_dir / "out" / "checkpoint-{epoch}.h5")
        )

    model.fit(
        ds_train,
        epochs=100,
        validation_data=ds_test,
        callbacks=callbacks,
        verbose=2 if hvd.rank() == 0 else 0,
    )


if __name__ == "__main__":
    main()
