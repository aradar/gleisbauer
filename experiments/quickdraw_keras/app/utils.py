import datetime

import tensorflow as tf
from tensorflow import keras


class TimerCallback(keras.callbacks.Callback):
    def __init__(self):
        self.start = None

    def on_epoch_begin(self, epoch, logs=None):
        self.start = datetime.datetime.now()

    def on_epoch_end(self, epoch, logs=None):
        end = datetime.datetime.now()
        print(f"Epoch {epoch + 1} of training took {end - self.start}")


class WarmedUpPolynomialDecayCallback(keras.callbacks.Callback):

    # todo: does momentum correction as seen here https://arxiv.org/pdf/1706.02677.pdf
    #  and after it does a polynomial decay schedule as it used for LARS

    def __init__(
        self,
        peak_lr: float,
        final_lr: float,
        num_warmup_steps: int,
        final_step: int,
        warmup_pow: float,
        decay_pow: float,
        model: tf.keras.Model,
        init_lr: float = 0.0,
        momentum_correction: bool = True,
    ):

        self._peak_lr = peak_lr
        self._final_lr = final_lr
        self._num_warmup_steps = num_warmup_steps
        self._final_step = final_step
        self._warmup_pow = warmup_pow
        self._decay_pow = decay_pow
        self._init_lr = init_lr
        self._model = model

        self._step = 0
        self._model.optimizer.learning_rate.assign(init_lr)
        if momentum_correction and getattr(self._model.optimizer, "momentum"):
            self._momentum = self._model.optimizer.momentum
        else:
            self._momentum = None
        self._momentum_correction = self._momentum is not None

    def _in_warmup_phase(self):
        return self._step <= self._num_warmup_steps

    def _in_decay_phase(self):
        return not self._in_warmup_phase() and self._step < self._final_step

    def _in_endless_phase(self):
        return not self._in_warmup_phase() and not self._in_decay_phase()

    def _start_of_endless_phase(self):
        return self._step == self._final_step

    def _updated_lr(self):
        old_lr = self._model.optimizer.learning_rate.numpy()
        new_lr = None

        if self._in_warmup_phase():
            new_lr = (
                self._init_lr
                + (self._peak_lr - self._init_lr)
                * (self._step / self._num_warmup_steps) ** self._warmup_pow
            )
        elif self._in_decay_phase():
            new_lr = (
                self._final_lr
                + (self._peak_lr - self._final_lr)
                * (
                    (self._final_step - self._step)
                    / (self._final_step - self._num_warmup_steps)
                )
                ** self._decay_pow
            )
        elif self._start_of_endless_phase():
            new_lr = self._final_lr

        return old_lr, new_lr

    def on_train_batch_begin(self, batch, logs=None):
        old_lr, new_lr = self._updated_lr()

        if new_lr is not None:
            self._model.optimizer.learning_rate.assign(new_lr)
            if self._momentum_correction and self._in_warmup_phase() and old_lr != 0.0:
                self._model.optimizer.momentum = self._momentum * new_lr / old_lr

        if self._step % 100 == 0:
            lr = new_lr if new_lr is not None else old_lr
            print(
                f"learning rate and momentum at step {self._step}: {lr},"
                f" {self.model.optimizer.momentum}"
            )
            tf.summary.scalar(name="learning_rate", data=lr, step=self._step)

        self._step += 1

    def on_train_batch_end(self, batch, logs=None):
        if self._momentum_correction:
            self._model.optimizer.momentum = self._momentum


@tf.keras.utils.register_keras_serializable()
class WarmedUpPolynomialDecay(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(
        self,
        peak_lr: float,
        final_lr: float,
        num_warmup_steps: int,
        final_step: int,
        warmup_pow: float,
        decay_pow: float,
        init_lr: float = 0.0,
        name=None,
    ):

        self._peak_lr = peak_lr
        self._final_lr = final_lr
        self._num_warmup_steps = num_warmup_steps
        self._final_step = final_step
        self._warmup_pow = warmup_pow
        self._decay_pow = decay_pow
        self._init_lr = init_lr
        self._name = name

        pass

    def __call__(self, step):
        with tf.name_scope(self._name or "WarmedUpPolynomialDecay"):
            init_lr = tf.constant(self._init_lr, dtype=tf.float64, name="init_lr")
            peak_lr = tf.constant(self._peak_lr, dtype=tf.float64, name="peak_lr")
            final_lr = tf.constant(self._final_lr, dtype=tf.float64, name="final_lr")
            warmup_pow = tf.constant(
                self._warmup_pow, dtype=tf.float64, name="warmup_pow"
            )
            decay_pow = tf.constant(self._decay_pow, dtype=tf.float64, name="decay_pow")
            final_step = tf.constant(
                self._final_step, dtype=tf.int64, name="final_step"
            )
            num_warmup_steps = tf.constant(
                self._num_warmup_steps, dtype=tf.int64, name="num_warmup_steps"
            )

            # before training tf calls the scheduler with a float step :(
            int_step = tf.cast(step, dtype=tf.int64)

            def warmup():
                return (
                    init_lr
                    + (peak_lr - init_lr) * (int_step / num_warmup_steps) ** warmup_pow
                )

            def decay():
                return (
                    final_lr
                    + (peak_lr - final_lr)
                    * ((final_step - int_step) / (final_step - num_warmup_steps))
                    ** decay_pow
                )

            def default():
                return final_lr

            lr = tf.case(
                pred_fn_pairs=[
                    (tf.less_equal(int_step, num_warmup_steps), warmup),
                    (
                        tf.logical_and(
                            tf.greater(int_step, num_warmup_steps),
                            tf.less_equal(int_step, final_step),
                        ),
                        decay,
                    ),
                ],
                default=default,
                exclusive=True,
            )

            return tf.cast(lr, dtype=tf.float32)

    def get_config(self):
        return {
            "peak_lr": self._peak_lr,
            "final_lr": self._final_lr,
            "num_warmup_steps": self._num_warmup_steps,
            "final_step": self._final_step,
            "warmup_pow": self._warmup_pow,
            "decay_pow": self._decay_pow,
            "init_lr": self._init_lr,
            "name": self._name,
        }
