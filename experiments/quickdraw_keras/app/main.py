#!/usr/bin/env python3
import argparse
from pathlib import Path

import horovod.tensorflow.keras as hvd
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow import keras

from lars_optimizer import LARS
from utils import (
    WarmedUpPolynomialDecayCallback,
    TimerCallback,
)

exp_dir = Path(__file__).resolve().parent.parent


def main():
    # --reduced-ds --optimizer lars --batch-size 128 --peak-lr 2.0
    # --reduced-ds --optimizer lars --batch-size 128 --peak-lr 1.4
    # --reduced-ds --optimizer lars --batch-size 128 --peak-lr 1.2
    # --reduced-ds --optimizer lars --batch-size 128 --peak-lr 1.0

    # --reduced-ds --optimizer lars --batch-size 4096 --peak-lr 7.0
    # --reduced-ds --optimizer lars --batch-size 4096 --peak-lr 8.0
    # --reduced-ds --optimizer lars --batch-size 4096 --peak-lr 9.0

    parser = argparse.ArgumentParser()
    parser.add_argument("--batch-size", type=int, default=4096)
    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--reduced-ds", action="store_true", default=False)
    parser.add_argument("--peak-lr", type=float, default=2.0)
    parser.add_argument("--optimizer", type=str, default="lars")

    args = parser.parse_args()

    hvd.init()

    batch_size = args.batch_size  # small batch size training
    num_epochs = args.epochs

    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        tf.config.set_visible_devices(gpus[hvd.local_rank()], device_type="GPU")
        tf.config.experimental.set_memory_growth(gpus[hvd.local_rank()], True)

    ds_builder = tfds.builder(
        "quickdraw_bitmap", data_dir=(exp_dir / "data").as_posix()
    )

    ds_info = ds_builder.info
    num_classes = ds_info.features["label"].num_classes

    ds_train, ds_val, ds_test = ds_builder.as_dataset(
        split=["train[:80%]", "train[80%:90%]", "train[90%:]"], as_supervised=True
    )

    def preprocess_train(image, label):
        image = tf.cast(image, tf.float32) / 255.0
        image = tf.image.pad_to_bounding_box(image, 2, 2, 36, 36)
        # min size for keras.applications models is 32
        image = tf.image.random_crop(image, (32, 32, 1))

        label = tf.one_hot(indices=label, depth=num_classes)

        return image, label

    def preprocess(image, label):
        image = tf.cast(image, tf.float32) / 255.0
        # min size for keras.applications models is 32
        image = tf.image.pad_to_bounding_box(image, 2, 2, 32, 32)

        label = tf.one_hot(indices=label, depth=num_classes)

        return image, label

    if args.reduced_ds:
        ds_train = ds_train.take(4096 * 500)
        ds_val = ds_val.take(4096 * 100)
        ds_test = ds_test.take(4096 * 100)

    ds_train = (
        ds_train.shard(hvd.size(), hvd.rank())
        .map(preprocess_train, num_parallel_calls=3)
        .shuffle(4096 * 2, reshuffle_each_iteration=True)
        .batch(batch_size)
        .prefetch(3)
    )

    ds_val = ds_val.map(preprocess, num_parallel_calls=3).batch(batch_size)

    ds_test = ds_test.map(preprocess, num_parallel_calls=3).batch(batch_size)

    input_shape = (32, 32, 1)

    def add_reg(
        model: tf.keras.Model, regularizer: tf.keras.regularizers.Regularizer = None
    ):
        if regularizer is None:
            regularizer = tf.keras.regularizers.L2(1e-4)

        for layer in model.layers:
            if hasattr(layer, "kernel_regularizer"):
                setattr(layer, "kernel_regularizer", regularizer)

    def update_bn(model: tf.keras.Model, epsilon: float, momentum: float):
        for layer in model.layers:
            if isinstance(layer, tf.keras.layers.BatchNormalization):
                layer.epsilon = epsilon
                layer.momentum = momentum

    model = tf.keras.applications.MobileNet(
        input_shape=input_shape,
        classes=num_classes,
        weights=None,
        classifier_activation=None,
        dropout=0.0,
    )
    add_reg(model)
    update_bn(model, 1e-5, 0.9)

    steps_per_epoch = tf.data.experimental.cardinality(ds_train).numpy()

    if args.optimizer == "lars":
        raw_opt = LARS(momentum=0.9)
    else:
        raw_opt = tf.keras.optimizers.SGD(momentum=0.9)
    opt = hvd.DistributedOptimizer(raw_opt)

    # # this seems like a big bug in tensorflow as the learning_rate hyperparam never
    # # gets deserialized if the optimizer gets serialized and deserialized afterwards.
    # # the following code overwrites the internal dict value with the value generated
    # # by deserializing the str representation.
    # opt._hyper["learning_rate"] = tf.keras.optimizers.schedules.deserialize(
    #     opt._hyper["learning_rate"]
    # )

    if hvd.rank() == 0:
        print(ds_info)
        model.summary()

    model.compile(
        optimizer=opt,
        loss=tf.keras.losses.CategoricalCrossentropy(
            from_logits=True, label_smoothing=0.1
        ),  # model ends with softmax
        metrics=[
            tf.keras.metrics.CategoricalAccuracy(),
            tf.keras.metrics.TopKCategoricalAccuracy(),
        ],
        experimental_run_tf_function=False,
    )

    callbacks = [
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
        WarmedUpPolynomialDecayCallback(
            init_lr=0.0,
            # no scaling with hvd.size()
            peak_lr=args.peak_lr,
            final_lr=1e-4,
            num_warmup_steps=int(steps_per_epoch * num_epochs * 0.25),
            final_step=int(steps_per_epoch * num_epochs),
            warmup_pow=1.0,
            decay_pow=2.0,
            model=model,
        ),
        # todo: this only works with update_freq="epoch" and it runs on all ranks??
        #  check this https://github.com/horovod/horovod/issues/3170
        tf.keras.callbacks.TensorBoard(
            log_dir=(exp_dir / "out/tensorboard").as_posix(),
            write_steps_per_second=True,
            update_freq=int(5),  # 100 for slower bs
        ),
    ]

    if hvd.rank() == 0:
        callbacks += [
            TimerCallback(),
            keras.callbacks.ModelCheckpoint(exp_dir / "out" / "checkpoint-{epoch}.h5"),
        ]

    model.fit(
        ds_train,
        epochs=num_epochs,
        validation_data=ds_val,
        callbacks=callbacks,
        verbose=2 if hvd.rank() == 0 else 0,
    )

    if hvd.rank() == 0:
        eval_dict = model.evaluate(
            ds_test, return_dict=True, verbose=2 if hvd.rank() == 0 else 0
        )

        for k, v in eval_dict.items():
            print(k, v)

        with (exp_dir / "out/classes.txt").open("w") as f:
            f.write("\n".join(ds_info.features["label"].names))

        with (exp_dir / "out/test_predictions.bsv").open("w+b") as f:
            field_width = 2
            f.write(field_width.to_bytes(1, "little"))
            eles_per_row = 6
            f.write(eles_per_row.to_bytes(2, "little"))
            for image_batch, label_batch in ds_test:
                predictions = model.predict_on_batch(image_batch)
                for l, p in zip(label_batch.numpy(), predictions):
                    f.write(l.argmax().astype("int16").tobytes())
                    f.write(
                        p.argsort().astype("int16")[::-1][: eles_per_row - 1].tobytes()
                    )


if __name__ == "__main__":
    main()
