#! /usr/bin python3

from pathlib import Path

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from sklearn.metrics import classification_report, confusion_matrix


def read_bsv(file):
    buffer = Path(file).read_bytes()

    field_width = np.frombuffer(buffer, "int8", 1)[0]
    eles_per_row = np.frombuffer(buffer, "int16", 1, 1)[0]

    rows = np.frombuffer(buffer, f"int{field_width * 8}", -1, 3)
    num_rows = int(len(rows) / eles_per_row)
    rows = rows.reshape((num_rows, eles_per_row))

    labels = rows[..., 0]
    top_pred = rows[..., 1:]

    return labels, top_pred


def create_loss_plot(csv_files, arguments, output_file):
    data_parts = list()
    for file, args in zip(csv_files, arguments):
        df = pd.read_csv(file)
        df["Relative training time"] = df["Step"] / df["Step"].max()
        df = df.rename(columns={"Value": "Loss"})
        df["Arguments"] = args

        data_parts.append(df)

    data = pd.concat(data_parts)
    data = data.reset_index(drop=True)

    ax = sns.lineplot(data=data, x="Relative training time", y="Loss", hue="Arguments")

    plt.setp(ax.get_legend().get_texts(), fontsize="9")
    plt.setp(ax.get_legend().get_title(), fontsize="9")

    ax.set(ylim=(1.5, 3.5))

    plt.savefig(output_file)
    plt.close()


def create_acc_plot(train_csv_files, val_csv_files, arguments, output_file):
    data_parts = list()
    for train_file, val_file, args in zip(train_csv_files, val_csv_files, arguments):
        train_df = pd.read_csv(train_file)
        train_df["Relative training time"] = train_df["Step"] / train_df["Step"].max()
        train_df = train_df.rename(columns={"Value": "Accuracy"})
        train_df["Arguments"] = args
        train_df["Dataset"] = "train"
        data_parts.append(train_df)

        val_df = pd.read_csv(val_file)
        val_df["Relative training time"] = val_df["Step"] / val_df["Step"].max()
        val_df = val_df.rename(columns={"Value": "Accuracy"})
        val_df["Arguments"] = args
        val_df["Dataset"] = "val"
        data_parts.append(val_df)

    data = pd.concat(data_parts)
    data = data.reset_index(drop=True)

    ax = sns.lineplot(
        data=data,
        x="Relative training time",
        y="Accuracy",
        hue="Arguments",
        style="Dataset",
    )

    plt.legend(loc="lower left", ncol=2)
    plt.setp(ax.get_legend().get_texts(), fontsize="9")
    plt.setp(ax.get_legend().get_title(), fontsize="9")

    plt.savefig(output_file)
    plt.close()


def latex_report_table_cols(report_dict, include_label, include_line_break, ignore_metrics):
    if not ignore_metrics:
        ignore_metrics = set()

    labels = list(next(iter(report_dict.values())).keys())

    for m_name in ignore_metrics:
        labels.remove(m_name)

    # header row
    if include_label:
        lines = [
            "\\textbf{label} & "
            + " & ".join([f"\\textbf{{{k}}}" for k in labels])
        ]
    else:
        lines = [" & ".join([f"\\textbf{{{k}}}" for k in labels])]
    if include_line_break:
        lines[-1] += " \\\\ \\hline"

    # data rows
    for label, metrics in report_dict.items():

        if isinstance(metrics, dict):
            line = label if include_label else ""
            for metric_name, value in metrics.items():
                if metric_name in ignore_metrics:
                    continue
                if isinstance(value, float):
                    line += f" & {value:0.3f}"
                else:
                    line += f" & {value}"
            lines.append(line)
        elif isinstance(metrics, float):
            lines.append(f" & & & & {metrics:0.3f}")
        elif isinstance(metrics, int):
            lines.append(f" & & & & {metrics}")

        if include_line_break:
            lines[-1] += " \\\\"

    return lines


def keep_top_n_entries(report_dict_a, report_dict_b, n=10, num_total_classes=4):
    items_a = list(report_dict_a.items())
    pred_items_a = items_a[:-num_total_classes]
    avg_items_a = items_a[-num_total_classes:]
    items_b = list(report_dict_b.items())
    pred_items_b = items_b[:-num_total_classes]
    avg_items_b = items_b[-num_total_classes:]

    selection = [(abs(a[1]["f1-score"] - b[1]["f1-score"]), a, b) for a, b in zip(pred_items_a, pred_items_b)]
    selection.sort(reverse=True)

    selected = selection[:n]

    reduced_a = dict([a for _, a, _ in selected] + avg_items_a)
    reduced_b = dict([b for _, _, b in selected] + avg_items_b)

    return reduced_a, reduced_b


def add_std_total(report, num_total_classes=3):
    items = list(report.items())
    pred_items = items[:-num_total_classes]
    total_items = items[-num_total_classes:]

    precision_std = np.std([item_tuple[1]["precision"] for item_tuple in pred_items])
    recall_std = np.std([item_tuple[1]["recall"] for item_tuple in pred_items])
    f1_std = np.std([item_tuple[1]["f1-score"] for item_tuple in pred_items])

    total_items += [("std", {"precision": precision_std, "recall": recall_std, "f1-score": f1_std})]

    print(total_items)

    return dict(pred_items + total_items)


sns.set(font="Sans")

create_loss_plot(
    [
        "./hyperparam_search_reduced_ds_bs128_plr_2_train_loss.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.4_train_loss.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.2_train_loss.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.0_train_loss.csv",
    ],
    [
        "--peak-lr 2.0",
        "--peak-lr 1.4",
        "--peak-lr 1.2",
        "--peak-lr 1.0",
    ],
    "hyperparam_search_train_loss_bs128.pdf",
)

create_acc_plot(
    [
        "./hyperparam_search_reduced_ds_bs128_plr_2_train_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.4_train_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.2_train_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.0_train_accuracy.csv",
    ],
    [
        "./hyperparam_search_reduced_ds_bs128_plr_2_val_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.4_val_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.2_val_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs128_plr_1.0_val_accuracy.csv",
    ],
    [
        "--peak-lr 2.0",
        "--peak-lr 1.4",
        "--peak-lr 1.2",
        "--peak-lr 1.0",
    ],
    "hyperparam_search_accuracy_bs128.pdf",
)

create_loss_plot(
    [
        "./hyperparam_search_reduced_ds_bs12288_plr_7.0_train_loss.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_8.0_train_loss.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_9.0_train_loss.csv",
    ],
    [
        "--peak-lr 7.0",
        "--peak-lr 8.0",
        "--peak-lr 9.0",
    ],
    "hyperparam_search_train_loss_bs12288.pdf",
)

create_acc_plot(
    [
        "./hyperparam_search_reduced_ds_bs12288_plr_7.0_train_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_8.0_train_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_9.0_train_accuracy.csv",
    ],
    [
        "./hyperparam_search_reduced_ds_bs12288_plr_7.0_val_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_8.0_val_accuracy.csv",
        "./hyperparam_search_reduced_ds_bs12288_plr_9.0_val_accuracy.csv",
    ],
    [
        "--peak-lr 7.0",
        "--peak-lr 8.0",
        "--peak-lr 9.0",
    ],
    "hyperparam_search_accuracy_bs12288.pdf",
)


test_labels_bs12288, test_top_preds_bs12288 = read_bsv("./bs12288_plr_4.0_test_predictions.bsv")
test_labels_bs128, test_top_preds_bs128 = read_bsv("./bs128_plr_0.4_test_predictions.bsv")

report_bs12288 = classification_report(
    test_labels_bs12288,
    test_top_preds_bs12288[..., 0],
    target_names=Path("./classes.txt").read_text().splitlines(),
    output_dict=True,
)

report_bs128 = classification_report(
        test_labels_bs128,
        test_top_preds_bs128[..., 0],
        target_names=Path("./classes.txt").read_text().splitlines(),
        output_dict=True,
)

report_bs12288 = add_std_total(report_bs12288)
report_bs128 = add_std_total(report_bs128)

cols_bs12288 = latex_report_table_cols(report_bs12288, True, False, {"support"})
cols_bs128 = latex_report_table_cols(report_bs128, False, True, set())
cols = "\n".join([f"{c1.strip()} {c2.strip()}" for c1, c2 in zip(cols_bs12288, cols_bs128)])
print(cols)
print()

reduced_rep_bs12288, reduced_repo_bs128 = keep_top_n_entries(report_bs12288, report_bs128)
red_cols_bs12288 = latex_report_table_cols(reduced_rep_bs12288, True, False, {"support"})
red_cols_bs128 = latex_report_table_cols(reduced_repo_bs128, False, True, set())
red_cols = "\n".join([f"{c1.strip()} {c2.strip()}" for c1, c2 in zip(red_cols_bs12288, red_cols_bs128)])
print(red_cols)
