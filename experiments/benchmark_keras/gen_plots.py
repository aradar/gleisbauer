#! /usr/bin python3

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


def create_scaling_plot(data, output_file, strong=True):
    ax = sns.barplot(
        x="GPUs", y="Scaling", hue="Batch size", data=data, palette="Blues_d"
    )

    anno_color = "green"

    if strong:
        ax.add_patch(
            Rectangle(
                (-0.40, 0), 0.80, 1., edgecolor=anno_color, fill=False, lw=0.8, ls="--"
            )
        )

        ax.add_patch(
            Rectangle(
                (0.60, 0), 0.80, 2., edgecolor=anno_color, fill=False, lw=0.8, ls="--"
            )
        )

        ax.add_patch(
            Rectangle(
                (1.60, 0), 0.80, 3., edgecolor=anno_color, fill=False, lw=0.8, ls="--"
            )
        )

        ax.set(ylim=(0, 3.5), ylabel="Speedup")
    else:
        ax.set(ylim=(0, 1.3), ylabel="Efficiency")
        ax.axhline(y=1., color=anno_color, linestyle="--", linewidth=0.8)
        ax.legend(loc="upper center", ncol=3, title="Batch size")

    plt.setp(ax.get_legend().get_texts(), fontsize="9")
    plt.setp(ax.get_legend().get_title(), fontsize="9")

    for container in ax.containers:
        ax.bar_label(container, padding=-25, color="w", fontsize=9, rotation="vertical", fmt="%0.2f")

    plt.savefig(output_file)
    plt.close()


sns.set()

# plt.rcParams.update({
#   "text.usetex": True,
#   "font.family": "Computer Modern Roman"
# })

weak_scaling_results = pd.read_csv("./weak_scaling.csv", sep=";", header=0)

create_scaling_plot(
    weak_scaling_results.loc[weak_scaling_results["Input shape"] == "32,32,3"],
    "small_data_ws.pdf",
    strong=False,
)
create_scaling_plot(
    weak_scaling_results.loc[weak_scaling_results["Input shape"] == "224,224,3"],
    "big_data_ws.pdf",
    strong=False,
)

strong_scaling_results = pd.read_csv("./strong_scaling.csv", sep=";", header=0)

create_scaling_plot(
    strong_scaling_results.loc[strong_scaling_results["Input shape"] == "32,32,3"],
    "small_data_ss.pdf",
)
create_scaling_plot(
    strong_scaling_results.loc[strong_scaling_results["Input shape"] == "224,224,3"],
    "big_data_ss.pdf",
)
