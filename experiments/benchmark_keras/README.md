# Results

## strong scaling

$`scaling = t_1 / t_n`$


| Model       | Input shape | Num samples | Num epochs | GPUs | Batch size | Avg. time per epoch | Speedup | Net IF usage | Time values                        | Wall time | Training time | std Time values |
|-------------|-------------|-------------|------------|------|------------|---------------------|---------|--------------|------------------------------------|-----------|---------------|-----------------|
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 128        | 9.36                | 1       |              | 19.78, 9.38, 9.44, 9.34, 9.29      | 58.68     | 57.28         | 0.05            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 128        | 4.88                | 1.92    |              | 16.09, 4.88, 4.86, 4.90, 4.88      | 37.16     | 35.66         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 128        | 18.32               | 0.51    | 840 MBit/s   | 29.78, 18.29, 18.31, 18.32, 18.35  | 104.61    | 103.09        | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 256        | 6.05                | 1       |              | 17.10, 6.00, 6.03, 6.08, 6.10      | 42.74     | 41.35         | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 256        | 3.30                | 1.53    |              | 15.11, 3.32, 3.28, 3.30, 3.31      | 29.85     | 28.39         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 256        | 9.36                | 0.54    | 840 MBit/s   | 21.40, 9.37, 9.35, 9.35, 9.37      | 60.39     | 58.89         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 512        | 5.04                | 1       |              | 17.06, 5.02, 5.03, 5.06, 5.04      | 38.67     | 37.29         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 512        | 2.75                | 1.83    |              | 15.06, 2.75, 2.75, 2.75, 2.76      | 27.62     | 26.15         | 0.00            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 512        | 4.81                | 1.05    | 840 MBit/s   | 18.02, 4.84, 4.82, 4.80, 4.79      | 38.86     | 37.33         | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 1024       | 4.70                | 1       |              | 18.30, 4.70, 4.69, 4.68, 4.71      | 38.55     | 37.17         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 1024       | 2.56                | 1.84    |              | 16.72, 2.55, 2.55, 2.57, 2.56      | 28.54     | 27.06         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 1024       | 2.67                | 1.76    | 725 MBit/s   | 17.39, 2.67, 2.68, 2.67, 2.67      | 29.69     | 28.17         | 0.00            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 2048       | 4.73                | 1       |              | 19.72, 4.73, 4.73, 4.71, 4.77      | 40.23     | 38.80         | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 2048       | 2.44                | 1.94    |              | 18.43, 2.43, 2.45, 2.44, 2.46      | 29.85     | 28.36         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 2048       | 1.64                | 2.88    | 540 MBit/s   | 18.45, 1.65, 1.64, 1.63, 1.64      | 26.71     | 25.16         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 4096       | 4.94                | 1       |              | 24.15, 4.89, 4.90, 4.99, 4.96      | 45.60     | 44.18         | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 2    | 4096       | 2.55                | 1.94    |              | 22.18, 2.55, 2.52, 2.55, 2.56      | 34.25     | 32.71         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 3    | 4096       | 1.60                | 3.09    | 250 MBit/s   | 22.45, 1.59, 1.61, 1.59, 1.62      | 30.73     | 29.19         | 0.01            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 32         | 94.91               | 1       |              | 108.50, 94.72, 94.91, 94.77, 95.26 | 491.11    | 488.28        | 0.21            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 2    | 32         | 52.72               | 1.8     |              | 61.77, 50.87, 52.79, 53.51, 53.71  | 274.28    | 272.80        | 1.12            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 3    | 32         | 36.51               | 2.60    | 540 MBit/s   | 48.46, 35.98, 35.75, 37.04, 37.28  | 196.14    | 194.62        | 0.66            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 64         | 96.19               | 1       |              | 110.51, 97.56, 97.18, 95.56, 94.47 | 496.91    | 495.52        | 1.25            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 2    | 64         | 51.90               | 1.85    |              | 62.70, 51.54, 51.73, 51.96, 52.37  | 272.07    | 270.58        | 0.31            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 3    | 64         | 36.64               | 2.63    | 280 MBit/s   | 48.12, 36.12, 36.80, 36.72, 36.93  | 196.49    | 194.94        | 0.31            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 128        | 91.57               | 1       |              | 106.56, 91.85, 90.51, 91.26, 92.67 | 474.70    | 473.27        | 0.79            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 2    | 128        | 50.83               | 1.8     |              | 63.47, 50.41, 51.06, 50.97, 50.87  | 268.80    | 267.26        | 0.25            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 3    | 128        | 34.70               | 2.64    | 140 MBit/s   | 49.30, 33.45, 35.02, 34.97, 35.36  | 190.18    | 188.61        | 0.74            |

## weak scaling

$`scaling = t_1 / t_n * n`$

4096, 32, 32, 3 caused a tensorflow out of memory warning which could explain the reduced compute speed

| Model       | Input shape | Num samples | Num epochs | GPUs | Batch size | Avg. time per epoch | Speedup | Net IF usage | Time values                             | Wall time | Training time | std Time values |
|-------------|-------------|-------------|------------|------|------------|---------------------|---------|--------------|-----------------------------------------|-----------|---------------|-----------------|
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 128        | 9.26                | 1       |              | 22.06, 9.41, 9.22, 9.13, 9.29           | 62.08     | 59.18         | 0.10            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 128        | 9.64                | 1.92    |              | 21.02, 9.58, 9.68, 9.66, 9.63           | 61.11     | 59.62         | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 128        | 55.23               | 0.5     | 870 MBit/s   | 66.68, 55.25, 55.20, 55.19, 55.29       | 289.17    | 287.65        | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 256        | 6.07                | 1       |              | 17.13, 6.05, 6.08, 6.05, 6.10           | 42.86     | 41.48         | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 256        | 6.75                | 1.8     |              | 18.47, 6.77, 6.75, 6.76, 6.74           | 47.03     | 45.55         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 256        | 28.94               | 0.63    | 830 MBit/s   | 41.03, 28.96, 28.93, 28.94, 28.93       | 158.36    | 156.83        | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 512        | 5.15                | 1       |              | 17.19, 5.12, 5.11, 5.20, 5.16           | 39.24     | 37.84         | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 512        | 5.72                | 1.8     |              | 18.11, 5.69, 5.71, 5.75, 5.75           | 42.54     | 41.09         | 0.03            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 512        | 15.12               | 1.02    | 790 MBit/s   | 28.05, 15.12, 15.13, 15.10, 15.12       | 90.10     | 88.59         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 1024       | 4.80                | 1       |              | 17.76, 4.79, 4.81, 4.82, 4.79           | 38.41     | 37.05         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 1024       | 5.29                | 1.81    |              | 19.78, 5.30, 5.29, 5.29, 5.28           | 42.54     | 41.04         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 1024       | 8.38                | 1.72    | 710 MBit/s   | 23.01, 8.39, 8.38, 8.39, 8.38           | 58.17     | 56.65         | 0.00            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 2048       | 4.76                | 1       |              | 20.24, 4.77, 4.76, 4.77, 4.75           | 40.85     | 39.43         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 2048       | 5.06                | 1.88    |              | 21.59, 5.07, 5.06, 5.04, 5.06           | 43.47     | 41.98         | 0.01            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 2048       | 5.68                | 2.51    | 530 MBit/s   | 22.38, 5.65, 5.67, 5.66, 5.75           | 46.82     | 45.26         | 0.04            |
| MobileNetV2 | 32, 32, 3   | 4096 * 10   | 5          | 1    | 4096       | 4.87                | 1       |              | 24.67, 4.91, 4.88, 4.85, 4.86           | 45.84     | 44.46         | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 20   | 5          | 2    | 4096       | 5.36                | 1.82    |              | 25.33, 5.33, 5.36, 5.37, 5.39           | 48.59     | 47.11         | 0.02            |
| MobileNetV2 | 32, 32, 3   | 4096 * 30   | 5          | 3    | 4096       | 5.91                | 2.47    | 250 MBit/s   | 26.49, 5.87, 5.82, 5.88, 6.05           | 51.96     | 50.43         | 0.09            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 32         | 94.87               | 1       |              | 107.03, 95.01, 94.59, 95.15, 94.74      | 487.99    | 486.62        | 0.22            |
| MobileNetV2 | 224, 224, 3 | 4096 * 6    | 5          | 2    | 32         | 109.08              | 1.74    |              | 117.51, 109.01, 109.27, 109.08, 108.97  | 555.49    | 553.98        | 0.12            |
| MobileNetV2 | 224, 224, 3 | 4096 * 9    | 5          | 3    | 32         | 117.09              | 2.43    | 530 MBit/s   | 128.68, 117.64, 117.22, 116.94, 116.56  | 598.72    | 597.17        | 0.39            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 64         | 98.43               | 1       |              | 113.81, 100.65, 98.41, 97.64, 97.01     | 509.17    | 507.76        | 1.38            |
| MobileNetV2 | 224, 224, 3 | 4096 * 6    | 5          | 2    | 64         | 107.64              | 1.83    |              | 116.76, 106.13, 107.70, 108.40, 108.32  | 549.05    | 547.56        | 0.91            |
| MobileNetV2 | 224, 224, 3 | 4096 * 9    | 5          | 3    | 64         | 118.07              | 2.5     | 260 MBit/s   | 129.16, 118.45, 118.36, 117.75, 117.73  | 603.23    | 601.70        | 0.33            |
| MobileNetV2 | 224, 224, 3 | 4096 * 3    | 5          | 1    | 128        | 95.57               | 1       |              | 111.76, 97.35, 95.84, 94.95, 94.16      | 495.90    | 494.47        | 1.18            |
| MobileNetV2 | 224, 224, 3 | 4096 * 6    | 5          | 2    | 128        | 104.69              | 1.83    |              | 116.60, 103.93, 104.83, 105.16, 104.85  | 537.37    | 535.82        | 0.46            |
| MobileNetV2 | 224, 224, 3 | 4096 * 9    | 5          | 3    | 128        | 112.72              | 2.54    | 140 MBit/s   | 127.10, 112.95, 112.85, 112.77, 112.31  | 580.01    | 578.44        | 0.25            |