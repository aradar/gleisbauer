#!/usr/bin/env python3
import datetime
from pathlib import Path

from tensorflow import keras
import tensorflow as tf
import tensorflow_datasets as tfds
import horovod.tensorflow.keras as hvd


exp_dir = Path(__file__).resolve().parent.parent


class TimerCallback(keras.callbacks.Callback):
    def __init__(self):
        self.seconds_per_epoch = list()
        self.epoch_start = None
        self.train_start = None

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch_start = datetime.datetime.now()

    def on_epoch_end(self, epoch, logs=None):
        end = datetime.datetime.now()
        self.seconds_per_epoch.append(end - self.epoch_start)

    def on_train_begin(self, logs=None):
        self.train_start = datetime.datetime.now()

    def on_train_end(self, logs=None):
        end = datetime.datetime.now()
        train_time = end - self.train_start

        print(f"training took {train_time.total_seconds():0.2f} seconds")
        avg_secs = sum([d.total_seconds() for d in self.seconds_per_epoch[1:]]) / (
            len(self.seconds_per_epoch) - 1
        )
        print(f"avg time per epoch without first {avg_secs:0.2f}")
        print(
            "times per epoch"
            f" {', '.join([f'{d.total_seconds():0.2f}' for d in self.seconds_per_epoch])}"
        )


def main():
    wall_time_start = datetime.datetime.now()

    hvd.init()

    # batch_size = 32
    # batch_size = 64
    batch_size = 128
    # batch_size = 256
    # batch_size = 512
    # batch_size = 1024
    # batch_size = 2048
    # batch_size = 4096
    # num_samples = 4096 * 10
    num_samples = 4096 * 3
    # num_samples = 4096 * 10 * hvd.size()
    # num_samples = 4096 * 3 * hvd.size()
    # input_shape = (32, 32, 3)
    input_shape = (224, 224, 3)

    batches_per_epoch = int(num_samples / batch_size)
    num_epochs = 5

    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        tf.config.set_visible_devices(gpus[hvd.local_rank()], device_type="GPU")
        tf.config.experimental.set_memory_growth(gpus[hvd.local_rank()], True)

    x = tf.random.uniform([batch_size, input_shape[0], input_shape[1], input_shape[2]])
    y = tf.random.uniform([batch_size, 1], minval=0, maxval=999, dtype=tf.int32)
    ds_train = (
        tf.data.Dataset.from_tensor_slices((x, y))
        .cache()
        .repeat(int(batches_per_epoch / hvd.size()))
        .batch(batch_size)
    )

    model = tf.keras.applications.MobileNetV2(
        input_shape=input_shape, classes=1000, weights=None
    )

    raw_opt = tf.keras.optimizers.SGD(0.01 * hvd.size())
    dist_opt = hvd.DistributedOptimizer(raw_opt)

    if hvd.rank() == 0:
        model.summary()

    model.compile(
        optimizer=dist_opt,
        loss=tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=False
        ),  # model ends with softmax
        metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
        experimental_run_tf_function=False,
    )

    callbacks = [
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
    ]

    if hvd.rank() == 0:
        callbacks += [
            TimerCallback(),
        ]

    model.fit(
        ds_train,
        epochs=num_epochs,
        callbacks=callbacks,
        verbose=2 if hvd.rank() == 0 else 0,
    )

    if hvd.rank() == 0:
        wall_time_end = datetime.datetime.now()
        print(
            "wall time"
            f" {(wall_time_end - wall_time_start).total_seconds():0.2f} seconds"
        )


if __name__ == "__main__":
    main()
