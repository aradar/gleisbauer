# creating a new dataset

```shell
tfds new <name_with_snake_case_if_needed>
```

# building a dataset

Run the following command to build the datasets. In some cases like for example the ImageNet dataset it makes sense to
set the optional `--data_dir` argument to place with a lot of storage.

```shell
cd <dataset_subdir>
tfds build [--data_dir <path>]
```
