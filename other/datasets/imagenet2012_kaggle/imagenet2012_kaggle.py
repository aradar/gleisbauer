"""imagenet2012_kaggle dataset."""
import csv
from collections import OrderedDict, defaultdict
from pathlib import Path
from typing import Dict, Tuple, List

import tensorflow as tf
import tensorflow_datasets as tfds

# TODO(imagenet2012_kaggle): Markdown description  that will appear on the catalog page.
from tensorflow_datasets.core.utils.image_utils import _get_runner

_DESCRIPTION = """
Description is **formatted** as markdown.

It should also contain any processing which has been applied (if any),
(e.g. corrupted example skipped, images cropped,...):
"""

# Web-site is asking to cite paper from 2015.
# http://www.image-net.org/challenges/LSVRC/2012/index#cite
_CITATION = """\
@article{ILSVRC15,
Author = {Olga Russakovsky and Jia Deng and Hao Su and Jonathan Krause and Sanjeev Satheesh and Sean Ma and Zhiheng Huang and Andrej Karpathy and Aditya Khosla and Michael Bernstein and Alexander C. Berg and Li Fei-Fei},
Title = {{ImageNet Large Scale Visual Recognition Challenge}},
Year = {2015},
journal   = {International Journal of Computer Vision (IJCV)},
doi = {10.1007/s11263-015-0816-y},
volume={115},
number={3},
pages={211-252}
}
"""

IMAGE_SHAPE = 224

# region images with problems
# From https://github.com/cytsai/ilsvrc-cmyk-image-list
CMYK_IMAGES = [
    "n01739381_1309.JPEG",
    "n02077923_14822.JPEG",
    "n02447366_23489.JPEG",
    "n02492035_15739.JPEG",
    "n02747177_10752.JPEG",
    "n03018349_4028.JPEG",
    "n03062245_4620.JPEG",
    "n03347037_9675.JPEG",
    "n03467068_12171.JPEG",
    "n03529860_11437.JPEG",
    "n03544143_17228.JPEG",
    "n03633091_5218.JPEG",
    "n03710637_5125.JPEG",
    "n03961711_5286.JPEG",
    "n04033995_2932.JPEG",
    "n04258138_17003.JPEG",
    "n04264628_27969.JPEG",
    "n04336792_7448.JPEG",
    "n04371774_5854.JPEG",
    "n04596742_4225.JPEG",
    "n07583066_647.JPEG",
    "n13037406_4650.JPEG",
]

PNG_IMAGES = ["n02105855_2933.JPEG"]
# endregion


def assert_paths(*paths: Path):
    for p in paths:
        assert p.exists()
        if p.is_dir():
            # not empty check
            assert next(p.iterdir(), None) is not None


def build_name_map(mapping_file: Path) -> Dict:
    label_fixes = [
        ("n02012849", "crane_bird"),
    ]

    with mapping_file.open("r") as f:
        name_map = OrderedDict([l.rstrip().split(" ", 1) for l in f.readlines()])

        for [k, v] in label_fixes:
            name_map[k] = v

    return name_map


def read_gt_file(
    data_root_dir: Path, gt_file: Path, is_train_file: bool
) -> Dict[str, List[Path]]:
    with gt_file.open("r") as f:
        reader = csv.reader(f)
        next(reader, None)  # drop header row

        data = defaultdict(list)
        for row in reader:
            [image_id, pred_str] = row
            class_label = pred_str.split(" ")[0]

            if is_train_file:
                image_path = data_root_dir / "train" / class_label
            else:
                image_path = data_root_dir / "val"

            image_file = image_path / f"{image_id}.JPEG"

            data[class_label].append(image_file)

    return {k: sorted(v) for k, v in data.items()}


def split_data_dict(
    data: Dict[str, List[Path]], samples_per_class: int = 50
) -> Tuple[Dict[str, List[Path]], Dict[str, List[Path]]]:

    first_new_data = dict()
    sec_new_data = dict()
    for k, v in data.items():
        first_new_data[k] = v[:-samples_per_class]
        sec_new_data[k] = v[-samples_per_class:]

    return first_new_data, sec_new_data


def fix_image(image_path: Path):
    image_bytes = image_path.read_bytes()
    img_name = image_path.name

    if img_name not in PNG_IMAGES:
        image = tf.image.decode_jpeg(image_bytes, channels=3)
    else:
        image = tf.image.decode_png(image_bytes, channels=3)

    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize_with_pad(image, IMAGE_SHAPE, IMAGE_SHAPE)
    image = tf.image.convert_image_dtype(image, tf.uint8)
    fixed_bytes = tf.image.encode_jpeg(image, format="rgb", quality=100).numpy()

    return fixed_bytes


def generate_examples(data: Dict[str, List[Path]], name_map: Dict[str, str]):
    examples = [list(zip([k] * len(v), v)) for k, v in data.items()]
    examples = [item for sublist in examples for item in sublist]

    for [label, image_path] in examples:
        yield image_path.stem, {
            "image": fix_image(image_path),  # bytes of jpeg
            "label": label,
            "human_readable_label": name_map[label],
            "file_name": image_path.name,
        }


class Imagenet2012Kaggle(tfds.core.GeneratorBasedBuilder):
    """DatasetBuilder for imagenet2012_kaggle dataset."""

    VERSION = tfds.core.Version("1.0.0")
    RELEASE_NOTES = {
        "1.0.0": "Initial release.",
    }

    MANUAL_DOWNLOAD_INSTRUCTIONS = """\
    manual_dir should contain the unzipped content of imagenet-object-localization-challenge.zip and its nested archive
    imagenet_object_localization_patched2019.tar.gz. To be precise it has to contain the following files or dirs:
    ILSVRC, LOC_train_solution.csv, LOC_val_solution.csv

    You need to register/ login with a third party service on https://www.kaggle.com/ to download the data from
    https://www.kaggle.com/c/imagenet-object-localization-challenge.
    """

    def _info(self) -> tfds.core.DatasetInfo:
        name_map = build_name_map(Path(__file__).with_name("LOC_synset_mapping.txt"))
        return tfds.core.DatasetInfo(
            builder=self,
            description=_DESCRIPTION,
            features=tfds.features.FeaturesDict(
                {
                    "image": tfds.features.Image(
                        shape=(IMAGE_SHAPE, IMAGE_SHAPE, 3), encoding_format="jpeg"
                    ),
                    "label": tfds.features.ClassLabel(names=list(name_map.keys())),
                    "human_readable_label": tfds.features.ClassLabel(
                        names=list(name_map.values())
                    ),
                    "file_name": tfds.features.Text(),  # Eg: 'n15075141_54.JPEG'
                }
            ),
            supervised_keys=("image", "label"),
            homepage="http://image-net.org/",
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager: tfds.download.DownloadManager):
        mapping_file = Path(__file__).with_name("LOC_synset_mapping.txt")

        data_dir = Path(dl_manager.manual_dir)
        image_root_data = data_dir / "ILSVRC/Data/CLS-LOC"
        train_gt_file = data_dir / "LOC_train_solution.csv"
        val_gt_file = data_dir / "LOC_val_solution.csv"

        assert_paths(mapping_file, image_root_data, train_gt_file, val_gt_file)

        train_data = read_gt_file(image_root_data, train_gt_file, True)
        train_data, test_data = split_data_dict(train_data)
        val_data = read_gt_file(image_root_data, val_gt_file, False)

        # # todo: debug only small subsample
        # _, train_data = split_data_dict(train_data, 10)
        # _, test_data = split_data_dict(test_data, 10)
        # _, val_data = split_data_dict(val_data, 10)

        name_map = build_name_map(mapping_file)

        return {
            "train": generate_examples(train_data, name_map),
            "validation": generate_examples(val_data, name_map),
            "test": generate_examples(test_data, name_map),
        }

    def _generate_examples(self, path, name_map):
        raise RuntimeError("not implemented")
        # TODO(imagenet2012_kaggle): Yields (key, example) tuples from the dataset
        for f in path.glob("*.jpeg"):
            yield "key", {
                "image": f,
                "label": "yes",
            }
