import tensorflow_datasets as tfds
import tensorflow as tf
import cv2

# ds, info = tfds.load("imagenet2012_kaggle", data_dir="/mnt/data/master_thesis/tensorflow_datasets/", with_info=True)
#
# train_ds: tf.data.Dataset = ds["train"]
# a = train_ds.take(5)
#
# names = info.features["human_readable_label"].names
#
# for b in a:
#     file_name = b["file_name"].numpy().decode("utf-8")
#     images = b["images"].numpy()
#     images = cv2.cvtColor(images, cv2.COLOR_RGB2BGR)
#     labels = b["labels"].numpy()
#     print("labels:", names[labels], "file_name:", file_name)
#     cv2.imwrite(file_name, images)
#
#     print(b["images"].numpy())
#
# print("hi")


def prepare_example(image: tf.Tensor, label: tf.Tensor):
    image = tf.cast(image, tf.float32)
    image /= 255.0

    return image, label


ds = tfds.load(
    "imagenet2012_kaggle",
    data_dir="/mnt/data/master_thesis/tensorflow_datasets/",
    split="train",
    as_supervised=True,
)

ds = ds.map(prepare_example)
ds = ds.batch(128)
ds = ds.prefetch(512)

tfds.benchmark(ds, batch_size=128)
