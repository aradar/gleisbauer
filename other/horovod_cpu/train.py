import argparse
import math
from typing import Tuple

import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow.keras.layers as tfkl
import tensorflow.keras as tfk
import horovod.keras as hvd


def prepare_data(
    train_batch_size: int = 128, test_batch_size: int = 128
) -> Tuple[tf.data.Dataset, tf.data.Dataset, tfds.core.DatasetInfo]:

    # todo: add test split

    (ds_train, ds_test), ds_info = tfds.load(
        "fashion_mnist",
        split=["train", "test"],
        shuffle_files=True,
        with_info=True,
        as_supervised=True,
    )

    def _process(image, label):
        processed_image = tf.cast(image, tf.float32) / 255.0
        processed_label = tf.one_hot(
            label, depth=ds_info.features["labels"].num_classes
        )
        return processed_image, processed_label

    ds_train = ds_train.map(_process, num_parallel_calls=tf.data.AUTOTUNE)
    ds_train = ds_train.cache()
    ds_train = ds_train.shuffle(ds_info.splits["train"].num_examples)
    ds_train = ds_train.batch(train_batch_size)
    ds_train = ds_train.prefetch(tf.data.AUTOTUNE)

    ds_test = ds_test.map(_process, num_parallel_calls=tf.data.AUTOTUNE)
    ds_test = ds_test.batch(test_batch_size)
    ds_test = ds_test.cache()
    ds_test = ds_test.prefetch(tf.data.AUTOTUNE)

    return ds_train, ds_test, ds_info


# noinspection PyPep8Naming
def build_LeNet5(input_shape: tuple) -> tfk.Model:
    return tfk.Sequential(
        [
            tfkl.Conv2D(
                input_shape=input_shape,
                filters=6,
                kernel_size=5,
                padding="same",
                activation="relu",
            ),
            tfkl.AvgPool2D(),
            tfkl.Conv2D(filters=16, kernel_size=5, padding="valid", activation="relu"),
            tfkl.AvgPool2D(),
            tfkl.Flatten(),
            tfkl.Dense(120, activation="relu"),
            tfkl.Dense(84, activation="relu"),
            tfkl.Dense(10, activation="relu"),
        ]
    )


def main(
    seed: int, num_helper: int, use_horovod: bool, num_epochs: int, batch_size: int
):
    hvd.init()

    tf.random.set_seed(seed)
    np.random.seed(seed * 2)

    if use_horovod:
        tf.config.threading.set_intra_op_parallelism_threads(1)
        tf.config.threading.set_inter_op_parallelism_threads(1)
    elif num_helper > 0:
        tf.config.threading.set_intra_op_parallelism_threads(num_helper)
        tf.config.threading.set_inter_op_parallelism_threads(num_helper)

    train, test, info = prepare_data(train_batch_size=batch_size)

    model = build_LeNet5(info.features["images"].shape)

    optimizer = tf.keras.optimizers.Adam(0.001 * hvd.size())

    if use_horovod:
        optimizer = hvd.DistributedOptimizer(optimizer)

    model.compile(
        optimizer=optimizer,
        loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
        metrics=[tf.keras.metrics.CategoricalAccuracy()],
    )

    callbacks = [
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
    ]

    if hvd.rank() == 0:
        model.summary()

    model.summary()
    model.fit(
        train,
        epochs=int(math.ceil(num_epochs / hvd.size())),
        callbacks=callbacks,
        verbose=1 if hvd.rank() == 0 else 0,
    )

    if hvd.rank() == 0:
        print("eval result:", model.evaluate(test))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--seed", type=int, default=91291249)
    parser.add_argument("--num-helper", type=int, default=-1)
    parser.add_argument("--use-horovod", action="store_true")
    parser.add_argument("--num-epochs", type=int, default=20)
    parser.add_argument("--batch-size", type=int, default=128)

    args = parser.parse_args()
    main(**args.__dict__)
