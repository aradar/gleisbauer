#!/usr/bin/env python

import itertools
import json
import signal
import subprocess
from datetime import datetime, timedelta
from pathlib import Path
from time import sleep
from typing import Dict

import psutil
import pandas as pd
import plotly.express as px


def run_command(command: str) -> dict:
    def get_net_io_counters_dict() -> dict:
        return {
            k: v._asdict()
            for k, v in psutil.net_io_counters(pernic=True, nowrap=True).items()
        }

    results = dict(
        net_io_counters=[(None, get_net_io_counters_dict())],
        memory_info=list(),
        cpu_times=None,
        connections=set(),
        io_counters=list(),
        start_time=datetime.now().isoformat(),
        system_load_1_min=list(),
    )

    proc = psutil.Popen(command.split(" "))
    try:
        while proc.poll() is None:
            now = datetime.now().isoformat()

            results["last_update_time"] = now
            results["net_io_counters"].append((now, get_net_io_counters_dict()))
            results["system_load_1_min"].append((now, psutil.getloadavg()[0]))

            results["cpu_times"] = proc.cpu_times()._asdict()
            connections = proc.connections("all")
            if connections:
                for c in connections:
                    results["connections"].add(frozenset(c._asdict().items()))
            results["memory_info"].append((now, proc.memory_info()._asdict()))
            results["io_counters"].append((now, proc.io_counters()._asdict()))

            sleep(0.5)
    except psutil.AccessDenied:
        pass  # there is a rare chance to try to access the data shortly after the process finished
    except:
        proc.send_signal(signal.SIGINT)
        raise RuntimeError()
    finally:
        proc.communicate()

    results["connections"] = [dict(c) for c in results["connections"]]

    return results


def do_benchmark_run(
    num_epochs: int, num_helper: int, batch_size: int, use_horovod: bool
):
    if use_horovod:
        return run_command(
            "horovodrun -np {} -H localhost:{} python train.py "
            "--num-epochs {} "
            "--use-horovod "
            "--batch-size {}".format(num_helper, num_helper, num_epochs, batch_size)
        )
    return run_command(
        "python3 train.py --num-epochs {} --num-helper {} --batch-size {}".format(
            num_epochs, num_helper, batch_size
        )
    )


def plot(results: pd.DataFrame):
    name_df = results.loc[:, results.columns != "time"]
    names = [
        "n: {}, b: {}, h: {}".format(t[0], t[1], t[2]) for t in name_df.values.tolist()
    ]
    fig = px.bar(
        x=names,
        y=results.time,
        labels=dict(
            x="Variant (n = num-helper, b = batch-size, h = use-horovod)",
            y="Execution time (s)",
        ),
    )
    fig.write_image("benchmark_result.pdf")


if __name__ == "__main__":

    # pos_num_helper = [1, 2, 6, 12, 24]
    pos_num_helper = [1, 2, 4, 8]
    # pos_batch_size = [128, 1024, 4096]
    pos_batch_size = [128, 4096]
    pos_use_horovod = [False, True]

    params = [pos_num_helper, pos_batch_size, pos_use_horovod]

    results = list()
    for num_helper, batch_size, use_horovod in itertools.product(*params):
        num_epochs = 4 * num_helper

        results.append(
            dict(
                config=dict(
                    num_epochs=num_epochs,
                    num_helper=num_helper,
                    batch_size=batch_size,
                    horovod_run=use_horovod,
                ),
                result=do_benchmark_run(
                    num_epochs, num_helper, batch_size, use_horovod
                ),
            )
        )

        sleep(70)  # to get the load avg back to normal sys load

    with (Path.cwd() / "benchmark_results.json").open("w") as f:
        json.dump(results, f, sort_keys=True, indent=4)

    # results = [(1, 4096, False, 22), (1, 4096, True, 24)]

    # result_df = pd.DataFrame(
    #         data=results,
    #         columns=["num_helper", "batch_size", "use_horovod", "time"])
    #
    # plot(result_df)
