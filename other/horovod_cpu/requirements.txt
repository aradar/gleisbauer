tensorflow==2.6
tensorflow-datasets==4.4
matplotlib==3.4.3
horovod==0.23.0

plotly
pandas
kaleido
