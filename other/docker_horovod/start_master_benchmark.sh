#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
cd ${SCRIPT_DIR}

docker run \
    --gpus "device=0" \
    --rm \
    -it \
    --network=host \
    --mount type=bind,source=${SCRIPT_DIR}/train.py,target=/root/train.py \
    mt_horovod_worker_exp \
    bash
