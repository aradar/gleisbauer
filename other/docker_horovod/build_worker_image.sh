#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
cd ${SCRIPT_DIR}

# always generate a new key pair
ssh-keygen -q -t ed25519 -N "" -C "auto generated key" -f horovod_worker_ssh_key <<<y >/dev/null 2>&1

docker build \
    -t mt_horovod_worker_exp:latest \
    -f worker_dockerfile \
    .
