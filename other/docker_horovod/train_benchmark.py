import argparse
import math
from typing import Tuple

import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow.keras.layers as tfkl
import tensorflow.keras as tfk
import horovod.keras as hvd
from tensorflow.keras import applications


def main(seed: int, num_epochs: int, batches_per_epoch: int, batch_size: int):
    hvd.init()

    tf.random.set_seed(seed)
    np.random.seed(seed * 2)

    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)

    data = tf.random.uniform([batch_size, 224, 224, 3])
    target = tf.random.uniform([batch_size, 1], minval=0, maxval=999, dtype=tf.int32)
    train = (
        tf.data.Dataset.from_tensor_slices((data, target))
        .cache()
        .repeat(batches_per_epoch)
        .batch(batch_size)
    )

    model = applications.ResNet50(weights=None)

    optimizer = tf.keras.optimizers.SGD(0.001 * hvd.size())
    optimizer = hvd.DistributedOptimizer(optimizer)

    model.compile(
        optimizer=optimizer,
        # loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(),
        metrics=[tf.keras.metrics.CategoricalAccuracy()],
    )

    callbacks = [
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
    ]

    if hvd.rank() == 0:
        model.summary()

    model.summary()
    model.fit(
        train,
        epochs=int(math.ceil(num_epochs / hvd.size())),
        callbacks=callbacks,
        verbose=1 if hvd.rank() == 0 else 0,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--seed", type=int, default=91291249)
    parser.add_argument("--num-epochs", type=int, default=1)
    parser.add_argument("--batches-per-epoch", type=int, default=1)
    parser.add_argument("--batch-size", type=int, default=64)

    args = parser.parse_args()
    main(**args.__dict__)
