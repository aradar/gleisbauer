#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
cd ${SCRIPT_DIR}

docker run \
    --gpus "device=0" \
    --rm \
    -it \
    --network=host \
    --ipc=host \
    --ulimit memlock=-1 \
    --ulimit stack=67108864 \
    --mount type=bind,source=${SCRIPT_DIR}/train_imagenet.py,target=/root/train_imagenet.py,readonly \
    --mount type=bind,source=$(realpath ${SCRIPT_DIR}/../../datasets/imagenet2012_kaggle),target=/root/imagenet2012_kaggle,readonly \
    --mount type=bind,source=$(realpath ~/tensorflow_datasets),target=/root/tensorflow_datasets,readonly \
    --env TFDS_DATA_DIR=/root/tensorflow_datasets \
    mt_horovod_worker_exp \
    bash
