#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
cd ${SCRIPT_DIR}

docker run \
    -d \
    --gpus "device=1" \
    --rm \
    -it \
    --network=host \
    --mount type=bind,source=${SCRIPT_DIR}/train.py,target=/root/train.py \
    mt_horovod_worker_exp \
    bash -c "mkdir /run/sshd; /usr/sbin/sshd -p 12345; sleep infinity"

docker run \
    -d \
    --gpus "device=2" \
    --rm \
    -it \
    --network=host \
    --mount type=bind,source=${SCRIPT_DIR}/train.py,target=/root/train.py \
    mt_horovod_worker_exp \
    bash -c "mkdir /run/sshd; /usr/sbin/sshd -p 12346; sleep infinity"
