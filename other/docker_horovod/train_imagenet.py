import argparse

from typing import Tuple

import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow_datasets as tfds

# import imagenet2012_kaggle


def add_regularizer(model: tfk.Model, regularizer: tfk.regularizers.Regularizer):
    for layer in model.layers:
        if isinstance(layer, tfk.layers.Conv2D):
            layer.kernel_regularizer = regularizer
        # if hasattr(layer, "bias_regularizer") and layer.use_bias:
        #     layer.add_loss(tfk.regularizers.l2(l2_coef)(layer.bias))


def prepare_examples(num_classes: int, image_height: int = 224, image_width: int = 224):
    def _prep(images: tf.Tensor, labels: tf.Tensor) -> Tuple[tf.Tensor, tf.Tensor]:
        images = tf.cast(images, tf.float32)
        images = tf.image.resize_with_pad(
            images, target_height=image_height, target_width=image_width
        )

        labels = tf.one_hot(labels, num_classes)

        return images, labels

    return _prep


def main(seed: int, num_epochs: int, batches_per_epoch: int, batch_size: int):
    ds, info = tfds.load(
        "food101",
        # "imagenet2012_kaggle",
        # "cifar10",
        # data_dir="/root/tensorflow_datasets",  # todo: remove me
        with_info=True,
        as_supervised=True,
    )

    # input_shape = info.features["image"].shape
    input_shape = (224, 224, 3)
    num_classes = info.features["label"].num_classes

    train_ds: tf.data.Dataset = ds["train"]
    train_ds = train_ds.shuffle(512, reshuffle_each_iteration=True)
    train_ds = train_ds.map(prepare_examples(num_classes))
    train_ds = train_ds.batch(batch_size)

    # val_ds: tf.data.Dataset = ds["test"]  # <= cifar10
    val_ds: tf.data.Dataset = ds["validation"]  # <= imagenet
    val_ds = val_ds.map(prepare_examples(num_classes))
    val_ds = val_ds.batch(batch_size)

    inputs = tfk.layers.Input(shape=input_shape)
    # tfk.layers.experimental.preprocessing.Normalization()
    # normalize = tfk.layers.Normalization()
    # normalize.adapt(ds["train"])
    preprocessing = tfk.Sequential(
        name="preprocessing",
        layers=[
            tfk.layers.Rescaling(scale=1.0 / 127.5, offset=-1),
            tfk.layers.RandomFlip("horizontal"),
            tfk.layers.RandomRotation(0.1),
            tfk.layers.RandomZoom(0.1),
        ],
    )

    x = preprocessing(inputs)

    # classifier = tfk.applications.EfficientNetB0(weights=None)
    # classifier = tfk.applications.ResNet50(
    #         weights=None,
    #         include_top=True,
    #         classes=num_classes)
    # classifier = tfk.applications.MobileNetV3Small(
    #         weights=None,
    #         include_top=True,
    #         classes=num_classes,
    #         include_preprocessing=False,
    #         dropout_rate=0.2)
    classifier = tfk.applications.MobileNetV3Large(
        weights=None,
        include_top=True,
        classes=num_classes,
        include_preprocessing=False,
        dropout_rate=0.2,
    )
    add_regularizer(classifier, tfk.regularizers.L2(1e-4))

    model = tfk.Model(inputs, classifier(x))

    cardinality = tf.data.experimental.cardinality(train_ds).numpy()

    # learning_rate = 0.1
    learning_rate = tfk.optimizers.schedules.PiecewiseConstantDecay(
        boundaries=[
            int(cardinality * (num_epochs * 0.5)),
            int(cardinality * (num_epochs * 0.75)),
        ],
        values=[0.01, 0.001, 0.0001],
    )

    optimizer = tfk.optimizers.SGD(learning_rate=learning_rate, momentum=0.9)

    model.compile(
        optimizer=optimizer,
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=[tf.keras.metrics.CategoricalAccuracy()],
    )

    callbacks = [
        # tfk.callbacks.ReduceLROnPlateau(verbose=1, monitor="val_loss")
    ]

    model.summary()

    model.fit(train_ds, validation_data=val_ds, epochs=num_epochs, callbacks=callbacks)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--seed", type=int, default=91291249)
    parser.add_argument("--num-epochs", type=int, default=1)
    parser.add_argument("--batches-per-epoch", type=int, default=1)
    parser.add_argument("--batch-size", type=int, default=96)

    args = parser.parse_args()
    main(**args.__dict__)
