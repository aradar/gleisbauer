#!/usr/bin/env bash

set -e

start_port=12345
num_nodes=4
image_name="dl_train_distributor_e2e_test:${USER}"
docker_home="docker_home"
user_ssh_key_file="${docker_home}/.ssh/test_user_key_ssh"

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${script_dir}

app="../dist/dist-dl-train.pyz"

if [[ -f ".running_containers" ]]; then
    echo "found a .running_containers file."
    echo "please check if there is already a e2e test running or it crashed unexpectedly. if it crashed unexpectedly kill the containers (if still running) and delete the lock file."
    exit 1
fi

if [[ ! -f  ${app} ]]; then
    echo "no existing dist package of the app exists. packaging it now..."
    echo "------------------------------------------------------------"
    cd ..
    poetry run tox -e py36-package
    cd ${script_dir}
    echo ""
fi

mkdir -p ${docker_home}
mkdir -m 700 -p "${docker_home}/.ssh"

if [[ ! -f ${user_ssh_key_file} ]]; then
    echo "no existing ssh key pair found for testing. creating it..."
    echo "------------------------------------------------------------"
    # auth key
    ssh-keygen -q -t ed25519 -N "" -C "auto generated key for e2e test of dist-dl-train" -f ${user_ssh_key_file} <<<y >/dev/null 2>&1
    cp ${user_ssh_key_file}.pub "${docker_home}/.ssh/authorized_keys"
    # host keys
    ssh-keygen -q -t ed25519 -N "" -f "${docker_home}/.ssh/ssh_host_ed25519_key" <<<y >/dev/null 2>&1
    ssh-keygen -q -t rsa -N "" -f "${docker_home}/.ssh/ssh_host_rsa_key" <<<y >/dev/null 2>&1
    ssh-keygen -q -t ecdsa -N "" -f "${docker_home}/.ssh/ssh_host_ecdsa_key" <<<y >/dev/null 2>&1
    echo ""
fi


echo "adding test ssh key to agent..."
echo "------------------------------------------------------------"
ssh-add ${user_ssh_key_file}
echo ""

if [[ -n "$(docker container inspect ${image_name} 2>&1 > /dev/null)" ]]; then

    echo "test docker image is missing... building it now..."
    echo "------------------------------------------------------------"

    docker build \
        -t ${image_name} \
        -f dockerfile \
        .

    echo ""
fi

echo "starting nodes with running sshd now..."
echo "------------------------------------------------------------"

start_node_container() {
    docker run \
        -d \
        --rm \
        --network=host \
        --user $(id -u ${USER}):$(id -g ${USER}) \
        --group-add $(getent group docker | cut -d: -f3) \
        --volume $(realpath ${docker_home}):${HOME} \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        --volume /etc/passwd:/etc/passwd:ro \
        --volume /etc/shadow:/etc/shadow:ro \
        --volume /etc/group:/etc/group:ro \
        --volume /etc/gshadow:/etc/gshadow:ro \
        --name "dist-dl-train_e2e_test_node_port_${1}" \
        ${image_name} \
        bash -c "mkdir /run/sshd; /usr/sbin/sshd -p ${1} -h ${HOME}/.ssh/ssh_host_ed25519_key -h ${HOME}/.ssh/ssh_host_ed25519_key -h ${HOME}/.ssh/ssh_host_rsa_key -h ${HOME}/.ssh/ssh_host_ecdsa_key; sleep infinity"
}


for (( port=${start_port}; port <= $((${start_port} + ${num_nodes})); port++ )); do
    start_node_container ${port} >> .running_containers
    echo "started node container with ssh port ${port}"
done

sleep 3
echo ""

# enables no exit on error again as we need to stop the running containers even if something fails
set +e

echo "starting testing..."
echo "------------------------------------------------------------"

#nodes_arg=""
#for (( port=${start_port}; port <= $((${start_port} + ${num_nodes})); port++ )); do
#    nodes_arg+="localhost:${port} "
#done

#./${app} ${nodes_arg}

read -s

echo ""

echo "stopping nodes..."
echo "------------------------------------------------------------"
docker container kill $(cat .running_containers | tr "\n" " ")
rm .running_containers
echo ""

echo "removing ssh key from agent..."
echo "------------------------------------------------------------"
ssh-add -d ${user_ssh_key_file}
